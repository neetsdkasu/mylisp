enum_with_iter! {
    #[derive(Debug, Clone, Copy, PartialEq)]
    pub enum SpecialForm {
        Cond,
        Define,
        Lambda,
        Quote,
        Quasiquote,
        Unquote,
        UnquoteSplicing,
        Macro,
        If,
        Recur,
        Next,
        Delay,
        DelayGet,
    }
}

impl SpecialForm {
    pub fn symbol(self) -> &'static str {
        use SpecialForm::*;
        match self {
            Cond => "cond",
            Define => "define",
            Lambda => "lambda",
            Quote => "quote",
            Quasiquote => "quasiquote",
            Unquote => "unquote",
            UnquoteSplicing => "unquote-splicing",
            Macro => "macro",
            If => "if",
            Recur => "recur",
            Next => "next",
            Delay => "delay",
            DelayGet => "delay-get",
        }
    }
}
