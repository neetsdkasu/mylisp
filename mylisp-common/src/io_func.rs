enum_with_iter! {
    #[derive(Debug, Clone, Copy, PartialEq)]
    pub enum IOFunc {
        Println,
        Readln,
        Eprintln,
        RaiseError,
    }
}

impl IOFunc {
    pub fn symbol(self) -> &'static str {
        use IOFunc::*;
        match self {
            Println => "println",
            Readln => "readln",
            Eprintln => "eprintln",
            RaiseError => "error",
        }
    }
}
