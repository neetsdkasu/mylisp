macro_rules! args_count {
    ($v:tt) => (1);
    ($v:tt, $($vs:tt),*) => (1 + args_count!($($vs),*));
}

macro_rules! enum_with_iter {
    {
        #[$($attr:meta),*]
        $vis:vis enum $name:ident {
            $($element:ident,)*
        }
    } => {
        #[$($attr),*]
        $vis enum $name {
            $($element,)*
        }

        mod enum_iter {
            use super::$name;

            impl $name {
                pub fn iter() -> Iter {
                    Iter::new()
                }
            }

            static ENUMS: [$name; args_count!($($element),*)] = {
                use $name::*;
                [$($element),*]
            };

            pub struct Iter {
                iter: std::slice::Iter<'static, $name>,
            }

            impl Iter {
                fn new() -> Iter {
                    Iter { iter: ENUMS.iter() }
                }
            }

            impl std::iter::Iterator for Iter {
                type Item = $name;
                fn next(&mut self) -> Option<Self::Item> {
                    self.iter.next().cloned()
                }
            }
        }
    };
}
