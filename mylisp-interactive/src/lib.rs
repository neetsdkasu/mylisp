use std::io;

use mylisp_code::{Case, Code, Syntax};
use mylisp_engine::{Engine, IOSet, Scope};
use mylisp_parser::Parser;
use mylisp_runner::print_runtime_error;
use mylisp_tokenizer::Tokenizer;

pub struct InteractiveFlags {
    pub show_prompt: bool,
    pub show_result: bool,
    pub auto_indent: bool,
    pub scope: Scope,
    pub preload_file: Option<String>,
}

impl InteractiveFlags {
    pub fn start<RI, WI, EI, RE, WE, EE>(
        mut self,
        io_set_for_interactive: IOSet<RI, WI, EI>,
        io_set_for_engine: IOSet<RE, WE, EE>,
    ) -> io::Result<()>
    where
        RI: io::BufRead,
        WI: io::Write,
        EI: io::Write,
        RE: io::BufRead,
        WE: io::Write,
        EE: io::Write,
    {
        let IOSet {
            input,
            mut output,
            mut error,
            mut case,
            mut syntax,
        } = io_set_for_interactive;

        let tokenizer = Tokenizer::new(input);
        let mut parser = Parser::new(tokenizer);
        let mut engine = Engine::with_scope(self.scope, io_set_for_engine);

        self.bind_specials(&mut engine)?;

        self.preload(&mut engine, &mut error)?;

        if self.show_result {
            let program_path = std::env::current_exe()?;
            if let Some(file_name) = program_path.file_name() {
                if let Some(file_name) = file_name.to_str() {
                    writeln!(
                        &mut error,
                        "[[ INFO ]] scope: {:?}, case: {:?}, syntax: {:?}",
                        self.scope, case, syntax
                    )?;
                    writeln!(&mut error, "[[ INFO ]] run (::quit) to exit {}", file_name)?;
                }
            }
        }

        let mut codes = vec![];

        loop {
            loop {
                match parser.current_nest() {
                    0 => {
                        if !codes.is_empty() {
                            break;
                        }
                        if self.show_prompt {
                            write!(&mut output, "LISP> ")?;
                            output.flush()?;
                        }
                    }
                    nest => {
                        if self.show_prompt {
                            write!(&mut output, "-{:>2}-| ", nest)?;
                            output.flush()?;
                        }
                        if self.auto_indent {
                            write!(&mut output, "{:width$}", " ", width = nest.min(30) as usize)?;
                            output.flush()?;
                        }
                    }
                }

                match parser.parse_line()? {
                    Err(err) => {
                        writeln!(&mut error, "{}", err)?;
                        error.flush()?;
                        parser.clear(true);
                        codes.clear();
                    }
                    Ok(None) => return Ok(()),
                    Ok(Some(mut tmp_codes)) => codes.append(&mut tmp_codes),
                }
            }

            for code in &codes {
                match engine.eval(code.clone()) {
                    Err(err) => {
                        print_runtime_error(&mut error, &err, &engine.get_stack_trace())?;
                    }
                    Ok(result) => {
                        if self.show_result {
                            writeln!(&mut output, "{}", result.display(case, syntax))?;
                            output.flush()?;
                        }
                        match self.check_special_command(
                            &mut engine,
                            &mut output,
                            &mut error,
                            &result,
                        )? {
                            None => return Ok(()),
                            Some(updated) => {
                                if updated {
                                    case = engine.io_set.case;
                                    syntax = engine.io_set.syntax;
                                    self.bind_specials(&mut engine)?;
                                }
                            }
                        }
                    }
                }
            }
            codes.clear();
        }
    }
}

const CMD_QUIT: &str = "::quit";
const CMD_RESET: &str = "::reset";
const CMD_SCOPE: &str = "::scope";
const CMD_CASE: &str = "::case";
const CMD_SYNTAX: &str = "::syntax";
const CMD_PROMPT: &str = "::prompt";
const CMD_RESULT: &str = "::result";
const CMD_INDENT: &str = "::indent";
const CMD_LOAD: &str = "::load";

impl InteractiveFlags {
    fn check_special_command<RE, WE, EE, WI, EI>(
        &mut self,
        engine: &mut Engine<RE, WE, EE>,
        output: &mut WI,
        error: &mut EI,
        result: &Code,
    ) -> io::Result<Option<bool>>
    where
        RE: io::BufRead,
        WE: io::Write,
        EE: io::Write,
        WI: io::Write,
        EI: io::Write,
    {
        let mut updated = false;
        if let Some(s) = result.get_symbol() {
            match s.as_str() {
                CMD_QUIT => return Ok(None),
                CMD_RESET => {
                    engine.reset();
                    match self.preload(engine, error) {
                        Ok(()) => {
                            if self.show_result {
                                writeln!(output, "RESET: Success")?;
                                output.flush()?;
                            }
                        }
                        Err(err) => {
                            if self.show_result {
                                writeln!(error, "{}", err)?;
                                error.flush()?;
                                writeln!(output, "RESET: Failure")?;
                                output.flush()?;
                            }
                        }
                    }
                }
                _ => {}
            }
        } else if let Some((codes, _)) = result.split_at(2) {
            let target = codes[0].get_symbol();
            let value = codes[1].get_symbol();
            if let (Some(ref t), Some(ref v)) = (&target, &value) {
                match t.as_str() {
                    CMD_SCOPE => {
                        updated = true;
                        engine.set_scope(update(
                            &mut self.scope,
                            v,
                            &[Scope::Lexical, Scope::Dynamic],
                        ));
                        if self.show_result {
                            writeln!(output, "SCOPE: {:?}", self.scope)?;
                            output.flush()?;
                        }
                    }
                    CMD_CASE => {
                        updated = true;
                        update(&mut engine.io_set.case, v, &[Case::Upper, Case::Lower]);
                        if self.show_result {
                            writeln!(output, "CASE: {:?}", engine.io_set.case)?;
                            output.flush()?;
                        }
                    }
                    CMD_SYNTAX => {
                        updated = true;
                        update(
                            &mut engine.io_set.syntax,
                            v,
                            &[Syntax::Extend, Syntax::Shorten],
                        );
                        if self.show_result {
                            writeln!(output, "SYNTAX: {:?}", engine.io_set.syntax)?;
                            output.flush()?;
                        }
                    }
                    CMD_PROMPT => {
                        updated = true;
                        switching(&mut self.show_prompt, v);
                        if self.show_result {
                            writeln!(output, "PROMPT: {}", switch_str(self.show_prompt))?;
                            output.flush()?;
                        }
                    }
                    CMD_RESULT => {
                        updated = true;
                        switching(&mut self.show_result, v);
                        if self.show_result {
                            writeln!(output, "RESULT: {}", switch_str(self.show_result))?;
                            output.flush()?;
                        }
                    }
                    CMD_INDENT => {
                        updated = true;
                        switching(&mut self.auto_indent, v);
                        if self.show_result {
                            writeln!(output, "INDENT: {}", switch_str(self.auto_indent))?;
                            output.flush()?;
                        }
                    }
                    _ => {}
                }
            } else if let (Some(ref t), Some(ref s)) = (&target, &codes[1].get_str()) {
                if t.as_str() == CMD_LOAD {
                    match self.run_file(engine, error, s) {
                        Ok(_) => {
                            if self.show_result {
                                writeln!(output, "LOAD(\"{}\"): Success", s)?;
                                output.flush()?;
                            }
                        }
                        Err(err) => {
                            if self.show_result {
                                writeln!(error, "{}", err)?;
                                error.flush()?;
                                writeln!(output, "LOAD(\"{}\"): Failure", s)?;
                                output.flush()?;
                            }
                        }
                    }
                }
            } else if let Some(ref t) = &target {
                // この処理は要らんかも
                match t.as_str() {
                    CMD_CASE | CMD_INDENT | CMD_LOAD | CMD_PROMPT | CMD_QUIT | CMD_RESET
                    | CMD_RESULT | CMD_SCOPE | CMD_SYNTAX => {
                        if self.show_result {
                            writeln!(
                                error,
                                "invalid command argument: [ {} ]",
                                engine.io_set.case.apply(t)
                            )?;
                            error.flush()?;
                        }
                    }
                    _ => {}
                }
            }
        }
        Ok(Some(updated))
    }
}

impl InteractiveFlags {
    fn preload<RE, WE, EE, E>(
        &self,
        engine: &mut Engine<RE, WE, EE>,
        error: &mut E,
    ) -> io::Result<()>
    where
        RE: io::BufRead,
        WE: io::Write,
        EE: io::Write,
        E: io::Write,
    {
        if let Some(ref preload_file) = self.preload_file {
            self.run_file(engine, error, preload_file)
        } else {
            Ok(())
        }
    }
}

impl InteractiveFlags {
    fn run_file<RE, WE, EE, E>(
        &self,
        engine: &mut Engine<RE, WE, EE>,
        error: &mut E,
        file: &str,
    ) -> io::Result<()>
    where
        RE: io::BufRead,
        WE: io::Write,
        EE: io::Write,
        E: io::Write,
    {
        let file = std::fs::File::open(file)?;
        let reader = io::BufReader::new(file);
        let tokenizer = Tokenizer::new(reader);
        let mut parser = Parser::new(tokenizer);
        loop {
            let code = match parser.parse()? {
                Err(err) => {
                    writeln!(error, "{}", err)?;
                    error.flush()?;
                    return Err(io::Error::new(io::ErrorKind::Other, "parse error"));
                }
                Ok(None) => return Ok(()),
                Ok(Some(code)) => code,
            };
            if let Err(err) = engine.eval(code) {
                print_runtime_error(error, &err, &engine.get_stack_trace())?;
                return Err(io::Error::new(io::ErrorKind::Other, "runtime error"));
            }
        }
    }
}

impl InteractiveFlags {
    fn bind_specials<R, W, E>(&self, engine: &mut Engine<R, W, E>) -> io::Result<()>
    where
        R: io::BufRead,
        W: io::Write,
        E: io::Write,
    {
        let bind_set = vec![
            (CMD_QUIT, format!("(lambda () '{})", CMD_QUIT)),
            (CMD_RESET, format!("(lambda () '{})", CMD_RESET)),
            (
                CMD_LOAD,
                format!("(lambda (file) (list '{} file))", CMD_LOAD),
            ),
            (
                CMD_SCOPE,
                format!(
                    "(lambda ARGS (if (nil? ARGS) '{:?} (list '{} (car ARGS))))",
                    self.scope, CMD_SCOPE
                ),
            ),
            (
                CMD_CASE,
                format!(
                    "(lambda ARGS (if (nil? ARGS) '{:?} (list '{} (car ARGS))))",
                    engine.io_set.case, CMD_CASE
                ),
            ),
            (
                CMD_SYNTAX,
                format!(
                    "(lambda ARGS (if (nil? ARGS) '{:?} (list '{} (car ARGS))))",
                    engine.io_set.syntax, CMD_SYNTAX
                ),
            ),
            (
                CMD_PROMPT,
                format!(
                    "(lambda ARGS (if (nil? ARGS) '{} (list '{} (car ARGS))))",
                    switch_str(self.show_prompt),
                    CMD_PROMPT
                ),
            ),
            (
                CMD_RESULT,
                format!(
                    "(lambda ARGS (if (nil? ARGS) '{} (list '{} (car ARGS))))",
                    switch_str(self.show_result),
                    CMD_RESULT
                ),
            ),
            (
                CMD_INDENT,
                format!(
                    "(lambda ARGS (if (nil? ARGS) '{} (list '{} (car ARGS))))",
                    switch_str(self.auto_indent),
                    CMD_INDENT
                ),
            ),
        ];
        for (key, src) in bind_set {
            let tokenizer = Tokenizer::new(io::Cursor::new(src));
            let mut parser = Parser::new(tokenizer);
            match parser.parse()? {
                Err(err) => {
                    return Err(io::Error::new(
                        io::ErrorKind::Other,
                        err.to_string().as_str(),
                    ))
                }
                Ok(None) => return Ok(()),
                Ok(Some(code)) => {
                    if let Err(err) = engine.bind_special_code(key, code) {
                        return Err(io::Error::new(
                            io::ErrorKind::Other,
                            err.to_string().as_str(),
                        ));
                    }
                }
            }
        }
        Ok(())
    }
}

fn update<T>(m: &mut T, v: &str, ls: &[T]) -> T
where
    T: Copy + std::fmt::Debug,
{
    for n in ls.iter() {
        if v == format!("{:?}", n).to_ascii_lowercase().as_str() {
            *m = *n;
        }
    }
    *m
}

fn switching(v: &mut bool, flag: &str) {
    if flag == "on" {
        *v = true;
    } else if flag == "off" {
        *v = false;
    }
}

fn switch_str(v: bool) -> &'static str {
    if v {
        "on"
    } else {
        "off"
    }
}
