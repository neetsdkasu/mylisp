use std::cell::RefCell;
use std::io::{BufRead, Read, Result, Write};
use std::rc::Rc;

pub struct SharedIO<T> {
    file: Rc<RefCell<T>>,
    buf: Vec<u8>,
}

impl<T> SharedIO<T> {
    pub fn new(file: T) -> Self {
        Self {
            file: Rc::new(RefCell::new(file)),
            buf: vec![],
        }
    }
}

impl<T> Clone for SharedIO<T> {
    fn clone(&self) -> Self {
        Self {
            file: self.file.clone(),
            buf: vec![],
        }
    }
}

impl<T: Read> Read for SharedIO<T> {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize> {
        Read::read(&mut *self.file.borrow_mut(), buf)
    }
}

impl<T: Write> Write for SharedIO<T> {
    fn write(&mut self, buf: &[u8]) -> Result<usize> {
        Write::write(&mut *self.file.borrow_mut(), buf)
    }

    fn flush(&mut self) -> Result<()> {
        Write::flush(&mut *self.file.borrow_mut())
    }
}

impl<T: BufRead> BufRead for SharedIO<T> {
    fn consume(&mut self, amt: usize) {
        BufRead::consume(&mut *self.file.borrow_mut(), amt);
    }

    fn fill_buf(&mut self) -> Result<&[u8]> {
        let mut file = self.file.borrow_mut();
        let res = BufRead::fill_buf(&mut *file)?;
        self.buf.clear();
        self.buf.extend(res);
        Ok(&self.buf)
    }
}
