use mylisp_command_line as command_line;
use std::io;

fn main() -> io::Result<()> {
    match command_line::parse_command_line_flags() {
        Ok(mut command_line_flags) => command_line_flags.start(),
        Err(err) => {
            let usage = command_line::usage()?;
            eprintln!("{}", usage);
            Err(io::Error::new(io::ErrorKind::Other, err))
        }
    }
}
