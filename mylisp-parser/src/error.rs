use std::error;
use std::fmt;

use crate::tokenizer::{self, Position};

#[derive(Debug)]
pub struct Error {
    msg: Option<String>,
    src: Option<tokenizer::Error>,
    pos: Option<Position>,
}

impl error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.pos
            .as_ref()
            .map_or(Ok(()), |pos| write!(f, "[{:?}] ", pos))
            .and_then(|_| write!(f, "Syntax Error: "))
            .and_then(|_| self.msg.as_ref().map_or(Ok(()), |msg| msg.fmt(f)))
            .and_then(|_| self.src.as_ref().map_or(Ok(()), |src| src.fmt(f)))
    }
}

impl Error {
    pub(super) fn new(msg: String) -> Error {
        Error {
            msg: Some(msg),
            src: None,
            pos: None,
        }
    }
    pub(super) fn with_pos(msg: String, pos: Position) -> Error {
        Error {
            msg: Some(msg),
            src: None,
            pos: Some(pos),
        }
    }
    pub(super) fn from_src(src: tokenizer::Error, pos: Position) -> Error {
        Error {
            msg: None,
            src: Some(src),
            pos: Some(pos),
        }
    }
}
