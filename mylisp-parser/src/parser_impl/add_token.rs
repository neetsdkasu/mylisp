use std::rc::Rc;
use std::result::Result;

use crate::code::Code;
use crate::tokenizer::{Position, Token};

use super::CodeFragment;
use super::Error;
use super::Parser;

type AddTokenResult = Result<Option<Rc<Code>>, Error>;
type MakeCodeResult = Result<Rc<Code>, Error>;

impl<R> Parser<R> {
    pub fn add_token(&mut self, token: Token, pos: Position) -> AddTokenResult {
        let mut code = match token {
            Token::Word(word) => self.make_symbol(word),
            Token::Str(value) => Code::Str(Box::new(value)).rc(),
            Token::Int(src) => parse_int(src, 10, &pos)?,
            Token::IntBin(src) => parse_int(src, 2, &pos)?,
            Token::IntHex(src) => parse_int(src, 16, &pos)?,
            Token::Dec(src) => parse_float(src, &pos)?,
            Token::ConsDot => return self.set_reqcons(pos),
            Token::Quote | Token::BackQuote | Token::Comma | Token::CommaAt => {
                return self.set_nest_fragment(token, pos)
            }
            Token::OpenBracket => return self.set_open_bracket(pos),
            Token::CloseBracket => self.close_bracket(&pos)?,
            Token::NewLine { comment: _ } => {
                return Err(Error::with_pos(bug!("with {:?}", token), pos))
            }
        };
        loop {
            let fragment = match self.fragment_stack.pop() {
                Some(fragment) => fragment,
                None => return Ok(Some(code)),
            };
            use CodeFragment::*;
            match fragment {
                Cons(_, _) => {
                    return Err(Error::with_pos(bug!("add_token {:?}", code), pos));
                }
                ReqCons(car) => {
                    self.fragment_stack.push(Cons(car, code));
                    return Ok(None);
                }
                ReqChild(Token::BackQuote) => {
                    code = self.set_child(fragment, code)?;
                }
                ReqChild(Token::Comma) | ReqChild(Token::CommaAt) => {
                    code = self.set_child(fragment, code)?;
                }
                ReqChild(_) => {
                    code = self.set_child(fragment, code)?;
                }
                Car(_) | OpenBracket => {
                    self.fragment_stack.push(fragment);
                    self.fragment_stack.push(Car(code));
                    return Ok(None);
                }
            }
        }
    }
}

fn increment(target: &mut u32) -> bool {
    target
        .checked_add(1)
        .map(|new_count| *target = new_count)
        .is_some()
}

fn decrement(target: &mut u32) -> bool {
    target
        .checked_sub(1)
        .map(|new_count| *target = new_count)
        .is_some()
}

impl<R> Parser<R> {
    fn increment_bracket_count(&mut self) -> bool {
        increment(&mut self.bracket_count)
    }

    fn decrement_bracket_count(&mut self) -> bool {
        decrement(&mut self.bracket_count)
    }
}

impl<R> Parser<R> {
    fn make_symbol(&self, word: String) -> Rc<Code> {
        self.constants
            .get(&word)
            .unwrap_or_else(|| Code::Symbol(Rc::new(word)).rc())
    }
}

fn parse_int(src: String, radix: u32, pos: &Position) -> MakeCodeResult {
    if let Ok(value) = i32::from_str_radix(&src, radix) {
        return Ok(Code::Int32(value).rc());
    }
    if let Ok(value) = i64::from_str_radix(&src, radix) {
        return Ok(Code::Int64(Box::new(value)).rc());
    }
    if radix == 10 {
        if let Ok(value) = src.parse::<f64>() {
            return Ok(Code::Float64(Box::new(value)).rc());
        }
    }
    Err(Error::with_pos(
        format!("integer too big ({})", src),
        pos.clone(),
    ))
}

fn parse_float(src: String, pos: &Position) -> MakeCodeResult {
    if let Ok(value) = src.parse::<f64>() {
        return Ok(Code::Float64(Box::new(value)).rc());
    }
    Err(Error::with_pos(
        format!("floating-point value too big ({})", src),
        pos.clone(),
    ))
}

impl<R> Parser<R> {
    fn set_reqcons(&mut self, pos: Position) -> AddTokenResult {
        match self.fragment_stack.pop() {
            Some(CodeFragment::Car(car)) => {
                let fragment = CodeFragment::ReqCons(car);
                self.fragment_stack.push(fragment);
                Ok(None)
            }
            _ => Err(Error::with_pos(
                "invalid position of cons-dot".to_string(),
                pos,
            )),
        }
    }
}

impl<R> Parser<R> {
    fn set_nest_fragment(&mut self, token: Token, pos: Position) -> AddTokenResult {
        if !matches!(
            token,
            Token::Quote | Token::BackQuote | Token::Comma | Token::CommaAt
        ) {
            return Err(Error::with_pos(
                bug!("set_nest_fragment({:?},)", token),
                pos,
            ));
        }
        self.fragment_stack.push(CodeFragment::ReqChild(token));
        Ok(None)
    }
}

impl<R> Parser<R> {
    fn set_open_bracket(&mut self, pos: Position) -> AddTokenResult {
        if !self.increment_bracket_count() {
            return Err(Error::with_pos("too many open bracket".to_string(), pos));
        }
        self.fragment_stack.push(CodeFragment::OpenBracket);
        Ok(None)
    }
}

impl<R> Parser<R> {
    fn close_bracket(&mut self, pos: &Position) -> MakeCodeResult {
        if !self.decrement_bracket_count() {
            return Err(Error::with_pos(
                "invalid close bracket".to_string(),
                pos.clone(),
            ));
        }
        let fragment = match self.fragment_stack.pop() {
            Some(fragment) => fragment,
            None => {
                return Err(Error::with_pos(
                    bug!("close_bracket(fragment stack is empty)"),
                    pos.clone(),
                ))
            }
        };
        use CodeFragment::*;
        let mut code = match fragment {
            OpenBracket => return Ok(self.constants.nil()),
            Car(car) => Code::cons(car, self.constants.nil()),
            Cons(car, cdr) => Code::cons(car, cdr),
            ReqCons(_) | ReqChild(_) => {
                return Err(Error::with_pos("require value".to_string(), pos.clone()))
            }
        };
        loop {
            match self.fragment_stack.pop() {
                Some(OpenBracket) => break,
                Some(Car(car)) => code = Code::cons(car, code),
                _ => {
                    return Err(Error::with_pos(
                        bug!("close_bracket(less fragment)"),
                        pos.clone(),
                    ))
                }
            }
        }
        Ok(code)
    }
}

impl<R> Parser<R> {
    fn set_child(&self, fragment: CodeFragment, code: Rc<Code>) -> MakeCodeResult {
        use CodeFragment::ReqChild;
        let car = match fragment {
            ReqChild(Token::Quote) => self.constants.quote(),
            ReqChild(Token::BackQuote) => self.constants.quasiquote(),
            ReqChild(Token::Comma) => self.constants.unquote(),
            ReqChild(Token::CommaAt) => {
                if code.is_value() {
                    return Err(Error::new(format!(
                        ",@ require non-value type ({:?})",
                        code
                    )));
                }
                self.constants.unquote_splicing()
            }
            _ => {
                return Err(Error::new(bug!(
                    "CodeFragment.set_child({:?} {:?})",
                    fragment,
                    code,
                )));
            }
        };
        let cdr = Code::cons(code, self.constants.nil());
        Ok(Code::cons(car, cdr))
    }
}
