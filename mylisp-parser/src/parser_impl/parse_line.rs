use std::io;

use super::Error;
use super::ParseLineResult;
use super::Parser;

impl<R: io::BufRead> Parser<R> {
    #[allow(dead_code)]
    pub fn parse_line(&mut self) -> ParseLineResult {
        let mut ret = vec![];
        while let Some((res, pos)) = self.tokenizer.read_token()? {
            match res {
                Err(err) => return Ok(Err(Error::from_src(err, pos))),
                Ok(token) if token.is_new_line() => {
                    return Ok(Ok(Some(ret)));
                }
                Ok(token) => match self.add_token(token, pos) {
                    Err(err) => return Ok(Err(err)),
                    Ok(Some(code)) => ret.push(code),
                    _ => {}
                },
            }
        }
        if self.fragment_stack.is_empty() {
            if !ret.is_empty() {
                Ok(Ok(Some(ret)))
            } else {
                Ok(Ok(None))
            }
        } else {
            Ok(Err(Error::new(format!(
                "less close brackets! (require {})",
                self.bracket_count
            ))))
        }
    }
}
