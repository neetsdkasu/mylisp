use std::collections::HashMap;
use std::rc::Rc;

use crate::builtin_func::BuiltinFunc;
use crate::code::Code;
use crate::io_func::IOFunc;
use crate::special_form::SpecialForm;

pub(super) struct Constants {
    code_nil: Rc<Code>,
    code_symbol_quote: Rc<Code>,
    code_symbol_quasiquote: Rc<Code>,
    code_symbol_unquote: Rc<Code>,
    code_symbol_unquote_splicing: Rc<Code>,
    symbols: HashMap<&'static str, Rc<Code>>,
}

impl Constants {
    pub(super) fn new() -> Constants {
        let mut symbols = HashMap::new();
        for key in SpecialForm::iter().map(SpecialForm::symbol) {
            symbols.insert(key, Code::make_symbol(key));
        }
        for key in BuiltinFunc::iter().map(BuiltinFunc::symbol) {
            symbols.insert(key, Code::make_symbol(key));
        }
        for key in IOFunc::iter().map(IOFunc::symbol) {
            symbols.insert(key, Code::make_symbol(key));
        }
        for key in &["t"] {
            symbols.insert(key, Code::make_symbol(key));
        }
        symbols.insert("true", Code::True.rc());
        symbols.insert("false", Code::False.rc());
        let code_nil = Code::Nil.rc();
        symbols.insert("nil", code_nil.clone());
        let get = |sym| {
            symbols
                .get(sym)
                .map(Rc::clone)
                .unwrap_or_else(|| Code::make_symbol(sym))
        };
        let code_symbol_quote = get(SpecialForm::Quote.symbol());
        let code_symbol_quasiquote = get(SpecialForm::Quasiquote.symbol());
        let code_symbol_unquote = get(SpecialForm::Unquote.symbol());
        let code_symbol_unquote_splicing = get(SpecialForm::UnquoteSplicing.symbol());
        Constants {
            code_nil,
            code_symbol_quote,
            code_symbol_quasiquote,
            code_symbol_unquote,
            code_symbol_unquote_splicing,
            symbols,
        }
    }

    pub(super) fn get(&self, s: &str) -> Option<Rc<Code>> {
        self.symbols.get(s).map(Rc::clone)
    }

    pub(super) fn nil(&self) -> Rc<Code> {
        self.code_nil.clone()
    }

    pub(super) fn quote(&self) -> Rc<Code> {
        self.code_symbol_quote.clone()
    }

    pub(super) fn quasiquote(&self) -> Rc<Code> {
        self.code_symbol_quasiquote.clone()
    }

    pub(super) fn unquote(&self) -> Rc<Code> {
        self.code_symbol_unquote.clone()
    }

    pub(super) fn unquote_splicing(&self) -> Rc<Code> {
        self.code_symbol_unquote_splicing.clone()
    }
}
