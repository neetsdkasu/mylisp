use std::io;

use super::Error;
use super::ParseResult;
use super::Parser;

impl<R: io::BufRead> Parser<R> {
    #[allow(dead_code)]
    pub fn parse(&mut self) -> ParseResult {
        while let Some((res, pos)) = self.tokenizer.read_token()? {
            match res {
                Err(err) => return Ok(Err(Error::from_src(err, pos))),
                Ok(token) if token.is_new_line() => {}
                Ok(token) => match self.add_token(token, pos) {
                    Err(err) => return Ok(Err(err)),
                    Ok(Some(code)) => return Ok(Ok(Some(code))),
                    _ => {}
                },
            }
        }
        if self.fragment_stack.is_empty() {
            Ok(Ok(None))
        } else {
            Ok(Err(Error::new(format!(
                "less close brackets! (require {})",
                self.bracket_count
            ))))
        }
    }
}
