use crate::tokenizer::Tokenizer;

use super::CodeFragment;
use super::Constants;

pub struct Parser<R> {
    pub(super) tokenizer: Tokenizer<R>,
    pub(super) fragment_stack: Vec<CodeFragment>,
    pub(super) bracket_count: u32,
    pub(super) constants: Constants,
}

impl<R> Parser<R> {
    pub fn new(tokenizer: Tokenizer<R>) -> Parser<R> {
        Parser {
            tokenizer,
            fragment_stack: vec![],
            bracket_count: 0,
            constants: Constants::new(),
        }
    }

    #[allow(dead_code)]
    pub fn clear(&mut self, skip_to_eol: bool) {
        self.fragment_stack.clear();
        self.bracket_count = 0;
        if skip_to_eol {
            self.tokenizer.skip_to_eol();
        }
    }

    #[allow(dead_code)]
    pub fn current_nest(&self) -> u32 {
        self.bracket_count
    }
}
