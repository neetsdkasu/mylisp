use std::io;
use std::rc::Rc;
use std::result;

use super::error::Error;
use crate::code::Code;

pub type ParseResult = io::Result<result::Result<Option<Rc<Code>>, Error>>;

pub type ParseLineResult = io::Result<result::Result<Option<Vec<Rc<Code>>>, Error>>;
