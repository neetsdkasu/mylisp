use std::rc::Rc;

use crate::code::Code;
use crate::tokenizer::Token;

#[derive(Debug)]
pub(super) enum CodeFragment {
    OpenBracket,
    Car(Rc<Code>),
    ReqCons(Rc<Code>),
    Cons(Rc<Code>, Rc<Code>),
    ReqChild(Token),
}
