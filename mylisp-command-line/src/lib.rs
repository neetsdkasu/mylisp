use std::io;

use mylisp_code as code;
use mylisp_engine as engine;
use mylisp_interactive::InteractiveFlags;
use mylisp_io::SharedIO;
use mylisp_machine_interactive as machine_interactive;
use mylisp_machine_runner as machine_runner;
use mylisp_runner::RunnerFlags;

// FLAGS
// engine setting
const FLAG_SCOPE_LEXICAL: &str = "--lexical-scope";
const FLAG_SCOPE_DYNAMIC: &str = "--dynamic-scope";
const FLAG_INPUT_FILE: &str = "--input-file";
const FLAG_OUTPUT_FILE: &str = "--output-file";
const FLAG_ERROR_FILE: &str = "--error-file";
// display setting
const FLAG_CASE_UPPER: &str = "--upper-case";
const FLAG_CASE_LOWER: &str = "--lower-case";
const FLAG_SYNTAX_EXTEND: &str = "--extend-syntax";
const FLAG_SYNTAX_SHORTEN: &str = "--shorten-syntax";
// common setting
const FLAG_PRELOAD_FILE: &str = "--preload-file";
const FLAG_MACHINE_MODE: &str = "--machine-mode";
// file run setting
const FLAG_SOURCE_FILE: &str = "--source-file";
// interactive setting
const FLAG_INTERACTIVE: &str = "--interactive";
const FLAG_AUTO_INDENT: &str = "--auto-indent";
const FLAG_SHOW_PROMPT: &str = "--show-prompt";
const FLAG_SHOW_RESULT: &str = "--show-result";

// SHORT-FLAGS
// engine setting
const SHORT_FLAG_SCOPE_LEXICAL: &str = "-lexical";
const SHORT_FLAG_SCOPE_DYNAMIC: &str = "-dynamic";
const SHORT_FLAG_INPUT_FILE: &str = "-fin";
const SHORT_FLAG_OUTPUT_FILE: &str = "-fout";
const SHORT_FLAG_ERROR_FILE: &str = "-ferr";
// display setting
const SHORT_FLAG_CASE_UPPER: &str = "-upper";
const SHORT_FLAG_CASE_LOWER: &str = "-lower";
const SHORT_FLAG_SYNTAX_EXTEND: &str = "-extend";
const SHORT_FLAG_SYNTAX_SHORTEN: &str = "-shorten";
// common setting
const SHORT_FLAG_PRELOAD_FILE: &str = "-preload";
const SHORT_FLAG_MACHINE_MODE: &str = "-machine";
// file run setting
const SHORT_FLAG_SOURCE_FILE: &str = "-src";
// interactive setting
const SHORT_FLAG_INTERACTIVE: &str = "-i";
const SHORT_FLAG_AUTO_INDENT: &str = "-indent";
const SHORT_FLAG_SHOW_PROMPT: &str = "-prompt";
const SHORT_FLAG_SHOW_RESULT: &str = "-result";

pub struct CommandLineFlags {
    interactive_mode: bool,
    machine_mode: bool,
    auto_indent: bool,
    show_prompt: bool,
    show_result: bool,
    case: code::Case,
    syntax: code::Syntax,
    scope: engine::Scope,
    preload_file: Option<String>,
    source_file: Option<String>,
    input_file: Option<String>,
    output_file: Option<String>,
    error_file: Option<String>,
}

pub fn usage() -> io::Result<String> {
    const FILE_NAME: &str = "<file_name>";
    let main_flags = &[
        (
            FLAG_SOURCE_FILE,
            SHORT_FLAG_SOURCE_FILE,
            FILE_NAME,
            "FileRun Mode",
        ),
        (
            FLAG_INTERACTIVE,
            SHORT_FLAG_INTERACTIVE,
            "",
            "Interactive Mode",
        ),
    ];
    let common_flags = &[
        (
            FLAG_PRELOAD_FILE,
            SHORT_FLAG_PRELOAD_FILE,
            "<file_name>",
            "preload program and run it before main program",
        ),
        (
            FLAG_SCOPE_DYNAMIC,
            SHORT_FLAG_SCOPE_DYNAMIC,
            "",
            "Use Dynamic scope (default)",
        ),
        (
            FLAG_SCOPE_LEXICAL,
            SHORT_FLAG_SCOPE_LEXICAL,
            "",
            "Use Lexical scope",
        ),
        (
            FLAG_CASE_UPPER,
            SHORT_FLAG_CASE_UPPER,
            "",
            "Use Upper case to show symbols (default)",
        ),
        (
            FLAG_CASE_LOWER,
            SHORT_FLAG_CASE_LOWER,
            "",
            "Use Lower case to show symbols",
        ),
        (
            FLAG_SYNTAX_EXTEND,
            SHORT_FLAG_SYNTAX_EXTEND,
            "",
            "show codes without quote operators (default)",
        ),
        (
            FLAG_SYNTAX_SHORTEN,
            SHORT_FLAG_SYNTAX_SHORTEN,
            "",
            "show codes with quote operators",
        ),
        (
            FLAG_INPUT_FILE,
            SHORT_FLAG_INPUT_FILE,
            "<file_name>",
            "use file for program-input (ex. readln)",
        ),
        (
            FLAG_OUTPUT_FILE,
            SHORT_FLAG_OUTPUT_FILE,
            "<file_name>",
            "use file for program-output (ex. println)",
        ),
        (
            FLAG_ERROR_FILE,
            SHORT_FLAG_ERROR_FILE,
            "<file_name>",
            "use file for program-error-output (ex. eprintln)",
        ),
        (
            FLAG_MACHINE_MODE,
            SHORT_FLAG_MACHINE_MODE,
            "",
            "run on machine-mode (require Lexical scope flag)",
        ),
    ];
    let interactive_flags = &[
        (
            FLAG_SHOW_PROMPT,
            SHORT_FLAG_SHOW_PROMPT,
            "<on/off>",
            "show prompt (default on)",
        ),
        (
            FLAG_SHOW_RESULT,
            SHORT_FLAG_SHOW_RESULT,
            "<on/off>",
            "show run-result by each codes (default on)",
        ),
        (
            FLAG_AUTO_INDENT,
            SHORT_FLAG_AUTO_INDENT,
            "<on/off>",
            "auto indent by brackets (default on)",
        ),
    ];
    use io::Write;
    let mut ret = vec![];
    writeln!(&mut ret, "usage:")?;
    let program_path = std::env::current_exe()?;
    if let Some(file_name) = program_path.file_name() {
        if let Some(file_name) = file_name.to_str() {
            writeln!(
                &mut ret,
                "    {} {} {} [COMMON-FLAGS]",
                file_name, FLAG_INTERACTIVE, FILE_NAME
            )?;
            writeln!(
                &mut ret,
                "    {} {} [COMMON-FLAGS] [INTERACTIVE-FLAGS]",
                file_name, FLAG_SOURCE_FILE
            )?;
        }
    }
    for (flag, short_flag, flag_value, description) in main_flags.iter() {
        writeln!(&mut ret)?;
        writeln!(
            &mut ret,
            "   {0} {2} | {1} {2}",
            flag, short_flag, flag_value,
        )?;
        writeln!(&mut ret, "       {}", description)?;
    }
    writeln!(&mut ret)?;
    writeln!(&mut ret, "[COMMON-FLAGS]")?;
    for (flag, short_flag, flag_value, description) in common_flags.iter() {
        writeln!(&mut ret)?;
        writeln!(
            &mut ret,
            "   {0} {2} | {1} {2}",
            flag, short_flag, flag_value,
        )?;
        writeln!(&mut ret, "       {}", description)?;
    }
    writeln!(&mut ret)?;
    writeln!(&mut ret, "[INTERACTIVE-FLAGS]")?;
    for (flag, short_flag, flag_value, description) in interactive_flags.iter() {
        writeln!(&mut ret)?;
        writeln!(
            &mut ret,
            "   {0} {2} | {1} {2}",
            flag, short_flag, flag_value,
        )?;
        writeln!(&mut ret, "       {}", description)?;
    }

    String::from_utf8(ret).map_err(|err| io::Error::new(io::ErrorKind::Other, err.to_string()))
}

pub fn parse_command_line_flags() -> Result<CommandLineFlags, String> {
    let mut command_line_flags = CommandLineFlags {
        interactive_mode: false,
        machine_mode: false,
        auto_indent: true,
        show_prompt: true,
        show_result: true,
        case: code::Case::Upper,
        syntax: code::Syntax::Extend,
        scope: engine::Scope::Dynamic,
        preload_file: None,
        source_file: None,
        input_file: None,
        output_file: None,
        error_file: None,
    };

    let args = std::env::args();
    let mut args = args.skip(1);

    while let Some(arg) = args.next() {
        match arg.as_str() {
            FLAG_INTERACTIVE | SHORT_FLAG_INTERACTIVE => command_line_flags.interactive_mode = true,
            FLAG_MACHINE_MODE | SHORT_FLAG_MACHINE_MODE => command_line_flags.machine_mode = true,
            FLAG_SCOPE_LEXICAL | SHORT_FLAG_SCOPE_LEXICAL => {
                command_line_flags.scope = engine::Scope::Lexical
            }
            FLAG_SCOPE_DYNAMIC | SHORT_FLAG_SCOPE_DYNAMIC => {
                command_line_flags.scope = engine::Scope::Dynamic
            }
            FLAG_CASE_UPPER | SHORT_FLAG_CASE_UPPER => command_line_flags.case = code::Case::Upper,
            FLAG_CASE_LOWER | SHORT_FLAG_CASE_LOWER => command_line_flags.case = code::Case::Lower,
            FLAG_SYNTAX_EXTEND | SHORT_FLAG_SYNTAX_EXTEND => {
                command_line_flags.syntax = code::Syntax::Extend
            }
            FLAG_SYNTAX_SHORTEN | SHORT_FLAG_SYNTAX_SHORTEN => {
                command_line_flags.syntax = code::Syntax::Shorten
            }
            FLAG_AUTO_INDENT | SHORT_FLAG_AUTO_INDENT => {
                command_line_flags.auto_indent = check_on_off(args.next())?
            }
            FLAG_SHOW_PROMPT | SHORT_FLAG_SHOW_PROMPT => {
                command_line_flags.show_prompt = check_on_off(args.next())?
            }
            FLAG_SHOW_RESULT | SHORT_FLAG_SHOW_RESULT => {
                command_line_flags.show_result = check_on_off(args.next())?
            }
            FLAG_PRELOAD_FILE | SHORT_FLAG_PRELOAD_FILE => {
                command_line_flags.preload_file = get_string(args.next())?
            }
            FLAG_SOURCE_FILE | SHORT_FLAG_SOURCE_FILE => {
                command_line_flags.source_file = get_string(args.next())?
            }
            FLAG_INPUT_FILE | SHORT_FLAG_INPUT_FILE => {
                command_line_flags.input_file = get_string(args.next())?
            }
            FLAG_OUTPUT_FILE | SHORT_FLAG_OUTPUT_FILE => {
                command_line_flags.output_file = get_string(args.next())?
            }
            FLAG_ERROR_FILE | SHORT_FLAG_ERROR_FILE => {
                command_line_flags.error_file = get_string(args.next())?
            }
            _ => return Err(format!("UNKNWON FLAG [ {} ]", arg)),
        }
    }

    if command_line_flags.interactive_mode == command_line_flags.source_file.is_some() {
        return Err(format!(
            "NEED {} OR {} FLAG",
            FLAG_SOURCE_FILE, FLAG_INTERACTIVE
        ));
    }

    if command_line_flags.machine_mode
        && !matches!(command_line_flags.scope, engine::Scope::Lexical)
    {
        return Err(format!("MACHINE-MODE NEED {} FLAG", FLAG_SCOPE_LEXICAL));
    }

    Ok(command_line_flags)
}

fn get_string(s: Option<String>) -> Result<Option<String>, String> {
    if s.is_some() {
        Ok(s)
    } else {
        Err("MISSING <filename>".to_string())
    }
}

fn check_on_off(s: Option<String>) -> Result<bool, String> {
    match s {
        Some(flag) if flag == "on" => Ok(true),
        Some(flag) if flag == "off" => Ok(false),
        _ => Err(r#"FLAG VALUE NEED "on" OR "off""#.to_string()),
    }
}

impl CommandLineFlags {
    pub fn start(&mut self) -> io::Result<()> {
        let stdin = io::stdin();
        let stdin = stdin.lock();
        let stdin = SharedIO::new(stdin);

        let stdout = io::stdout();
        let stdout = SharedIO::new(stdout);

        let stderr = io::stderr();
        let stderr = SharedIO::new(stderr);

        let io_set_for_engine = engine::IOSet {
            input: stdin.clone(),
            output: stdout.clone(),
            error: stderr.clone(),
            case: self.case,
            syntax: self.syntax,
        };

        if !self.interactive_mode {
            let none: Option<engine::IOSet<io::Empty, io::Sink, io::Sink>> = None;
            return self.bind_input(io_set_for_engine, none);
        }

        let io_set_for_interactive = engine::IOSet {
            input: stdin,
            output: stdout,
            error: stderr,
            case: io_set_for_engine.case,
            syntax: io_set_for_engine.syntax,
        };

        self.bind_input(io_set_for_engine, Some(io_set_for_interactive))
    }

    fn bind_input<RI, WI, EI, RE, WE, EE>(
        &mut self,
        io_set_for_engine: engine::IOSet<RE, WE, EE>,
        io_set_for_interactive: Option<engine::IOSet<RI, WI, EI>>,
    ) -> io::Result<()>
    where
        RI: io::BufRead,
        WI: io::Write,
        EI: io::Write,
        RE: io::BufRead,
        WE: io::Write,
        EE: io::Write,
    {
        if let Some(input_file) = self.input_file.take() {
            let file = std::fs::File::open(input_file)?;
            let io_set_for_engine = engine::IOSet {
                input: io::BufReader::new(file),
                output: io_set_for_engine.output,
                error: io_set_for_engine.error,
                case: io_set_for_engine.case,
                syntax: io_set_for_engine.syntax,
            };
            self.bind_output(io_set_for_engine, io_set_for_interactive)
        } else {
            self.bind_output(io_set_for_engine, io_set_for_interactive)
        }
    }

    fn bind_output<RI, WI, EI, RE, WE, EE>(
        &mut self,
        io_set_for_engine: engine::IOSet<RE, WE, EE>,
        io_set_for_interactive: Option<engine::IOSet<RI, WI, EI>>,
    ) -> io::Result<()>
    where
        RI: io::BufRead,
        WI: io::Write,
        EI: io::Write,
        RE: io::BufRead,
        WE: io::Write,
        EE: io::Write,
    {
        if let Some(output_file) = self.output_file.take() {
            let file = std::fs::File::open(output_file)?;
            let io_set_for_engine = engine::IOSet {
                input: io_set_for_engine.input,
                output: file,
                error: io_set_for_engine.error,
                case: io_set_for_engine.case,
                syntax: io_set_for_engine.syntax,
            };
            self.bind_error(io_set_for_engine, io_set_for_interactive)
        } else {
            self.bind_error(io_set_for_engine, io_set_for_interactive)
        }
    }

    fn bind_error<RI, WI, EI, RE, WE, EE>(
        &mut self,
        io_set_for_engine: engine::IOSet<RE, WE, EE>,
        io_set_for_interactive: Option<engine::IOSet<RI, WI, EI>>,
    ) -> io::Result<()>
    where
        RI: io::BufRead,
        WI: io::Write,
        EI: io::Write,
        RE: io::BufRead,
        WE: io::Write,
        EE: io::Write,
    {
        if let Some(error_file) = self.error_file.take() {
            let file = std::fs::File::open(error_file)?;
            let io_set_for_engine = engine::IOSet {
                input: io_set_for_engine.input,
                output: io_set_for_engine.output,
                error: file,
                case: io_set_for_engine.case,
                syntax: io_set_for_engine.syntax,
            };
            self.run(io_set_for_engine, io_set_for_interactive)
        } else {
            self.run(io_set_for_engine, io_set_for_interactive)
        }
    }

    fn run<RI, WI, EI, RE, WE, EE>(
        &mut self,
        io_set_for_engine: engine::IOSet<RE, WE, EE>,
        io_set_for_interactive: Option<engine::IOSet<RI, WI, EI>>,
    ) -> io::Result<()>
    where
        RI: io::BufRead,
        WI: io::Write,
        EI: io::Write,
        RE: io::BufRead,
        WE: io::Write,
        EE: io::Write,
    {
        if let Some(io_set_for_interactive) = io_set_for_interactive {
            if self.machine_mode {
                self.run_interactive_on_machine(io_set_for_engine, io_set_for_interactive)
            } else {
                self.run_interactive(io_set_for_engine, io_set_for_interactive)
            }
        } else if self.machine_mode {
            self.run_file_on_machine(io_set_for_engine)
        } else {
            self.run_file(io_set_for_engine)
        }
    }

    fn run_interactive<RI, WI, EI, RE, WE, EE>(
        &mut self,
        io_set_for_engine: engine::IOSet<RE, WE, EE>,
        io_set_for_interactive: engine::IOSet<RI, WI, EI>,
    ) -> io::Result<()>
    where
        RI: io::BufRead,
        WI: io::Write,
        EI: io::Write,
        RE: io::BufRead,
        WE: io::Write,
        EE: io::Write,
    {
        let interactive_flags = InteractiveFlags {
            show_prompt: self.show_prompt,
            show_result: self.show_result,
            auto_indent: self.auto_indent,
            scope: self.scope,
            preload_file: self.preload_file.take(),
        };

        interactive_flags.start(io_set_for_interactive, io_set_for_engine)
    }

    fn run_interactive_on_machine<RI, WI, EI, RE, WE, EE>(
        &mut self,
        io_set_for_machine: engine::IOSet<RE, WE, EE>,
        io_set_for_interactive: engine::IOSet<RI, WI, EI>,
    ) -> io::Result<()>
    where
        RI: io::BufRead,
        WI: io::Write,
        EI: io::Write,
        RE: io::BufRead,
        WE: io::Write,
        EE: io::Write,
    {
        let interactive_flags = machine_interactive::InteractiveFlags {
            show_prompt: self.show_prompt,
            show_result: self.show_result,
            auto_indent: self.auto_indent,
            preload_file: self.preload_file.take(),
        };

        interactive_flags.start(io_set_for_interactive, io_set_for_machine)
    }

    fn run_file<RE, WE, EE>(
        &mut self,
        io_set_for_engine: engine::IOSet<RE, WE, EE>,
    ) -> io::Result<()>
    where
        RE: io::BufRead,
        WE: io::Write,
        EE: io::Write,
    {
        let source_file = self
            .source_file
            .take()
            .ok_or_else(|| io::Error::new(io::ErrorKind::Other, "bug in run_file"))?;

        let runner_flags = RunnerFlags {
            source_file,
            scope: self.scope,
            preload_file: self.preload_file.take(),
        };

        runner_flags.start(io_set_for_engine)
    }

    fn run_file_on_machine<RE, WE, EE>(
        &mut self,
        io_set_for_machine: engine::IOSet<RE, WE, EE>,
    ) -> io::Result<()>
    where
        RE: io::BufRead,
        WE: io::Write,
        EE: io::Write,
    {
        let source_file = self
            .source_file
            .take()
            .ok_or_else(|| io::Error::new(io::ErrorKind::Other, "bug in run_file"))?;

        let runner_flags = machine_runner::RunnerFlags {
            source_file,
            preload_file: self.preload_file.take(),
        };

        runner_flags.start(io_set_for_machine)
    }
}
