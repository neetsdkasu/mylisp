#[derive(Debug)]
pub enum Token {
    NewLine { comment: Option<String> },
    BackQuote,
    CloseBracket,
    Comma,
    CommaAt,
    ConsDot,
    Dec(String),
    Int(String),
    IntBin(String),
    IntHex(String),
    OpenBracket,
    Quote,
    Str(String),
    Word(String),
}

impl Token {
    pub(super) fn new_line() -> Token {
        Token::NewLine { comment: None }
    }

    pub(super) fn new_line_with_comment(comment: String) -> Token {
        Token::NewLine {
            comment: Some(comment),
        }
    }

    pub fn is_new_line(&self) -> bool {
        matches!(self, Token::NewLine { comment: _ })
    }
}
