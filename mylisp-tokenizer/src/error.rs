use std::error;
use std::fmt;

#[derive(Debug)]
pub struct Error {
    msg: &'static str,
}

impl error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Token Error: {}", self.msg)
    }
}

impl Error {
    pub(super) fn new(msg: &'static str) -> Error {
        Error { msg }
    }
}
