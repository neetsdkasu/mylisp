use std::io::{self, BufRead};

use super::Tokenizer;

impl<R: BufRead> Tokenizer<R> {
    pub(super) fn read_line(&mut self) -> io::Result<()> {
        self.buf.clear();
        self.tokenizer.read_line(&mut self.buf)?;
        self.chars.clear();
        self.chars.extend(self.buf.chars());
        self.head = 0;
        self.tail = 0;
        if !self.is_eof() {
            self.line_count += 1;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use std::io;

    use super::Tokenizer;

    #[test]
    fn test_read_line() {
        let buf: &[_] = b"abc \n\nefg\r\nhij";

        let mut tokenizer = Tokenizer::new(buf);

        assert!(tokenizer.read_line().is_ok());
        assert_eq!(tokenizer.buf, "abc \n");
        assert_eq!(tokenizer.chars, ['a', 'b', 'c', ' ', '\n']);
        assert_eq!(tokenizer.line_count, 1);

        assert!(tokenizer.read_line().is_ok());
        assert_eq!(tokenizer.buf, "\n");
        assert_eq!(tokenizer.chars, ['\n']);
        assert_eq!(tokenizer.line_count, 2);

        assert!(tokenizer.read_line().is_ok());
        assert_eq!(tokenizer.buf, "efg\r\n");
        assert_eq!(tokenizer.chars, ['e', 'f', 'g', '\r', '\n']);
        assert_eq!(tokenizer.line_count, 3);

        assert!(tokenizer.read_line().is_ok());
        assert_eq!(tokenizer.buf, "hij");
        assert_eq!(tokenizer.chars, ['h', 'i', 'j']);
        assert_eq!(tokenizer.line_count, 4);

        assert!(tokenizer.read_line().is_ok());
        assert!(tokenizer.buf.is_empty());
        assert_eq!(tokenizer.line_count, 4);
    }

    struct ReadErr;

    impl io::Read for ReadErr {
        fn read(&mut self, _buf: &mut [u8]) -> io::Result<usize> {
            Err(io::Error::new(io::ErrorKind::Other, ""))
        }
    }

    #[test]
    fn test_read_line_with_io_error() {
        let mut tokenizer = Tokenizer::new(io::BufReader::new(ReadErr));

        assert!(tokenizer.read_line().is_err());
    }
}
