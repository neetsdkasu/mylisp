use super::Tokenizer;

impl<R> Tokenizer<R> {
    pub(super) fn commit(&mut self) {
        self.head = self.tail;
    }
}

#[cfg(test)]
mod tests {
    use super::Tokenizer;

    #[test]
    fn test_commit() {
        let buf: &[_] = b"abcde";
        let mut tokenizer = Tokenizer::new(buf);

        let _ = tokenizer.read_char();
        let _ = tokenizer.read_char();
        let _ = tokenizer.read_char();

        assert_eq!(tokenizer.head, 0);
        assert_eq!(tokenizer.tail, 3);

        tokenizer.commit();

        assert_eq!(tokenizer.head, 3);
        assert_eq!(tokenizer.tail, 3);
    }
}
