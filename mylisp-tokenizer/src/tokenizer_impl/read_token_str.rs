use std::io::BufRead;

use super::utils::Found;
use super::Error;
use super::Result;
use super::Token;
use super::Tokenizer;

impl<R: BufRead> Tokenizer<R> {
    pub(super) fn read_token_str(&mut self) -> Result {
        match self.read_char()? {
            Some(ch) if *ch == '"' => {}
            _ => {
                self.roll_back();
                return no_such_token!();
            }
        }
        let pos = self.get_position();
        let mut buf = String::new();
        let mut escape = false;
        loop {
            if let Some(&ch) = self.read_char()? {
                if ch == '\r' || ch == '\n' {
                    continue;
                }
                if escape {
                    escape = false;
                    buf.push(match ch {
                        'n' => '\n',
                        'r' => '\r',
                        't' => '\t',
                        _ => ch,
                    });
                } else if ch == '\\' {
                    escape = true;
                } else if ch == '"' {
                    self.commit();
                    return Token::Str(buf).found(pos);
                } else {
                    buf.push(ch);
                }
            } else {
                self.commit();
                if self.is_eof() {
                    let pos = self.get_position();
                    return Error::new("invalid string token").found(pos);
                }
                if escape {
                    escape = false;
                } else {
                    buf.push('\n');
                }
            }
        }
    }
}
