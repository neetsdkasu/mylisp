use super::Tokenizer;

impl<R> Tokenizer<R> {
    pub(super) fn roll_back(&mut self) {
        self.tail = self.head;
    }
}
