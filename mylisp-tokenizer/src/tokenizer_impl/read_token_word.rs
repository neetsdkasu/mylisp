use std::io::BufRead;

use super::utils::{is_token_separator, Found};
use super::Result;
use super::Token;
use super::Tokenizer;

impl<R: BufRead> Tokenizer<R> {
    pub(super) fn read_token_word(&mut self) -> Result {
        let mut token = String::new();
        while let Some(ch) = self.read_char()? {
            if is_token_separator(*ch) {
                self.push_back();
                break;
            }
            token.push(ch.to_ascii_lowercase());
        }
        if token.is_empty() {
            no_such_token!()
        } else if token == "." {
            let pos = self.get_position();
            self.commit();
            Token::ConsDot.found(pos)
        } else {
            let pos = self.get_position();
            self.commit();
            Token::Word(token).found(pos)
        }
    }
}
