pub struct Tokenizer<R> {
    pub(super) tokenizer: R,
    pub(super) chars: Vec<char>,
    pub(super) buf: String,
    pub(super) head: usize,
    pub(super) tail: usize,
    pub(super) line_count: usize,
}

impl<R> Tokenizer<R> {
    pub fn new(file: R) -> Tokenizer<R> {
        Tokenizer {
            tokenizer: file,
            chars: Vec::new(),
            buf: String::new(),
            head: 0,
            tail: 0,
            line_count: 0,
        }
    }
}
