use std::io::BufRead;

use super::utils::Found;
use super::Error;
use super::Result;
use super::Token;
use super::Tokenizer;

impl<R: BufRead> Tokenizer<R> {
    pub fn read_token(&mut self) -> Result {
        macro_rules! find_token {
            ($fun:ident($($args:expr),*)) => {
                let res = self.$fun($($args),*);
                if !matches!(res, no_such_token!()) {
                    return res;
                }
            };
            ($($fun:ident($($args:expr),*),)*) => {
                $(find_token!($fun($($args),*));)*
            };
        }
        find_token!(skip_whitespaces());
        if self.is_eof() {
            return Ok(None);
        }
        find_token!(
            read_one_char_token('(', Token::OpenBracket),
            read_one_char_token(')', Token::CloseBracket),
            read_one_char_token('\'', Token::Quote),
            read_one_char_token('`', Token::BackQuote),
            read_token_comma(),
            read_token_str(),
            read_token_num(),
            read_token_word(),
        );
        Error::new("Tokenizer BUG? in read_token()").found(self.get_position())
    }
}
