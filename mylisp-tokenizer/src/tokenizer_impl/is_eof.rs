use super::Tokenizer;

impl<R> Tokenizer<R> {
    pub fn is_eof(&self) -> bool {
        self.buf.is_empty()
    }

    #[allow(dead_code)]
    pub fn is_eol(&self) -> bool {
        self.head == self.buf.len()
    }

    #[allow(dead_code)]
    pub fn skip_to_eol(&mut self) {
        self.tail = self.buf.len();
        self.commit();
    }
}
