use std::io::{self, BufRead};

use super::Tokenizer;

impl<R: BufRead> Tokenizer<R> {
    pub(super) fn read_char(&mut self) -> io::Result<Option<&char>> {
        while self.tail == self.chars.len() {
            if self.head == self.tail {
                self.read_line()?;
                if self.is_eof() {
                    return Ok(None);
                }
            } else {
                return Ok(None); // EOL
            }
        }
        let pos = self.tail;
        self.tail += 1;
        Ok(self.chars.get(pos))
    }
}
