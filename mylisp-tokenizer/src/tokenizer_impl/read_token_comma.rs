use std::io::BufRead;

use super::utils::Found;
use super::Error;
use super::Result;
use super::Token;
use super::Tokenizer;

impl<R: BufRead> Tokenizer<R> {
    pub(super) fn read_token_comma(&mut self) -> Result {
        match self.read_char()? {
            Some(ch) if *ch == ',' => {}
            Some(ch) if *ch == '@' => {
                let pos = self.get_position();
                return Error::new("invalid @ token").found(pos);
            }
            _ => {
                self.roll_back();
                return no_such_token!();
            }
        }
        let token = match self.read_char()? {
            None => Token::Comma,
            Some(ch) => {
                if *ch == '@' {
                    Token::CommaAt
                } else {
                    self.push_back();
                    Token::Comma
                }
            }
        };
        let pos = self.get_position();
        self.commit();
        token.found(pos)
    }
}
