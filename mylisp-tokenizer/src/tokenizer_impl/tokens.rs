use std::io::{self, BufRead};
use std::iter;
use std::result;

use super::Error;
use super::Position;
use super::Token;
use super::Tokenizer;

impl<R: BufRead> Tokenizer<R> {
    #[allow(dead_code)]
    pub fn tokenize(self) -> io::Result<result::Result<Vec<Token>, (Error, Position)>> {
        let mut ret = vec![];
        for res in self.tokens() {
            match res {
                Err(err) => return Err(err),
                Ok((Err(err), pos)) => return Ok(Err((err, pos))),
                Ok((Ok(token), _)) => ret.push(token),
            }
        }
        Ok(Ok(ret))
    }
}

impl<R> Tokenizer<R> {
    pub fn tokens(self) -> Tokens<R> {
        Tokens::new(self)
    }
}

pub struct Tokens<R> {
    tokenizer: Tokenizer<R>,
    stopped: bool,
}

impl<R> Tokens<R> {
    fn new(tokenizer: Tokenizer<R>) -> Tokens<R> {
        Tokens {
            tokenizer,
            stopped: false,
        }
    }
}

impl<R: BufRead> iter::Iterator for Tokens<R> {
    type Item = io::Result<(Result<Token, Error>, Position)>;
    fn next(&mut self) -> Option<Self::Item> {
        if self.stopped {
            return None;
        }
        match self.tokenizer.read_token() {
            Ok(Some(res)) => Some(Ok(res)),
            Ok(None) => {
                self.stopped = true;
                None
            }
            Err(err) => {
                self.stopped = true;
                Some(Err(err))
            }
        }
    }
}
