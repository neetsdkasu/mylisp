use super::Tokenizer;

impl<R> Tokenizer<R> {
    pub(super) fn push_back(&mut self) {
        if self.tail > self.head {
            self.tail -= 1;
        }
    }
}
