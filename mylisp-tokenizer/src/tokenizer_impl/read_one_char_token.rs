use std::io::BufRead;

use super::utils::Found;
use super::Result;
use super::Token;
use super::Tokenizer;

impl<R: BufRead> Tokenizer<R> {
    pub(super) fn read_one_char_token(&mut self, token_ch: char, token: Token) -> Result {
        match self.read_char()? {
            Some(ch) if *ch == token_ch => {
                let pos = self.get_position();
                self.commit();
                token.found(pos)
            }
            _ => {
                self.roll_back();
                no_such_token!()
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use std::io;

    use super::Token;
    use super::Tokenizer;

    #[test]
    fn test_read_one_char_token() {
        let buf: &[_] = b"abcde";
        let mut tokenizer = Tokenizer::new(buf);

        let res = tokenizer.read_one_char_token('a', Token::Quote);
        assert!(res.is_ok());

        let res = res.unwrap();
        assert!(res.is_some());

        let (token, pos) = res.unwrap();
        assert!(matches!(token, Ok(Token::Quote)));
        assert_eq!(pos.pos, 0);
        assert_eq!(pos.line_count, 1);

        let res = tokenizer.read_one_char_token('b', Token::Comma);
        assert!(res.is_ok());

        let res = res.unwrap();
        assert!(res.is_some());

        let (token, pos) = res.unwrap();
        assert!(matches!(token, Ok(Token::Comma)));
        assert_eq!(pos.pos, 1);
        assert_eq!(pos.line_count, 1);

        let res = tokenizer.read_one_char_token('a', Token::Quote);
        assert!(matches!(res, no_such_token!()));

        let res = tokenizer.read_one_char_token('c', Token::BackQuote);
        assert!(res.is_ok());

        let res = res.unwrap();
        assert!(res.is_some());

        let (token, pos) = res.unwrap();
        assert!(matches!(token, Ok(Token::BackQuote)));
        assert_eq!(pos.pos, 2);
        assert_eq!(pos.line_count, 1);
    }

    struct ReadErr;

    impl io::Read for ReadErr {
        fn read(&mut self, _buf: &mut [u8]) -> io::Result<usize> {
            Err(io::Error::new(io::ErrorKind::Other, ""))
        }
    }

    #[test]
    fn test_read_one_char_token_with_io_error() {
        let mut tokenizer = Tokenizer::new(io::BufReader::new(ReadErr));

        let res = tokenizer.read_one_char_token('a', Token::Quote);
        assert!(res.is_err());
    }
}
