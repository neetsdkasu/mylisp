use std::io::BufRead;

use super::utils::{is_token_separator, Found};
use super::Error;
use super::Result;
use super::Token;
use super::Tokenizer;

impl<R: BufRead> Tokenizer<R> {
    pub(super) fn read_token_num(&mut self) -> Result {
        let mut num = String::new();
        let top = match self.read_char()? {
            Some(ch) if ch.is_ascii_digit() => *ch,
            Some(ch) if *ch == '-' || *ch == '+' => {
                num.push(*ch);
                match self.read_char()? {
                    Some(ch) if ch.is_ascii_digit() => *ch,
                    _ => {
                        self.roll_back();
                        return no_such_token!();
                    }
                }
            }
            _ => {
                self.roll_back();
                return no_such_token!();
            }
        };
        if top == '0' {
            match self.read_char()? {
                Some(ch) if *ch == 'b' || *ch == 'B' => return self.read_token_bin_num(num),
                Some(ch) if *ch == 'x' || *ch == 'X' => return self.read_token_hex_num(num),
                Some(_) => self.push_back(),
                _ => {}
            }
        }
        num.push(top);
        loop {
            match self.read_char()? {
                None => break,
                Some(ch) if is_token_separator(*ch) => {
                    self.push_back();
                    break;
                }
                Some(ch) if ch.is_ascii_digit() => num.push(*ch),
                Some(ch) if *ch == '_' => {}
                Some(ch) if *ch == '.' => return self.read_token_decimal(num),
                Some(ch) if *ch == 'e' || *ch == 'E' => return self.read_token_exponent(num),
                _ => {
                    let pos = self.get_position();
                    return Error::new("invalid number").found(pos);
                }
            }
        }
        let pos = self.get_position();
        self.commit();
        Token::Int(num).found(pos)
    }

    fn read_token_bin_num(&mut self, mut num: String) -> Result {
        loop {
            match self.read_char()? {
                None => break,
                Some(ch) if is_token_separator(*ch) => {
                    self.push_back();
                    break;
                }
                Some(ch) if *ch == '0' || *ch == '1' => num.push(*ch),
                Some(ch) if *ch == '_' => {}
                _ => {
                    let pos = self.get_position();
                    return Error::new("invalid binary number").found(pos);
                }
            }
        }
        if num.is_empty() {
            let pos = self.get_position();
            return Error::new("invalid binary number").found(pos);
        }
        let pos = self.get_position();
        self.commit();
        Token::IntBin(num).found(pos)
    }

    fn read_token_hex_num(&mut self, mut num: String) -> Result {
        loop {
            match self.read_char()? {
                None => break,
                Some(ch) if is_token_separator(*ch) => {
                    self.push_back();
                    break;
                }
                Some(ch) if ch.is_ascii_hexdigit() => num.push(ch.to_ascii_lowercase()),
                Some(ch) if *ch == '_' => {}
                _ => {
                    let pos = self.get_position();
                    return Error::new("invalid hex number").found(pos);
                }
            }
        }
        if num.is_empty() {
            let pos = self.get_position();
            return Error::new("invalid hex number").found(pos);
        }
        let pos = self.get_position();
        self.commit();
        Token::IntHex(num).found(pos)
    }

    fn read_token_decimal(&mut self, mut num: String) -> Result {
        num.push('.');
        match self.read_char()? {
            None => {
                let pos = self.get_position();
                self.commit();
                return Token::Dec(num).found(pos);
            }
            Some(ch) if is_token_separator(*ch) => {
                self.push_back();
                let pos = self.get_position();
                self.commit();
                return Token::Dec(num).found(pos);
            }
            Some(ch) if ch.is_ascii_digit() => num.push(*ch),
            _ => {
                let pos = self.get_position();
                return Error::new("invalid decimal").found(pos);
            }
        }
        loop {
            match self.read_char()? {
                None => break,
                Some(ch) if is_token_separator(*ch) => {
                    self.push_back();
                    break;
                }
                Some(ch) if ch.is_ascii_digit() => num.push(*ch),
                Some(ch) if *ch == '_' => {}
                Some(ch) if *ch == 'e' || *ch == 'E' => return self.read_token_exponent(num),
                _ => {
                    let pos = self.get_position();
                    return Error::new("invalid decimal").found(pos);
                }
            }
        }
        let pos = self.get_position();
        self.commit();
        Token::Dec(num).found(pos)
    }

    fn read_token_exponent(&mut self, mut num: String) -> Result {
        num.push('e');
        match self.read_char()? {
            Some(ch) if ch.is_ascii_digit() => num.push(*ch),
            Some(ch) if *ch == '+' || *ch == '-' || *ch == '_' => {
                if *ch != '_' {
                    num.push(*ch);
                }
                loop {
                    match self.read_char()? {
                        Some(ch) if *ch == '_' => {}
                        Some(ch) if ch.is_ascii_digit() => {
                            num.push(*ch);
                            break;
                        }
                        _ => {
                            let pos = self.get_position();
                            return Error::new("invalid exponent").found(pos);
                        }
                    }
                }
            }
            _ => {
                let pos = self.get_position();
                return Error::new("invalid exponent").found(pos);
            }
        }
        loop {
            match self.read_char()? {
                None => break,
                Some(ch) if is_token_separator(*ch) => {
                    self.push_back();
                    break;
                }
                Some(ch) if ch.is_ascii_digit() => num.push(*ch),
                Some(ch) if *ch == '_' => {}
                _ => {
                    let pos = self.get_position();
                    return Error::new("invalid exponent").found(pos);
                }
            }
        }
        let pos = self.get_position();
        self.commit();
        Token::Dec(num).found(pos)
    }
}
