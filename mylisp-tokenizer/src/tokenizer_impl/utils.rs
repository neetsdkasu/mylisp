use super::Error;
use super::Position;
use super::Result;
use super::Token;

macro_rules! no_such_token {
    () => {
        Ok(None)
    };
}

pub(super) fn is_token_separator(ch: char) -> bool {
    ch.is_whitespace() || "();,'`\"".contains(ch)
}

pub(super) trait Found {
    fn found(self, pos: Position) -> Result;
}

impl Found for Token {
    fn found(self, pos: Position) -> Result {
        Ok(Some((Ok(self), pos)))
    }
}

impl Found for Error {
    fn found(self, pos: Position) -> Result {
        Ok(Some((Err(self), pos)))
    }
}
