use std::io::BufRead;

use super::utils::Found;
use super::Result;
use super::Token;
use super::Tokenizer;

impl<R: BufRead> Tokenizer<R> {
    pub(super) fn skip_whitespaces(&mut self) -> Result {
        while let Some(ch) = self.read_char()? {
            if *ch == ';' {
                self.push_back();
                self.commit();
                let mut comment = String::new();
                while let Some(ch) = self.read_char()? {
                    comment.push(*ch);
                }
                let omit: &[_] = &['\r', '\n'];
                comment = comment.trim_end_matches(omit).to_owned();
                let pos = self.get_position();
                self.commit();
                return Token::new_line_with_comment(comment).found(pos);
            } else if !ch.is_whitespace() {
                self.push_back();
                self.commit();
                return Ok(None);
            }
        }
        self.commit();
        if self.is_eof() {
            Ok(None)
        } else {
            let pos = self.get_position();
            Token::new_line().found(pos)
        }
    }
}
