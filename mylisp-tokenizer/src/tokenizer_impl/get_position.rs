use super::Position;
use super::Tokenizer;

impl<R> Tokenizer<R> {
    pub fn get_position(&self) -> Position {
        Position {
            pos: self.head,
            line_count: self.line_count,
        }
    }
}
