use std::io;
use std::result;

use super::Error;
use super::Position;
use super::Token;

pub type Result = io::Result<Option<(result::Result<Token, Error>, Position)>>;
