// alias tokenizer_impl::*
pub use tokenizer_struct::Tokenizer;
pub use tokens::Tokens; // dead_code

// alias tokenizer_impl::* for modules under tokenizer_impl module
use super::Error;
use super::Position;
use super::Result;
use super::Token;

// utils for tokenizer_impl module
#[macro_use]
mod utils;

// modules for pub
mod is_eof;
mod read_token;
mod tokenizer_struct;
mod tokens; // dead_code

// modules for tokenizer_impl module
mod commit;
mod get_position;
mod push_back;
mod read_char;
mod read_line;
mod read_one_char_token;
mod read_token_comma;
mod read_token_num;
mod read_token_str;
mod read_token_word;
mod roll_back;
mod skip_whitespaces;
