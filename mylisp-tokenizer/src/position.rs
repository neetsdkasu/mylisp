#[derive(Debug, Clone)]
pub struct Position {
    pub pos: usize,
    pub line_count: usize,
}
