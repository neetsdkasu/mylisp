;
; 実行時間計測マクロ
;
(macro bench! (f)
    `((lambda (_ t0 r t1)
          (car
            (list
              r
              (eprintln
                "bench-time:" (- t1 t0) "ms"
                "(" (div (- t1 t0) 60000) "min."
                (/ (rem (- t1 t0) 60000) 1000) "sec." ")"))))
        (eprintln "bench-code:" ',f)
        (time-now)
        ,f
        (time-now)))

;
; アサーション
;
(macro assert! (_eval)
    `(if ,_eval
        nil
        (eprintln 'Assertion-Error ',_eval)))


;
; スコープ判定
;
(define scope 'dynamic)
(define is-dynamic-scope
  ((lambda (scope)
      (lambda () (equals scope 'dynamic)))
    'lexical))


;
; たらい関数 (シンプルな実装) (Dynamicスコープのみで動作)
;
(define dyn-tarai
  (lambda (x y z)
    (if (<= x y)
      y
      (dyn-tarai
        (dyn-tarai (- x 1) y z)
        (dyn-tarai (- y 1) z x)
        (dyn-tarai (- z 1) x y)))))


;
; たらい関数 (内部で再帰呼び出しを行う)
;
(define tarai*
  (lambda (x y z)
    ((lambda (x y z tarai)
        (tarai tarai x y z))
      x
      y
      z
      (lambda (tarai x y z)
      (if (<= x y)
        y
        (tarai
          tarai
          (tarai tarai (- x 1) y z)
          (tarai tarai (- y 1) z x)
          (tarai tarai (- z 1) x y)))))))


;
; たらい関数 (内部で再帰呼び出しを行う) (組み込みの関数等を埋め込む)
;
(macro make-tarai+! ()
  `(lambda (tarai x y z)
      (,if (,<= x y)
        y
        (tarai
          tarai
          (tarai tarai (,- x 1) y z)
          (tarai tarai (,- y 1) z x)
          (tarai tarai (,- z 1) x y)))))
(define tarai+
  (lambda (x y z)
    ((lambda (x y z tarai)
        (tarai tarai x y z))
      x
      y
      z
      (make-tarai+!))))


;
; たらい関数 (末尾再帰関数recurを用いる)
;
(define tarai~
  (lambda (x y z)
    ((lambda (x y z f)
        (recur 'foo (f x y z) f))
      x
      y
      z
      (lambda (tarai x y z)
        (if (<= x y)
          y
          (next 'foo
            tarai
            (recur 'foo (tarai (- x 1) y z) tarai)
            (recur 'foo (tarai (- y 1) z x) tarai)
            (recur 'foo (tarai (- z 1) x y) tarai)))))))


;
; たらい関数 (クロージャによる遅延評価実装) (Lexicalスコープのみで動作)
;
(define closure-tarai
  (lambda (x y z)
    ((lambda (x y z tarai) (tarai tarai x y z))
        (lambda () x)
        (lambda () y)
        (lambda () z)
        (lambda (tarai x y z)
          ((lambda (tarai xe ye z)
            (if (<= xe ye)
              ye
              (tarai
                tarai
                (lambda ()
                  (tarai
                    tarai
                    (lambda () (- xe 1))
                    (lambda () ye)
                    z))
                (lambda ()
                  (tarai
                    tarai
                    (lambda () (- ye 1))
                    z
                    (lambda () xe)))
                (lambda ()
                  (tarai
                    tarai
                    (lambda () (- (z) 1))
                    (lambda () xe)
                    (lambda () ye))))))
            tarai
            (x)
            (y)
            z)))))


;
; たらい関数 (遅延評価利用)
;
(define delay-tarai
  (lambda (x y z)
    (delay-get
      ((lambda (x y z tarai) (tarai tarai x y z))
        (delay x)
        (delay y)
        (delay z)
        (lambda (tarai x y z)
          (if (<= (delay-get x) (delay-get y))
            y
            (tarai
              tarai
              (delay (delay-get (tarai tarai (delay (- (delay-get x) 1)) y z)))
              (delay (delay-get (tarai tarai (delay (- (delay-get y) 1)) z x)))
              (delay (delay-get (tarai tarai (delay (- (delay-get z) 1)) x y)))
              )))))))


;
; たらい関数の出力値の検査用
;
(define result-of-tarai
  (lambda (x y z)
    (if (<= x y)
      y
      (if (and (> x y) (<= y z))
        z
        x))))


;
; 計測実行
;
(define result             (result-of-tarai 12 6 0))
(assert! (= result (bench! (tarai*          12 6 0))))
(assert! (= result (bench! (tarai+          12 6 0))))
(assert! (= result (bench! (tarai~          12 6 0))))
(assert! (= result (bench! (delay-tarai     12 6 0))))
(if (is-dynamic-scope)
  (assert! (= result (bench! (dyn-tarai     12 6 0))))
  (assert! (= result (bench! (closure-tarai 12 6 0)))))


;
; 結果 (Dynamicスコープmode)
;
; bench-code: (TARAI* 12 6 0)
; bench-time: 380940 ms ( 6 min. 20.94 sec. )
; bench-code: (TARAI+ 12 6 0)
; bench-time: 366271 ms ( 6 min. 6.271 sec. )
; bench-code: (TARAI~ 12 6 0)
; bench-time: 472130 ms ( 7 min. 52.13 sec. )
; bench-code: (DELAY-TARAI 12 6 0)
; bench-time: 10 ms ( 0 min. 0.01 sec. )
; bench-code: (DYN-TARAI 12 6 0)
; bench-time: 473871 ms ( 7 min. 53.871 sec. )

;
; 結果 (Lexicalスコープmode)
;
; bench-code: (TARAI* 12 6 0)
; bench-time: 384191 ms ( 6 min. 24.191 sec. )
; bench-code: (TARAI+ 12 6 0)
; bench-time: 366641 ms ( 6 min. 6.641 sec. )
; bench-code: (TARAI~ 12 6 0)
; bench-time: 479800 ms ( 7 min. 59.8 sec. )
; bench-code: (DELAY-TARAI 12 6 0)
; bench-time: 0 ms ( 0 min. 0 sec. )
; bench-code: (CLOSURE-TARAI 12 6 0)
; bench-time: 20 ms ( 0 min. 0.02 sec. )
