;
; AVL-Tree (maybe...)
;
; binary tree design
; ( (key . (value . depth)) . (left-subtree . right-subtree) )
; 
; key: string
; value: any
;
; (tree-empty)
; (tree-insert key value tree)
; (tree-delete key tree)
; (tree-get key tree)
; (tree-fold f init tree) ;; f: (f acc entry)
; (tree-to-list tree)
; (tree-from-list list) ;; list: ((key . value) (key . value) (key . value) ...)
;


(define max (lambda (a b) (if (> a b) a b)))
(define equals-key (lambda (a b) (equals a b)))
(define less-key (lambda (a b) (< (str-compare a b) 0)))


(define tree-empty (lambda () nil))


(define tree-get-root-entry
  (lambda (tree)
    (if (nil? tree) nil
      (car tree)
      )))


(define tree-get-root-key
  (lambda (tree)
    (if (nil? tree) nil
      (list (car (car tree)))
      )))


(define tree-get-root-value
  (lambda (tree)
    (if (nil? tree) nil
      (list (car (cdr (car tree))))
      )))


(define tree-get-depth
  (lambda (tree)
    (if (nil? tree) 0
      (cdr (cdr (car tree)))
      )))


(define tree-left-subtree
  (lambda (tree)
    (if (nil? tree) nil
      (if (nil? (cdr tree)) nil
        (car (cdr tree))
        ))))


(define tree-right-subtree
  (lambda (tree)
    (if (nil? tree) nil
      (if (nil? (cdr tree)) nil
        (cdr (cdr tree))
        ))))


(define tree-new-depth
  (lambda (left-subtree right-subtree)
    (+ 1
      (max
        (tree-get-depth left-subtree)
        (tree-get-depth right-subtree)
        ))))


(define tree-get-min
  (lambda (tree)
    ((lambda (tree tree-get-min)
        (tree-get-min tree tree-get-min))
      tree
      (lambda (tree tree-get-min)
        (if (nil? (tree-left-subtree tree))
          (tree-get-root-entry tree)
          (tree-get-min
            (tree-left-subtree tree)
            tree-get-min
            ))))))


(define tree-get-max
  (lambda (tree)
    ((lambda (tree tree-get-max)
        (tree-get-max tree tree-get-max))
      tree
      (lambda (tree tree-get-max)
        (if (nil? (tree-right-subtree tree))
          (tree-get-root-entry tree)
          (tree-get-max
            (tree-right-subtree tree)
            tree-get-max
            ))))))


(define tree-depth-diff
  (lambda (tree1 tree2)
    (- (tree-get-depth tree1) (tree-get-depth tree2))
    ))


(define tree-make
  (lambda (key value left-subtree right-subtree)
    ((lambda (key value left-subtree right-subtree tree-make)
        (tree-make key value left-subtree right-subtree tree-make))
      key
      value
      left-subtree
      right-subtree
      (lambda (key value left-subtree right-subtree tree-make)
        (if (< 1 (tree-depth-diff left-subtree right-subtree))
          ; rotate clockwise
          (if
            (< 0
              (tree-depth-diff
                (tree-left-subtree left-subtree)
                (tree-right-subtree left-subtree)
                ))
            ; lift up left-subtree-root
            (tree-make
              (car (tree-get-root-key left-subtree))
              (car (tree-get-root-value left-subtree))
              (tree-left-subtree left-subtree)
              (tree-make
                key
                value
                (tree-right-subtree left-subtree)
                right-subtree
                tree-make
                )
              tree-make
              )
            ; lift up right-subtree-root of left-subtree
            (tree-make
              (car (tree-get-root-key (tree-right-subtree left-subtree)))
              (car (tree-get-root-value (tree-right-subtree left-subtree)))
              (tree-make
                (car (tree-get-root-key left-subtree))
                (car (tree-get-root-value left-subtree))
                (tree-left-subtree left-subtree)
                (tree-left-subtree (tree-right-subtree left-subtree))
                tree-make
                )
              (tree-make
                key
                value
                (tree-right-subtree (tree-right-subtree left-subtree))
                right-subtree
                tree-make
                )
              tree-make
              ))
          (if (< 1 (tree-depth-diff right-subtree left-subtree))
            ; rotate counter-clockwise
            (if
              (< 0
                (tree-depth-diff
                  (tree-right-subtree right-subtree)
                  (tree-left-subtree right-subtree)
                  ))
              ; lift up right-subtree-root
              (tree-make
                (car (tree-get-root-key right-subtree))
                (car (tree-get-root-value right-subtree))
                (tree-make
                  key
                  value
                  left-subtree
                  (tree-left-subtree right-subtree)
                  tree-make
                  )
                (tree-right-subtree right-subtree)
                tree-make
                )
              ; lift up left-subtree-root of right-subtree
              (tree-make
                (car (tree-get-root-key (tree-left-subtree right-subtree)))
                (car (tree-get-root-value (tree-left-subtree right-subtree)))
                (tree-make
                  key
                  value
                  left-subtree
                  (tree-left-subtree (tree-left-subtree right-subtree))
                  tree-make
                  )
                (tree-make
                  (car (tree-get-root-key right-subtree))
                  (car (tree-get-root-value right-subtree))
                  (tree-right-subtree (tree-left-subtree right-subtree))
                  (tree-right-subtree right-subtree)
                  tree-make
                  )
                tree-make
                ))
            ; not rorate
            (cons
              (cons key
                (cons value
                  (tree-new-depth left-subtree right-subtree)
                  ))
              (cons left-subtree right-subtree)
              )))))))


(define tree-replace-left-subtree
  (lambda (left-subtree tree)
    (if (nil? tree) nil
      (tree-make
        (car (tree-get-root-key tree))
        (car (tree-get-root-value tree))
        left-subtree
        (tree-right-subtree tree)
        ))))


(define tree-replace-right-subtree
  (lambda (right-subtree tree)
    (if (nil? tree) nil
      (tree-make
        (car (tree-get-root-key tree))
        (car (tree-get-root-value tree))
        (tree-left-subtree tree)
        right-subtree
        ))))


(define tree-delete-min
  (lambda (tree)
    ((lambda (tree tree-delete-min)
        (tree-delete-min tree tree-delete-min))
      tree
      (lambda (tree tree-delete-min)
        (if (nil? (tree-left-subtree tree))
          (tree-right-subtree tree)
          (tree-make
            (car (tree-get-root-key tree))
            (car (tree-get-root-value tree))
            (tree-delete-min
              (tree-left-subtree tree)
              tree-delete-min
              )
            (tree-right-subtree tree)
            ))))))


(define tree-delete-max
  (lambda (tree)
    ((lambda (tree tree-delete-max)
        (tree-delete-max tree tree-delete-max))
      tree
      (lambda (tree tree-delete-max)
        (if (nil? (tree-right-subtree tree))
          (tree-left-subtree tree)
          (tree-make
            (car (tree-get-root-key tree))
            (car (tree-get-root-value tree))
            (tree-left-subtree tree)
            (tree-delete-max
              (tree-right-subtree tree)
              tree-delete-max
              )
            ))))))


(define tree-insert
  (lambda (key value tree)
    ((lambda (key value tree tree-insert)
        (tree-insert key value tree tree-insert))
      key
      value
      tree
      (lambda (key value tree tree-insert)
        (if (nil? tree) (tree-make key value nil nil)
          (if (equals-key key (car (tree-get-root-key tree)))
            (tree-make
              key
              value
              (tree-left-subtree tree)
              (tree-right-subtree tree)
              )
            (if (less-key key (car (tree-get-root-key tree)))
              (tree-replace-left-subtree
                (tree-insert key value (tree-left-subtree tree) tree-insert)
                tree
                )
              (tree-replace-right-subtree
                (tree-insert key value (tree-right-subtree tree) tree-insert)
                tree
                ))))))))


(define tree-get
  (lambda (key tree)
    ((lambda (key tree tree-get)
        (tree-get key tree tree-get))
      key
      tree
      (lambda (key tree tree-get)
        (if (nil? tree) nil
          (if (equals-key key (car (tree-get-root-key tree)))
            (tree-get-root-value tree)
            (if (less-key key (car (tree-get-root-key tree)))
              (tree-get key
                (tree-left-subtree tree)
                tree-get
                )
              (tree-get key
                (tree-right-subtree tree)
                tree-get
                ))))))))


(define tree-delete
  (lambda (key tree)
    ((lambda (key tree tree-delete)
        (tree-delete key tree tree-delete))
      key
      tree
      (lambda (key tree tree-delete)
        (if (nil? tree) nil
          (if (equals-key key (car (tree-get-root-key tree)))
            (if (nil? (tree-right-subtree tree))
              (tree-left-subtree tree)
              (tree-make
                (car (tree-get-min (tree-right-subtree tree)))
                (car (cdr (tree-get-min (tree-right-subtree tree))))
                (tree-left-subtree tree)
                (tree-delete-min (tree-right-subtree tree))
                ))
            (if (less-key key (car (tree-get-root-key tree)))
              (tree-make
                (car (tree-get-root-key tree))
                (car (tree-get-root-value tree))
                (tree-delete
                  key
                  (tree-left-subtree tree)
                  tree-delete
                  )
                (tree-right-subtree tree)
                )
              (tree-make
                (car (tree-get-root-key tree))
                (car (tree-get-root-value tree))
                (tree-left-subtree tree)
                (tree-delete
                  key
                  (tree-right-subtree tree)
                  tree-delete
                  )))))))))


(define tree-fold
  (lambda (f init tree)
    ((lambda (f acc tree tree-fold)
        (tree-fold f acc tree tree-fold))
      f
      init
      tree
      (lambda (f acc tree tree-fold)
        (if (nil? tree) acc
          (tree-fold
            f
            (f
              (tree-fold
                f
                acc
                (tree-left-subtree tree)
                tree-fold
                )
              (tree-get-root-entry tree)
              )
            (tree-right-subtree tree)
            tree-fold
            ))))))


(define tree-to-list
  (lambda (tree)
    ((lambda (tree acc tree-to-list)
        (tree-to-list tree acc tree-to-list))
      tree
      nil
      (lambda (tree acc tree-to-list)
        (if (nil? tree) acc
          (tree-to-list
            (tree-left-subtree tree)
            (cons
              (tree-get-root-entry tree)
              (tree-to-list
                (tree-right-subtree tree)
                acc
                tree-to-list
                ))
            tree-to-list
            ))))))


(define tree-from-list
  (lambda (ls)
    (recur 'fold ((tree-empty) ls)
      (lambda (acc ls)
        (if (nil? ls) acc
          (next 'fold
            (tree-insert
              (car (car ls))
              (cdr (car ls))
              acc
              )
            (cdr ls)
            ))))))    


;
; examples
;

(define mytree
  (tree-from-list
    '(("abc" . 123) ("xyz" . 999) ("aaa" . 100)
      ("vfx" . 200) ("etc" .  89) ("god" . 818)
      ("mac" . 333) ("win" .  95) ("dep" . 555)
      ("con" .   4) ("opt" . 404) ("ext" . 777)
      )))


'(
  ("etc" 89 . 5)
  ; etc left
  (
    ("abc" 123 . 3)
    ; abc left
    (
      ("aaa" 100 . 1)
      ; aaa left
      NIL
      ; aaa right (. nil)
    )
    ; abc right
    ("dep" 555 . 2)
    ; dep left
    (
      ("con" 4 . 1)
      ; con left
      NIL
      ; con right (. nil)
    )
    ; dep right (. nil)
  )
  ; etc right
  ("vfx" 200 . 4)
  ; vfx left
  (
    ("mac" 333 . 3)
    ; mac left
    (
      ("god" 818 . 2)
      ; god left
      (
        ("ext" 777 . 1)
        ; ext left
        NIL
        ; ext right (. nil)
      )
      ; god right (. nil)
    )
    ; mac right
    ("opt" 404 . 1)
    ; opt left
    NIL
    ; opt right (. nil)
  )
  ; vfx right
  ("xyz" 999 . 2)
  ; xyz left
  (
    ("win" 95 . 1)
    ; win left
    NIL
    ; win right (. nil)
  )
  ; xyz right (. nil)
)


mytree
(tree-to-list mytree)
(tree-fold (lambda (acc entry) (cons entry acc)) nil mytree)
