; MACHINE-MODE用

;
; 実行時間計測マクロ
;
(macro bench! (f)
    `((lambda (_ t0 r t1)
          (car
            (list
              r
              (eprintln
                "bench-time:" (- t1 t0) "ms"
                "(" (div (- t1 t0) 60000) "min."
                (/ (rem (- t1 t0) 60000) 1000) "sec." ")"))))
        (eprintln "bench-code:" ',f)
        (time-now)
        ,f
        (time-now)))

;
; アサーション
;
(macro assert! (_eval)
    `(if ,_eval
        nil
        (eprintln 'Assertion-Error ',_eval)))

;
; たらい関数 (内部で再帰呼び出しを行う)
;
(define tarai*
  (lambda (x y z)
    ((lambda (x y z tarai)
        (tarai tarai x y z))
      x
      y
      z
      (lambda (tarai x y z)
      (if (<= x y)
        y
        (tarai
          tarai
          (tarai tarai (- x 1) y z)
          (tarai tarai (- y 1) z x)
          (tarai tarai (- z 1) x y)))))))


;
; たらい関数 (内部で再帰呼び出しを行う) (組み込みの関数等を埋め込む)
;
(macro make-tarai+! ()
  `(lambda (tarai x y z)
      (if (,<= x y)
        y
        (tarai
          tarai
          (tarai tarai (,- x 1) y z)
          (tarai tarai (,- y 1) z x)
          (tarai tarai (,- z 1) x y)))))
(define tarai+
  (lambda (x y z)
    ((lambda (x y z tarai)
        (tarai tarai x y z))
      x
      y
      z
      (make-tarai+!))))


;
; たらい関数 (末尾再帰関数recurを用いる)
;
(define tarai~
  (lambda (x y z)
    ((lambda (x y z f)
        (recur 'foo (f x y z) f))
      x
      y
      z
      (lambda (tarai x y z)
        (if (<= x y)
          y
          (next 'foo
            tarai
            (recur 'foo (tarai (- x 1) y z) tarai)
            (recur 'foo (tarai (- y 1) z x) tarai)
            (recur 'foo (tarai (- z 1) x y) tarai)))))))


;
; たらい関数 (クロージャによる遅延評価実装) (Lexicalスコープのみで動作)
;
(define closure-tarai
  (lambda (x y z)
    ((lambda (x y z tarai) (tarai tarai x y z))
        (lambda () x)
        (lambda () y)
        (lambda () z)
        (lambda (tarai x y z)
          ((lambda (tarai xe ye z)
            (if (<= xe ye)
              ye
              (tarai
                tarai
                (lambda ()
                  (tarai
                    tarai
                    (lambda () (- xe 1))
                    (lambda () ye)
                    z))
                (lambda ()
                  (tarai
                    tarai
                    (lambda () (- ye 1))
                    z
                    (lambda () xe)))
                (lambda ()
                  (tarai
                    tarai
                    (lambda () (- (z) 1))
                    (lambda () xe)
                    (lambda () ye))))))
            tarai
            (x)
            (y)
            z)))))


;
; たらい関数 (遅延評価利用)
;
(define delay-tarai
  (lambda (x y z)
    (delay-get
      ((lambda (x y z tarai) (tarai tarai x y z))
        (delay x)
        (delay y)
        (delay z)
        (lambda (tarai x y z)
          (if (<= (delay-get x) (delay-get y))
            y
            (tarai
              tarai
              (delay (delay-get (tarai tarai (delay (- (delay-get x) 1)) y z)))
              (delay (delay-get (tarai tarai (delay (- (delay-get y) 1)) z x)))
              (delay (delay-get (tarai tarai (delay (- (delay-get z) 1)) x y)))
              )))))))


;
; たらい関数の出力値の検査用
;
(define result-of-tarai
  (lambda (x y z)
    (if (<= x y)
      y
      (if (and (> x y) (<= y z))
        z
        x))))


;
; 計測実行
;
(define result             (result-of-tarai 12 6 0))
(assert! (= result (bench! (tarai*          12 6 0))))
(assert! (= result (bench! (tarai+          12 6 0))))
(assert! (= result (bench! (tarai~          12 6 0))))
(assert! (= result (bench! (delay-tarai     12 6 0))))
(assert! (= result (bench! (closure-tarai   12 6 0))))


;
; 結果 (Mahine Mode)
;
; bench-code: (TARAI* 12 6 0)
; bench-time: 94249 ms ( 1 min. 34.249 sec. )
; bench-code: (TARAI+ 12 6 0)
; bench-time: 97892 ms ( 1 min. 37.892 sec. )
; bench-code: (TARAI~ 12 6 0)
; bench-time: 126564 ms ( 2 min. 6.564 sec. )
; bench-code: (DELAY-TARAI 12 6 0)
; bench-time: 0 ms ( 0 min. 0 sec. )
; bench-code: (CLOSURE-TARAI 12 6 0)
; bench-time: 0 ms ( 0 min. 0 sec. )
