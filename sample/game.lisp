;
; guessing game
;

(define right (lambda (_ v) v))

(define str-match (lambda (s1 s2) (= 0 (str-compare s1 s2))))

(define yes? (lambda (answer) (str-match "yes" answer)))
(define no? (lambda (answer) (str-match "no" answer)))

(define cofirm-next-game
  (lambda ()
    (recur 'confirm ("")
      (lambda (answer)
        (if (or (yes? answer) (no? answer))
          (yes? answer)
          (next 'confirm
            (right
              (println "\nnext game? (yes/no)")
              (readln))))))))

(define xorshift
  (lambda (seed)
    ((lambda (seed)
        ((lambda (seed)
            (bitwise-xor seed (shift-left seed 17)))
          (bitwise-xor seed (shift-right seed 7))))
      (bitwise-xor seed (shift-left seed 13)))))

(define abs (lambda (v) (if (< v 0) (- v) v)))

(define new-game
  (lambda (seed)
    (cons (rem (abs seed) 10000) (xorshift seed))))

(define get-secret (lambda (game) (car game)))

(define get-seed (lambda (game) (cdr game)))

(define input-number
  (lambda (times)
    (right
      (println "\nnumber? (0 - 9999 OR giveup) [" times "times ]")
      (readln))))

(define rate (lambda (success try) (/ (div (* 1000 success) try) 10)))

(define run
  (lambda ()
    (recur 'main-loop (true (time-now) 0 1)
      (lambda (next-game seed success try)
        (if (not next-game)
          (println "bye!")
          (recur 'game-loop ((new-game seed) 1 (input-number 1))
            (lambda (game times answer)
              (if (str-match "giveup" answer)
                (right
                  (println "secret is" (get-secret game)
                    "[ rate" (rate success try) "% ]")
                  (next 'main-loop
                    (cofirm-next-game)
                    (get-seed game)
                    success
                    (+ try 1)))
                (if (= (get-secret game) (str-to-num answer))
                  (right
                    (println "Correct! [ rate" (rate (+ success 1) try) "% ]")
                    (next 'main-loop
                      (cofirm-next-game)
                      (get-seed game)
                      (+ success 1)
                      (+ try 1)))
                  (right
                    (println
                      "your number is"
                      (if (< (get-secret game) (str-to-num answer))
                        "GREATER" 
                        "LESS") 
                      "than secret")
                    (next 'game-loop
                      game 
                      (+ times 1)
                      (input-number (+ times 1)))
                  ))))))))))

(run)
