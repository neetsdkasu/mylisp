;
; 適当に定義
;
; (id ANY-CODE)
; (void ANY-CODE)
; (assert! BOOL-RESULT-CODE)
; (map LIST APPLY-FUNC)
; (take LIST COUNT)
; (drop LIST COUNT)
; (take-while LIST VALIDATOR)
; (drop-while LIST VALIDATOR)
; (fold LIST INIT FOLDER)
; (fold-r LIST INIT FOLDER)
; (filter LIST VALIDATOR)
; (return SYMBOL ANY-CODE)
; (do! SYMBOL ANY-CODES...)
; (zip-with LIST1 LIST2 APPLY-FUNC)
; (zip! LIST1 LIST2)
; (unzip ZIPPED-LIST)
; (flatten ANY-CODES...)
; (recfun! NAME ARGS BODY)
;

;
; id
; ===========================
;
; 例
;   (id '(1 2 3 4))
;   -> (1 2 3 4)
;
; 例
;   (id (+ 1 2 3))
;   -> 6
;
; 例
;   (id 5)
;   -> 5
;
(define id (lambda (v) v))


;
; void
; ===========================
;
; 例
;   (void (list 1 2 3 4))
;   -> NIL
;
(define void (lambda (_) nil))


;
; assert
; ===========================
;
; 例
;   (assert! (equals '(a b) '(a c)))
;   -> [ error ] (ASSERTION-ERROR (EQUALS (QUOTE (A B)) (QUOTE A C)))
;
(macro assert! (_eval)
    `(if ,_eval
        nil
        (error 'Assertion-Error ',_eval)))

;
; map
; ===========================
;
; 例
;   (map '(1 2 3 4 5) (lambda (v) (* v 3)))
;   -> (3 6 9 12 15)
;
(define map
    (lambda (_map-target-list _map-apply-func)
        ((lambda
                (_map-target-list _map-apply-func _map-inner)
                (_map-inner
                    _map-target-list
                    _map-apply-func
                    _map-inner))
            _map-target-list
            _map-apply-func
            (lambda
                (_map-target-list _map-apply-func _map-inner)
                (if (nil? _map-target-list) nil
                    (cons
                        (_map-apply-func (car _map-target-list))
                        (_map-inner
                            (cdr _map-target-list)
                            _map-apply-func
                            _map-inner)))))))

; 動作確認
(assert! (equals
    (map '(1 2 3 4 5) (lambda (v) (* v 3)))
    '(3 6 9 12 15)))


;
; take
; ===========================
;
; 例
;   (take '(a b c d e f g) 4)
;   -> (A B C D)
;
(define take
  (lambda (_take-target-list _take-count)
    ((lambda (_take-target-list _take-count _take-inner)
        (_take-inner
          _take-target-list
          _take-count
          _take-inner))
      _take-target-list
      _take-count
      (lambda (_take-target-list _take-count _take-inner)
        (if (nil? _take-target-list)
          nil
          (if (> _take-count 0)
            (cons
              (car _take-target-list)
              (_take-inner
                (cdr _take-target-list)
                (- _take-count 1)
                _take-inner))
            nil))))))

; 動作確認
(assert! (equals
    (take '(a b c d e f g) 4)
    '(A B C D)))


;
; drop
; ===========================
;
;　例
;   (drop '(a b c d e f g) 4)
;   -> (E F G)
;
(define drop
  (lambda (_drop-target-list _drop-count)
    ((lambda (_drop-target-list _drop-count _drop-inner)
        (_drop-inner
          _drop-target-list
          _drop-count
          _drop-inner))
      _drop-target-list
      _drop-count
      (lambda (_drop-target-list _drop-count _drop-inner)
        (if (nil? _drop-target-list)
          nil
          (if (> _drop-count 0)
            (_drop-inner
              (cdr _drop-target-list)
              (- _drop-count 1)
              _drop-inner)
            _drop-target-list))))))

; 動作確認
(assert! (equals
    (drop '(a b c d e f g) 4)
    '(E F G)))


;
; take-while
; ===========================
;
; 例
;   (take-while '(1 3 5 7 9 11) (lambda (v) (< v 8)))
;   -> (1 3 5 7)
;
(define take-while
  (lambda (_take-while-target-list _take-while-validator)
    ((lambda (_take-while-target-list _take-while-validator _take-while-inner)
        (_take-while-inner
          _take-while-target-list
          _take-while-validator
          _take-while-inner))
      _take-while-target-list
      _take-while-validator
      (lambda (_take-while-target-list _take-while-validator _take-while-inner)
        (if (nil? _take-while-target-list)
          nil
          (if (_take-while-validator (car _take-while-target-list))
            (cons
              (car _take-while-target-list)
              (_take-while-inner
                (cdr _take-while-target-list)
                _take-while-validator
                _take-while-inner))
            nil))))))

; 動作確認
(assert! (equals
    (take-while '(1 3 5 7 9 11) (lambda (v) (< v 8)))
    '(1 3 5 7)))


;
; drop-while
; ===========================
;
; 例
;   (drop-while '(1 3 5 7 9 11) (lambda (v) (< v 8)))
;   -> (9 11)
;
(define drop-while
  (lambda (_drop-while-target-list _drop-while-validator)
    ((lambda (_drop-while-target-list _drop-while-validator _drop-while-inner)
        (_drop-while-inner
          _drop-while-target-list
          _drop-while-validator
          _drop-while-inner))
      _drop-while-target-list
      _drop-while-validator
      (lambda (_drop-while-target-list _drop-while-validator _drop-while-inner)
        (if (nil? _drop-while-target-list)
          nil
          (if (_drop-while-validator (car _drop-while-target-list))
            (_drop-while-inner
              (cdr _drop-while-target-list)
              _drop-while-validator
              _drop-while-inner)
            _drop-while-target-list))))))

; 動作確認
(assert! (equals
    (drop-while '(1 3 5 7 9 11) (lambda (v) (< v 8)))
    '(9 11)))


;
; fold
; ===========================
;
; 例
;   (fold '(1 2 3 4 5 6) 0 (lambda (v acc) (+ v (* acc 10))))
;   -> 123456
;
(define fold
  (lambda (_fold-target-list _fold-init _fold-folder)
    ((lambda (_fold-target-list _fold-init _fold-folder _fold-inner)
        (_fold-inner
          _fold-target-list
          _fold-init
          _fold-folder
          _fold-inner))
      _fold-target-list
      _fold-init
      _fold-folder
      (lambda (_fold-target-list _fold-accumulator _fold-folder _fold-inner)
        (if (nil? _fold-target-list)
          _fold-accumulator
          (_fold-inner
            (cdr _fold-target-list)
            (_fold-folder
              (car _fold-target-list)
              _fold-accumulator)
            _fold-folder
            _fold-inner))))))

; 動作確認
(assert! (equals
    (fold '(1 2 3 4 5 6) 0 (lambda (v acc) (+ v (* acc 10))))
    123456))


;
; fold-r
; ===========================
;
; 例
;   (fold-r '(1 2 3 4 5 6) 0 (lambda (v acc) (+ v (* acc 10))))
;   -> 654321
;
(define fold-r
  (lambda (_fold-r-target-list _fold-r-init _fold-r-folder)
    ((lambda (_fold-r-target-list _fold-r-init _fold-r-folder _fold-r-inner)
        (_fold-r-inner
          _fold-r-target-list
          _fold-r-init
          _fold-r-folder
          _fold-r-inner))
      _fold-r-target-list
      _fold-r-init
      _fold-r-folder
      (lambda (_fold-r-target-list _fold-r-init _fold-r-folder _fold-r-inner)
        (if (nil? _fold-r-target-list)
          _fold-r-init
          (_fold-r-folder
            (car _fold-r-target-list)
            (_fold-r-inner
              (cdr _fold-r-target-list)
              _fold-r-init
              _fold-r-folder
              _fold-r-inner)))))))

; 動作確認
(assert! (equals
    (fold-r '(1 2 3 4 5 6) 0 (lambda (v acc) (+ v (* acc 10))))
    654321))


;
; filter
; ===========================
;
; 例
;   (filter '(1 2 3 4 5 6) (lambda (v) (= 0 (rem v 2))))
;   -> (2 4 6)
;
(define filter
  (lambda (_filter-target-list _filter-validator)
    ((lambda (_filter-target-list _filter-validator _filter-inner)
        (_filter-inner
          _filter-target-list
          _filter-validator
          _filter-inner))
      _filter-target-list
      _filter-validator
      (lambda (_filter-target-list _filter-validator _filter-inner)
        (if (nil? _filter-target-list)
          nil
          (if (_filter-validator (car _filter-target-list))
            (cons
              (car _filter-target-list)
              (_filter-inner
                (cdr _filter-target-list)
                _filter-validator
                _filter-inner))
            (_filter-inner
              (cdr _filter-target-list)
              _filter-validator
              _filter-inner)))))))

; 動作確認
(assert! (equals
    (filter '(1 2 3 4 5 6) (lambda (v) (= 0 (rem v 2))))
    '(2 4 6)))


;
; do! return
; ===========================
; REQUIRE: fold-r
;
; 例
;   (do! 'foo
;     (println 1)
;     (do! 'bar
;       (println 2)
;       (return 'foo '(A B C))
;       (println 3)
;        'nil)
;     (println 4)
;     '(X Y Z))
;   -> (A B C)
;   OUTPUT:
;     1
;     2
;
; 例
;   (do! 'foo
;     (println 1)
;     (do! 'bar
;       (println 2)
;       (return 'bar '(A B C))
;       (println 3)
;       'nil)
;     (println 4)
;     '(X Y Z))
;   -> (X Y Z)
;   OUTPUT:
;     1
;     2
;     4
;
(define return (lambda ARGS (cons 'return ARGS)))
(macro do! (_DO-SYMBOL . _DO-ARGS)
    (fold-r _DO-ARGS nil
      (lambda (_DO-EXP _DO-ACC)
        (if (nil? _DO-ACC)
          _DO-EXP
          `((lambda (_DO-EVAL)
                (if (cond
                    ((atom _DO-EVAL) false)
                    (t (bool (eq 'return (car _DO-EVAL)))))
                  (cond
                    ((eq ,_DO-SYMBOL (car (cdr _DO-EVAL)))
                      (car (cdr (cdr _DO-EVAL))))
                    (t _DO-EVAL))
                  ,_DO-ACC))
              ,_DO-EXP)))))

; 動作確認
(assert! (equals
  '(A B C)
  (do! 'foo
    1
    (do! 'bar
      2
      (return 'foo '(A B C))
      3
      'nil)
    4
    '(X Y Z))))

; 動作確認
(assert! (equals
  '(X Y Z)
  (do! 'foo
    1
    (do! 'bar
      2
      (return 'bar '(A B C))
      3
      'nil)
    4
    '(X Y Z))))


;
; zip-with
; ===========================
;
; 例
;   (zip-with '(1 2 3 4 5) '(600 700 800 900) +)
;   -> (601 702 803 904)
;
; 例
;   (zip-with '(1 2 3) '(A B C D) (lambda (x y) (list y x y)))
;   -> ((A 1 A) (B 2 B) (C 3 C))
;
(define zip-with
  (lambda (_ZIP-WITH-LIST-1 _ZIP-WITH-LIST-2 _ZIP-WITH-APPLY-FUNC)
    ((lambda (_ZIP-WITH-LIST-1 _ZIP-WITH-LIST-2 _ZIP-WITH-APPLY-FUNC _ZIP-WITH-INNER)
        (_ZIP-WITH-INNER _ZIP-WITH-LIST-1 _ZIP-WITH-LIST-2 _ZIP-WITH-APPLY-FUNC _ZIP-WITH-INNER))
      _ZIP-WITH-LIST-1
      _ZIP-WITH-LIST-2
      _ZIP-WITH-APPLY-FUNC
      (lambda (_ZIP-WITH-LIST-1 _ZIP-WITH-LIST-2 _ZIP-WITH-APPLY-FUNC _ZIP-WITH-INNER)
        (if (nil? _ZIP-WITH-LIST-1)
          nil
          (if (nil? _ZIP-WITH-LIST-2)
            nil
            (cons
              (_ZIP-WITH-APPLY-FUNC
                (car _ZIP-WITH-LIST-1)
                (car _ZIP-WITH-LIST-2))
              (_ZIP-WITH-INNER
                (cdr _ZIP-WITH-LIST-1)
                (cdr _ZIP-WITH-LIST-2)
                _ZIP-WITH-APPLY-FUNC
                _ZIP-WITH-INNER))))))))

; 動作確認
(assert! (equals
    (zip-with '(1 2 3 4 5) '(600 700 800 900) +)
    '(601 702 803 904)))

; 動作確認
(assert! (equals
    (zip-with '(1 2 3) '(A B C D) (lambda (x y) (list y x y)))
    '((A 1 A) (B 2 B) (C 3 C))))


;
; zip!
; ===========================
; REQUIRE: zip-with
;
; 例
;   (zip! '(a b c d e) '(1 2 3 4 5))
;   -> ((A . 1) (B . 2) (C . 3) (D . 4) (E . 5))
;
; 例
;   (zip! '(a b c) '(1 2 3 4 5))
;   -> ((A . 1) (B . 2) (C . 3))
;
; 例
;   (zip! '(a b c d e) '(1 2 3))
;   -> ((A . 1) (B . 2) (C . 3))
;
(macro zip! (_ZIP-LIST-1 _ZIP-LIST-2)
    `(zip-with ,_ZIP-LIST-1 ,_ZIP-LIST-2 cons))

; 動作確認
(assert! (equals
  '((A . 1) (B . 2) (C . 3) (D . 4) (E . 5))
  (zip! '(a b c d e) '(1 2 3 4 5))))

; 動作確認
(assert! (equals
  '((A . 1) (B . 2) (C . 3))
  (zip! '(a b c) '(1 2 3 4 5))))

; 動作確認
(assert! (equals
  '((A . 1) (B . 2) (C . 3))
  (zip! '(a b c d e) '(1 2 3))))


;
; unzip
; ===========================
; REQUIRE: fold-r
;
; 例
;   (unzip '((1 . a) (2 . b) (3 . c) (4 . d)))
;   -> ((1 2 3 4) . (A B C D))
;
(define unzip
  (lambda (_UNZIP-LIST)
    (fold-r _UNZIP-LIST '(nil . nil)
      (lambda (_UNZIP-ELEM _UNZIP-ACC)
        (cons
          (cons (car _UNZIP-ELEM) (car _UNZIP-ACC))
          (cons (cdr _UNZIP-ELEM) (cdr _UNZIP-ACC)))))))

; 動作確認
(assert! (equals
  '((1 2 3 4) . (A B C D))
  (unzip '((1 . a) (2 . b) (3 . c) (4 . d)))))


;
; flatten
; ===========================
;
; 例
;   (flatten 'a '(b c) 'd '((e (f) g) . h) 'i)
;   -> (A B C D E F G H I)
;
(define flatten
  (lambda _FLATTEN-ARGS
    ((lambda (_FLATTEN-ARGS _FLATTEN-INIT _FLATTEN-INNER)
        (_FLATTEN-INNER _FLATTEN-ARGS _FLATTEN-INIT _FLATTEN-INNER))
      _FLATTEN-ARGS
      nil
      (lambda (_FLATTEN-ELEM _FLATTEN-ACC _FLATTEN-INNER)
        (cond
          ((atom _FLATTEN-ELEM)
            (cons _FLATTEN-ELEM _FLATTEN-ACC))
          (t
            (_FLATTEN-INNER
              (car _FLATTEN-ELEM)
              (if (nil? (cdr _FLATTEN-ELEM))
                _FLATTEN-ACC
                (_FLATTEN-INNER
                  (cdr _FLATTEN-ELEM)
                  _FLATTEN-ACC
                  _FLATTEN-INNER))
              _FLATTEN-INNER)))))))

; 動作確認
(assert! (equals
    (flatten 'a '(b c) 'd '((e (f) g) . h) 'i)
    '(A B C D E F G H I)))


;
; recfun!
; ===========================
;
; 例
;   (recfun! sum (ls)
;     (if (nil? ls)
;       0
;       (+ (car ls) (sum (cdr ls)))
;       ))
;   -> SUM
;   (sum '(1 2 3 4 5 6 7 8 9 10))
;   -> 55
;
(macro recfun! (name args body)
  `(define ,name
      (lambda ,args
        ((lambda (,@args ,name) (,name ,name ,@args))
          ,@args
          (lambda (,name ,@args)
            ,((lambda (body f) (f body f))
              body
              (lambda (body f)
                (if (bool (atom body))
                  body
                  (if (equals name (car body))
                    (cons name
                      (cons name
                        (f (cdr body) f)))
                    (cons
                      (f (car body) f)
                      (f (cdr body) f)
                      ))))))))))

; 動作確認
(recfun! sum (ls)
  (if (nil? ls)
    0
    (+ (car ls) (sum (cdr ls)))
    ))
(assert! (equals
  55
  (sum '(1 2 3 4 5 6 7 8 9 10))))
