use std::fmt;
use std::rc::Rc;

use mylisp_common::special_form::SpecialForm;

use super::{Code, ConsCell, DelayedValue};

#[derive(Debug, Clone, Copy)]
pub enum Case {
    Upper,
    Lower,
}

#[derive(Debug, Clone, Copy)]
pub enum Syntax {
    Shorten,
    Extend,
}

pub struct CodeDisplay<'a> {
    code: &'a Code,
    case: Case,
    syntax: Syntax,
}

impl Case {
    pub fn apply(self, s: &str) -> String {
        match self {
            Case::Lower => s.to_ascii_lowercase(),
            Case::Upper => s.to_ascii_uppercase(),
        }
    }
}

impl<'a> CodeDisplay<'a> {
    pub(super) fn new(code: &'a Code, case: Case, syntax: Syntax) -> CodeDisplay<'a> {
        CodeDisplay { code, case, syntax }
    }

    fn case(&self, s: &str) -> String {
        self.case.apply(s)
    }

    fn is_shorten(&self) -> bool {
        matches!(self.syntax, Syntax::Shorten)
    }
}

impl<'a> fmt::Debug for CodeDisplay<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(self.code, f)
    }
}

enum State {
    Top,
    Car,
    ListContinue,
    ListEndCar,
    ListEndNil,
    WithDot,
}

impl<'a> fmt::Display for CodeDisplay<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut stack = vec![self.code];
        let mut bracket = vec![];
        while let Some(code) = stack.pop() {
            let b = bracket.pop().unwrap_or(State::Top);
            if matches!(b, State::WithDot) {
                write!(f, ". ")?;
            }
            match code {
                Code::Nil => {
                    if !matches!(b, State::ListEndNil) {
                        if self.is_shorten() {
                            write!(f, "()")?;
                        } else {
                            write!(f, "{}", self.case("nil"))?;
                        }
                    }
                }
                Code::Cons(ref cell) => {
                    let ConsCell { ref car, ref cdr } = &**cell;
                    if !matches!(b, State::ListContinue) && self.is_shorten() {
                        if let Some((syntax, inner)) = get_special_syntax(car, cdr) {
                            write!(f, "{}", syntax)?;
                            bracket.push(b);
                            stack.push(inner);
                            continue;
                        }
                    }
                    if !matches!(b, State::ListContinue) {
                        write!(f, "(")?;
                    }
                    if cdr.is_nil() {
                        bracket.push(State::ListEndNil);
                        bracket.push(State::ListEndCar);
                    } else if cdr.is_cons() {
                        bracket.push(State::ListContinue);
                        bracket.push(State::Car);
                    } else {
                        bracket.push(State::WithDot);
                        bracket.push(State::Car);
                    }
                    stack.push(cdr);
                    stack.push(car);
                    continue;
                }
                Code::Symbol(ref s) => write!(f, "{}", self.case(s))?,
                Code::SpecialForm(_) => write!(f, "{:?}", code)?,
                Code::BuiltinFunc(_) => write!(f, "{:?}", code)?,
                Code::Lambda(ref info) => {
                    write!(f, "Lambda{{ id: {}, args: ", info.id)?;
                    fmt::Display::fmt(&info.args.display(self.case, self.syntax), f)?;
                    write!(f, ", body: ")?;
                    fmt::Display::fmt(&info.body.display(self.case, self.syntax), f)?;
                    write!(f, " }}")?;
                }
                Code::Closure(ref info) => {
                    write!(f, "Closure{{ id: {}, args: ", info.lambda_info.id)?;
                    fmt::Display::fmt(&info.lambda_info.args.display(self.case, self.syntax), f)?;
                    write!(f, ", body: ")?;
                    fmt::Display::fmt(&info.lambda_info.body.display(self.case, self.syntax), f)?;
                    write!(f, " }}")?;
                }
                Code::Macro(ref info) => {
                    write!(f, "Macro{{ id: {}, args: ", info.id)?;
                    fmt::Display::fmt(&info.args.display(self.case, self.syntax), f)?;
                    write!(f, ", generator: ")?;
                    fmt::Display::fmt(&info.body.display(self.case, self.syntax), f)?;
                    write!(f, " }}")?;
                }
                Code::MacroWith(ref info) => {
                    write!(f, "Macro{{ id: {}, args: ", info.lambda_info.id)?;
                    fmt::Display::fmt(&info.lambda_info.args.display(self.case, self.syntax), f)?;
                    write!(f, ", generator: ")?;
                    fmt::Display::fmt(&info.lambda_info.body.display(self.case, self.syntax), f)?;
                    write!(f, " }}")?;
                }
                Code::Next(ref info) => {
                    write!(
                        f,
                        "Next{{ symbol: {}, args: ",
                        self.case.apply(&*info.symbol)
                    )?;
                    fmt::Display::fmt(&info.args.display(self.case, self.syntax), f)?;
                    write!(f, " }}")?;
                }
                Code::IOFunc(_) => write!(f, "{:?}", code)?,
                Code::Delayed(ref dv) => {
                    write!(f, "Delayed{{ ")?;
                    match &*dv.borrow() {
                        DelayedValue::UnEvaluated(ref code, _) => {
                            write!(f, "UnEvaluated: ")?;
                            fmt::Display::fmt(&code.display(self.case, self.syntax), f)?;
                        }
                        DelayedValue::Evaluating => write!(f, "Evaluating")?,
                        DelayedValue::Evaluated(ref code) => {
                            write!(f, "Evaluated: ")?;
                            fmt::Display::fmt(&code.display(self.case, self.syntax), f)?;
                        }
                    }
                    write!(f, " }}",)?;
                }
                Code::InjectFunc(_) => write!(f, "{:?}", code)?,
                Code::True => write!(f, "{}", self.case("true"))?,
                Code::False => write!(f, "{}", self.case("false"))?,
                Code::Str(ref s) => write!(f, "{:?}", s)?,
                Code::Int32(v) => write!(f, "{}", v)?,
                Code::Int64(v) => write!(f, "{}", v)?,
                Code::Float64(v) => write!(f, "{}", v)?,
            }
            if matches!(b, State::ListContinue | State::WithDot | State::ListEndNil) {
                write!(f, ")")?;
                if let Some(last) = stack.last() {
                    if !last.is_nil() {
                        write!(f, " ")?;
                    }
                }
            } else if matches!(b, State::Car) {
                write!(f, " ")?;
            }
        }
        Ok(())
    }
}

fn get_special_syntax<'a>(
    car: &'a Rc<Code>,
    cdr: &'a Rc<Code>,
) -> Option<(&'static str, &'a Rc<Code>)> {
    let s = match **car {
        Code::Symbol(ref s) => s,
        _ => return None,
    };
    if let Code::Cons(ref cell) = **cdr {
        let ConsCell {
            car: ref cadr,
            cdr: ref cddr,
        } = &**cell;
        if !matches!(**cddr, Code::Nil) {
            return None;
        }
        if **s == SpecialForm::Quote.symbol() {
            return Some(("'", cadr));
        } else if **s == SpecialForm::Quasiquote.symbol() {
            return Some(("`", cadr));
        } else if **s == SpecialForm::Unquote.symbol() {
            return Some((",", cadr));
        } else if **s == SpecialForm::UnquoteSplicing.symbol() {
            return Some((",@", cadr));
        }
    }
    None
}
