use std::cell::RefCell;
use std::collections::HashMap;
use std::fmt;
use std::rc::Rc;

use mylisp_common::builtin_func::BuiltinFunc;
use mylisp_common::io_func::IOFunc;
use mylisp_common::special_form::SpecialForm;

pub use code_display::Case;
pub use code_display::CodeDisplay;
pub use code_display::Syntax;

mod code_display;

pub type Bindings = HashMap<Rc<String>, Rc<Code>>;

pub type InjectFunc = fn(&[Rc<Code>]) -> Result<Rc<Code>, String>;

#[derive(Debug)]
pub struct LambdaInfo {
    pub id: u32,
    pub arg_min_size: usize,
    pub arg_max_size: usize,
    pub args: Rc<Code>,
    pub body: Rc<Code>,
}

pub struct ClosureInfo {
    pub lambda_info: LambdaInfo,
    pub scope: Rc<Bindings>,
}

#[derive(Debug)]
pub struct ConsCell {
    pub car: Rc<Code>,
    pub cdr: Rc<Code>,
}

#[derive(Debug)]
pub struct NextInfo {
    pub symbol: Rc<String>,
    pub args: Rc<Code>,
}

#[derive(Debug)]
pub enum DelayedValue {
    UnEvaluated(Rc<Code>, Rc<Bindings>),
    Evaluating,
    Evaluated(Rc<Code>),
}

pub struct InjectFuncInfo {
    pub id: u32,
    pub name: Rc<String>,
    pub arg_min_size: usize,
    pub arg_max_size: usize,
    pub call_back: InjectFunc,
}

#[derive(Debug)]
pub enum Code {
    Nil,
    Cons(Box<ConsCell>),
    Symbol(Rc<String>),
    SpecialForm(SpecialForm),
    BuiltinFunc(BuiltinFunc),
    Lambda(Rc<LambdaInfo>),
    Closure(Rc<ClosureInfo>),
    Macro(Rc<LambdaInfo>),
    MacroWith(Rc<ClosureInfo>),
    Next(Box<NextInfo>),
    IOFunc(IOFunc),
    Delayed(Box<RefCell<DelayedValue>>),
    InjectFunc(Rc<InjectFuncInfo>),
    True,
    False,
    Str(Box<String>),
    Int32(i32),
    Int64(Box<i64>),
    Float64(Box<f64>),
}

impl Code {
    pub fn make_symbol(s: &str) -> Rc<Code> {
        Code::Symbol(Rc::new(s.to_string())).rc()
    }

    pub fn make_delayed(code: Rc<Code>, bindings: Bindings) -> Rc<Code> {
        let dv = DelayedValue::UnEvaluated(code, Rc::new(bindings));
        Code::Delayed(Box::new(RefCell::new(dv))).rc()
    }

    pub fn cons(car: Rc<Code>, cdr: Rc<Code>) -> Rc<Code> {
        Code::Cons(Box::new(ConsCell { car, cdr })).rc()
    }

    pub fn eq(code1: &Code, code2: &Code) -> Option<bool> {
        use Code::*;
        macro_rules! match2 {
            ($pat:pat $(if $expr:expr)?) => {
                match code2 {
                    $pat $(if $expr)? => Some(true),
                    Cons(_) => None,
                    SpecialForm(_) => None,
                    BuiltinFunc(_) => None,
                    Lambda(_) => None,
                    Closure(_) => None,
                    Macro(_) => None,
                    MacroWith(_) => None,
                    Next(_) => None,
                    IOFunc(_) => None,
                    Delayed(_) => None,
                    _ => Some(false),
                }
            };
        }
        match code1 {
            Nil => match2!(Nil),
            Cons(_) => None,
            Symbol(ref s1) => match2!(Symbol(ref s2) if s1 == s2),
            SpecialForm(_) => None,
            BuiltinFunc(_) => None,
            Lambda(_) => None,
            Closure(_) => None,
            Macro(_) => None,
            MacroWith(_) => None,
            Next(_) => None,
            IOFunc(_) => None,
            Delayed(_) => None,
            InjectFunc(_) => None,
            True => match2!(True),
            False => match2!(False),
            Str(ref s1) => match2!(Str(ref s2) if s1 == s2),
            Int32(v1) => match2!(Int32(v2) if v1 == v2),
            Int64(v1) => match2!(Int64(v2) if v1 == v2),
            Float64(v1) => match2!(Float64(v2) if v1 == v2),
        }
    }
}

impl Code {
    pub fn rc(self) -> Rc<Code> {
        Rc::new(self)
    }

    pub fn is_value(&self) -> bool {
        use Code::*;
        match self {
            Nil => false,
            Cons(_) => false,
            Symbol(_) => false,
            SpecialForm(_) => false,
            BuiltinFunc(_) => false,
            Lambda(_) => false,
            Closure(_) => false,
            Macro(_) => false,
            MacroWith(_) => false,
            Next(_) => false,
            IOFunc(_) => false,
            Delayed(_) => false,
            InjectFunc(_) => false,
            True => true,
            False => true,
            Str(_) => true,
            Int32(_) => true,
            Int64(_) => true,
            Float64(_) => true,
        }
    }

    pub fn is_list(&self) -> bool {
        let mut cur = self;
        loop {
            match cur {
                Code::Nil => return true,
                Code::Cons(ref cell) => cur = &cell.cdr,
                _ => return false,
            }
        }
    }

    pub fn list_depth(&self) -> Option<usize> {
        let mut depth = 0;
        let mut cur = self;
        loop {
            match cur {
                Code::Nil => return Some(depth),
                Code::Cons(ref cell) => {
                    depth += 1;
                    cur = &cell.cdr
                }
                _ => return None,
            }
        }
    }

    pub fn is_nil(&self) -> bool {
        matches!(self, Code::Nil)
    }

    pub fn is_cons(&self) -> bool {
        matches!(self, Code::Cons(_))
    }

    pub fn is_atom(&self) -> bool {
        !matches!(self, Code::Cons(_))
    }

    pub fn is_symbol(&self) -> bool {
        matches!(self, Code::Symbol(_))
    }

    pub fn get_symbol(&self) -> Option<Rc<String>> {
        if let Code::Symbol(ref s) = self {
            Some(s.clone())
        } else {
            None
        }
    }

    pub fn get_str(&self) -> Option<&str> {
        if let Code::Str(ref s) = self {
            Some(s)
        } else {
            None
        }
    }

    pub fn split(&self) -> Option<(&Rc<Code>, &Rc<Code>)> {
        match self {
            Code::Cons(ref cell) => Some((&cell.car, &cell.cdr)),
            _ => None,
        }
    }

    pub fn split_at(&self, size: usize) -> Option<(Vec<&Rc<Code>>, &Rc<Code>)> {
        let mut ret = Vec::with_capacity(size);
        let mut cur = self;
        loop {
            let (head, tail) = cur.split()?;
            ret.push(head);
            if ret.len() == size {
                return Some((ret, tail));
            }
            cur = tail;
        }
    }

    pub fn to_vec(&self) -> Option<Vec<Rc<Code>>> {
        let mut ret = vec![];
        let mut cur = self;
        loop {
            match cur {
                Code::Nil => return Some(ret),
                Code::Cons(ref cell) => {
                    ret.push(cell.car.clone());
                    cur = &cell.cdr;
                }
                _ => return None,
            }
        }
    }

    pub fn display(&self, case: Case, syntax: Syntax) -> CodeDisplay {
        CodeDisplay::new(self, case, syntax)
    }
}

impl LambdaInfo {
    pub fn validate_args_count(&self, args: &Rc<Code>) -> Option<bool> {
        args.list_depth().map(|size| self.check_args_size(size))
    }

    pub fn check_args_size(&self, size: usize) -> bool {
        self.arg_min_size <= size && size <= self.arg_max_size
    }
}

impl ClosureInfo {
    pub fn validate_args_count(&self, args: &Rc<Code>) -> Option<bool> {
        self.lambda_info.validate_args_count(args)
    }

    pub fn check_args_size(&self, size: usize) -> bool {
        self.lambda_info.check_args_size(size)
    }
}

impl fmt::Debug for ClosureInfo {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "ClosureInfo( {:?}, scope-key-size: {} )",
            self.lambda_info,
            self.scope.len()
        )
    }
}

impl fmt::Display for Code {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(&self.display(Case::Upper, Syntax::Extend), f)
    }
}

impl InjectFuncInfo {
    pub fn validate_args_count(&self, args: &Rc<Code>) -> Option<bool> {
        args.list_depth().map(|size| self.check_args_size(size))
    }

    pub fn check_args_size(&self, size: usize) -> bool {
        self.arg_min_size <= size && size <= self.arg_max_size
    }
}

impl fmt::Debug for InjectFuncInfo {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "InjectFuncInfo( id: {}, name: {}, arg_min_size: {}, arg_max_size: {} )",
            self.id, self.name, self.arg_min_size, self.arg_max_size
        )
    }
}
