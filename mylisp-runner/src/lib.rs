use std::io;

use mylisp_engine::{Engine, IOSet, Scope};
use mylisp_parser::Parser;
use mylisp_tokenizer::Tokenizer;

pub struct RunnerFlags {
    pub source_file: String,
    pub scope: Scope,
    pub preload_file: Option<String>,
}

pub fn print_runtime_error<E, ERR>(
    error: &mut E,
    err: &ERR,
    stack_trace: &[String],
) -> io::Result<()>
where
    E: io::Write,
    ERR: std::fmt::Display,
{
    writeln!(error, "Runtime Error: {}", err)?;
    error.flush()?;
    for s in stack_trace {
        writeln!(error, "  {}", s)?;
        error.flush()?;
    }
    Ok(())
}

pub fn run_file<RE, WE, EE>(engine: &mut Engine<RE, WE, EE>, file: &str) -> io::Result<()>
where
    RE: io::BufRead,
    WE: io::Write,
    EE: io::Write,
{
    let file = std::fs::File::open(file)?;
    let reader = io::BufReader::new(file);
    let tokenizer = Tokenizer::new(reader);
    let mut parser = Parser::new(tokenizer);
    loop {
        let code = match parser.parse()? {
            Err(err) => {
                let error = &mut engine.io_set.error;
                writeln!(error, "{}", err)?;
                error.flush()?;
                return Err(io::Error::new(io::ErrorKind::Other, "parse error"));
            }
            Ok(None) => return Ok(()),
            Ok(Some(code)) => code,
        };
        if let Err(err) = engine.eval(code) {
            let stack_trace = engine.get_stack_trace();
            print_runtime_error(&mut engine.io_set.error, &err, &stack_trace)?;
            return Err(io::Error::new(io::ErrorKind::Other, "runtime error"));
        }
    }
}

impl RunnerFlags {
    pub fn start<RE, WE, EE>(self, io_set_for_engine: IOSet<RE, WE, EE>) -> io::Result<()>
    where
        RE: io::BufRead,
        WE: io::Write,
        EE: io::Write,
    {
        let mut engine = Engine::with_scope(self.scope, io_set_for_engine);

        if let Some(ref preload_file) = self.preload_file {
            run_file(&mut engine, preload_file)?;
        }

        run_file(&mut engine, &self.source_file)
    }
}
