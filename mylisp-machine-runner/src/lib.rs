use std::io;

use mylisp_engine::IOSet;
use mylisp_machine::ProgramManager;
use mylisp_parser::Parser;
use mylisp_tokenizer::Tokenizer;

pub struct RunnerFlags {
    pub source_file: String,
    pub preload_file: Option<String>,
}

pub fn run_file<RE, WE, EE>(manager: &mut ProgramManager<RE, WE, EE>, file: &str) -> io::Result<()>
where
    RE: io::BufRead,
    WE: io::Write,
    EE: io::Write,
{
    let file = std::fs::File::open(file)?;
    let reader = io::BufReader::new(file);
    let tokenizer = Tokenizer::new(reader);
    let mut parser = Parser::new(tokenizer);
    loop {
        let code = match parser.parse()? {
            Err(err) => {
                let error = &mut manager.io_set().error;
                writeln!(error, "PARSE ERROR: {}", err)?;
                error.flush()?;
                return Err(io::Error::new(io::ErrorKind::Other, "parse error"));
            }
            Ok(None) => return Ok(()),
            Ok(Some(code)) => code,
        };
        let program = match manager.compile(code) {
            Ok(program) => program,
            Err(err) => {
                let error = &mut manager.io_set().error;
                writeln!(error, "COMPILE ERROR: {}", err)?;
                error.flush()?;
                return Err(io::Error::new(io::ErrorKind::Other, "compile error"));
            }
        };
        if let Err(err) = program.run() {
            let error = &mut manager.io_set().error;
            writeln!(error, "RUNTIME ERROR: {}", err)?;
            error.flush()?;
            return Err(io::Error::new(io::ErrorKind::Other, "runtime error"));
        }
    }
}

impl RunnerFlags {
    pub fn start<RE, WE, EE>(self, io_set_for_machine: IOSet<RE, WE, EE>) -> io::Result<()>
    where
        RE: io::BufRead,
        WE: io::Write,
        EE: io::Write,
    {
        let mut manager = ProgramManager::new(io_set_for_machine);

        if let Some(ref preload_file) = self.preload_file {
            run_file(&mut manager, preload_file)?;
        }

        run_file(&mut manager, &self.source_file)
    }
}
