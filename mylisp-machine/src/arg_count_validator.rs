use crate::builtin_func::BuiltinFunc;
use crate::code::LambdaInfo;
use crate::io_func::IOFunc;

use super::MAX_ARG_SIZE;

pub(super) trait ArgCountValidator {
    fn validate_arg_count(&self, len: usize) -> bool;
}

impl ArgCountValidator for LambdaInfo {
    fn validate_arg_count(&self, len: usize) -> bool {
        (self.arg_min_size..=self.arg_max_size).contains(&len)
    }
}

impl ArgCountValidator for BuiltinFunc {
    fn validate_arg_count(&self, len: usize) -> bool {
        use BuiltinFunc::*;
        match self {
            Atom => len == 1,
            Car => len == 1,
            Cdr => len == 1,
            Cons => len == 2,
            Eq => len == 2,
            NumAdd => len <= MAX_ARG_SIZE,
            NumSub => (1..=MAX_ARG_SIZE).contains(&len),
            NumMul => len <= MAX_ARG_SIZE,
            NumIDiv => (2..=MAX_ARG_SIZE).contains(&len),
            NumFDiv => (2..=MAX_ARG_SIZE).contains(&len),
            NumRem => (2..=MAX_ARG_SIZE).contains(&len),
            NumEq => (2..=MAX_ARG_SIZE).contains(&len),
            NumLT => (2..=MAX_ARG_SIZE).contains(&len),
            NumLE => (2..=MAX_ARG_SIZE).contains(&len),
            NumGT => (2..=MAX_ARG_SIZE).contains(&len),
            NumGE => (2..=MAX_ARG_SIZE).contains(&len),
            Bool => len == 1,
            And => len <= MAX_ARG_SIZE,
            Or => len <= MAX_ARG_SIZE,
            Not => len == 1,
            TimeNow => len == 0,
            StrConcat => (1..=MAX_ARG_SIZE).contains(&len),
            StrSubstr => (2..=3).contains(&len),
            StrSplit => (1..=2).contains(&len),
            StrToNum => (1..=2).contains(&len),
            NumToStr => len == 1,
            StrCompare => len == 2,
            StrLength => len == 1,
            NumCastInt32 => len == 1,
            NumCastInt64 => len == 1,
            NumCastFloat64 => len == 1,
            BitwiseAnd => len <= MAX_ARG_SIZE,
            BitwiseOr => len <= MAX_ARG_SIZE,
            BitwiseXor => len <= MAX_ARG_SIZE,
            BitwiseNot => len == 1,
            BitwiseShiftLeft => len == 2,
            BitwiseShiftRight => len == 2,
            NumToBinStr => len == 1,
            NumToHexStr => len == 1,
            IsNil => (1..=MAX_ARG_SIZE).contains(&len),
            IsList => (1..=MAX_ARG_SIZE).contains(&len),
            MakeList => len <= MAX_ARG_SIZE,
            Equals => (2..=MAX_ARG_SIZE).contains(&len),
        }
    }
}

impl ArgCountValidator for IOFunc {
    fn validate_arg_count(&self, len: usize) -> bool {
        use IOFunc::*;
        match self {
            Println => len <= MAX_ARG_SIZE,
            Readln => len == 0,
            Eprintln => len <= MAX_ARG_SIZE,
            RaiseError => len <= MAX_ARG_SIZE,
        }
    }
}
