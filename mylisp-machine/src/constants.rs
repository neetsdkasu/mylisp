use std::collections::HashMap;
use std::rc::Rc;

use crate::builtin_func::BuiltinFunc;
use crate::code::{Case, Code, CodeDisplay, Syntax};
use crate::io_func::IOFunc;
use crate::special_form::SpecialForm;

pub struct Constants {
    pub(super) builtin_funcs: HashMap<String, BuiltinFunc>,
    pub(super) special_forms: HashMap<String, SpecialForm>,
    pub(super) io_funcs: HashMap<String, IOFunc>,
    code_nil: Rc<Code>,
    code_t: Rc<Code>,
    code_true: Rc<Code>,
    code_false: Rc<Code>,
    code_zero: Rc<Code>,
    code_positive_one: Rc<Code>,
    code_negative_one: Rc<Code>,
    pub(super) case: Case,
    pub(super) syntax: Syntax,
}

impl Constants {
    #[allow(dead_code)]
    pub fn new() -> Constants {
        Constants::new_with(Case::Upper, Syntax::Extend)
    }

    pub fn new_with(case: Case, syntax: Syntax) -> Constants {
        let mut builtin_funcs = HashMap::new();
        for f in BuiltinFunc::iter() {
            let key = f.symbol().to_string();
            builtin_funcs.insert(key, f);
        }
        let mut special_forms = HashMap::new();
        for f in SpecialForm::iter() {
            let key = f.symbol().to_string();
            special_forms.insert(key, f);
        }
        let mut io_funcs = HashMap::new();
        for f in IOFunc::iter() {
            let key = f.symbol().to_string();
            io_funcs.insert(key, f);
        }
        Constants {
            builtin_funcs,
            special_forms,
            io_funcs,
            code_nil: Code::Nil.rc(),
            code_t: Code::make_symbol("t"),
            code_true: Code::True.rc(),
            code_false: Code::False.rc(),
            code_zero: Code::Int32(0).rc(),
            code_positive_one: Code::Int32(1).rc(),
            code_negative_one: Code::Int32(-1).rc(),
            case,
            syntax,
        }
    }
}

impl Constants {
    pub(super) fn code_nil(&self) -> Rc<Code> {
        self.code_nil.clone()
    }

    pub(super) fn code_t(&self) -> Rc<Code> {
        self.code_t.clone()
    }

    pub(super) fn code_true(&self) -> Rc<Code> {
        self.code_true.clone()
    }

    pub(super) fn code_false(&self) -> Rc<Code> {
        self.code_false.clone()
    }

    pub(super) fn code_bool(&self, value: bool) -> Rc<Code> {
        if value {
            self.code_true()
        } else {
            self.code_false()
        }
    }

    pub(super) fn code_zero(&self) -> Rc<Code> {
        self.code_zero.clone()
    }

    pub(super) fn code_positive_one(&self) -> Rc<Code> {
        self.code_positive_one.clone()
    }

    pub(super) fn code_negative_one(&self) -> Rc<Code> {
        self.code_negative_one.clone()
    }

    pub(super) fn get_display<'a>(&self, code: &'a Code) -> CodeDisplay<'a> {
        code.display(self.case, self.syntax)
    }

    pub(super) fn get_short_display<'a>(&self, code: &'a Code) -> CodeDisplay<'a> {
        self.get_display(code)
    }
}
