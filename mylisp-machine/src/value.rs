use std::fmt;
use std::rc::Rc;

use crate::code::Code;

pub enum Value {
    ArgCount(isize),
    Code(Rc<Code>),
    LambdaId(usize),
}

impl Value {
    pub fn rc(self) -> Rc<Value> {
        Rc::new(self)
    }

    pub fn get_code(&self) -> Option<&Rc<Code>> {
        match self {
            Value::Code(code) => Some(code),
            _ => None,
        }
    }

    pub fn arg_count(&self) -> Option<isize> {
        match self {
            Value::ArgCount(count) => Some(*count),
            _ => None,
        }
    }
}

impl fmt::Debug for Value {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Value::ArgCount(count) => write!(f, "ArgCount{{ {} }}", *count),
            Value::Code(ref code) => write!(f, "Code{{ {} }}", code),
            Value::LambdaId(id) => write!(f, "LambdaId{{ {} }}", id),
        }
    }
}
