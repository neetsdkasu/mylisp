use std::collections::HashMap;
use std::rc::Rc;

use crate::code::Code;
use crate::special_form::SpecialForm;

use super::arg_count_validator::ArgCountValidator;
use super::command::Command;
use super::constants::Constants;
use super::program_store::{Closure, ProgramStore};
use super::MAX_ARG_SIZE;

type Error = String;
type CompileResult = Result<(), Error>;

pub(super) type Bindings = HashMap<Rc<String>, Command>;

pub struct Compiler<'a> {
    constants: &'a Constants,
    scope_bindings: Bindings,
    request_bindings: Bindings,
}

impl<'a> Compiler<'a> {
    pub fn new(constants: &'a Constants) -> Compiler {
        Compiler::with_scope_bindings(constants, Bindings::new())
    }

    pub fn with_scope_bindings(constants: &'a Constants, scope_bindings: Bindings) -> Compiler {
        Compiler {
            constants,
            scope_bindings,
            request_bindings: Bindings::new(),
        }
    }

    pub fn release_scope_bindings(self) -> Bindings {
        self.scope_bindings
    }
}

mod compile_list;
mod compile_value;

impl<'a> Compiler<'a> {
    pub fn compile(
        &mut self,
        store: &mut ProgramStore,
        code: &Rc<Code>,
    ) -> Result<Vec<Command>, Error> {
        let mut commands: Vec<Command> = Vec::new();

        self.request_bindings.clear();

        self.compile_value(store, true, &mut commands, code)?;

        if !self.request_bindings.is_empty() {
            let mut msg = String::new();
            msg += "unknown names: ";
            for (k, _) in self.request_bindings.iter() {
                msg += &**k;
                msg += ", ";
            }
            return Err(msg);
        }

        commands.push(Command::Ret);

        Ok(commands)
    }
}

impl<'a> Compiler<'a> {
    fn validate_name(&self, symbol: &str) -> bool {
        !["t", "nil", "true", "false"].contains(&symbol)
            && !self.constants.builtin_funcs.contains_key(symbol)
            && !self.constants.special_forms.contains_key(symbol)
            && !self.constants.io_funcs.contains_key(symbol)
    }
}

mod cond_compiler;
mod define_compiler;
mod delay_compiler;
mod delay_get_compiler;
mod if_compiler;
mod lambda_compiler;
mod macro_compiler;
mod next_compiler;
mod quasiquote_compiler;
mod quote_compiler;
mod recur_compiler;

impl<'a> Compiler<'a> {
    fn compile_special_form(
        &mut self,
        store: &mut ProgramStore,
        top_level: bool,
        commands: &mut Vec<Command>,
        list: &[Rc<Code>],
        sf: SpecialForm,
    ) -> CompileResult {
        use SpecialForm::*;
        match sf {
            Cond => cond_compiler::compile(self, store, commands, list),
            Define => define_compiler::compile(self, store, top_level, commands, list),
            Lambda => lambda_compiler::compile(self, store, commands, list),
            Quote => quote_compiler::compile(commands, list),
            Quasiquote => quasiquote_compiler::compile(self, store, commands, list),
            Unquote => Err("invalid Unquote".to_string()),
            UnquoteSplicing => Err("invalid UnquoteSplicing".to_string()),
            Macro => macro_compiler::compile(self, store, top_level, commands, list),
            If => if_compiler::compile(self, store, commands, list),
            Recur => recur_compiler::compile(self, store, commands, list),
            Next => next_compiler::compile(self, store, commands, list),
            Delay => delay_compiler::compile(self, store, commands, list),
            DelayGet => delay_get_compiler::compile(self, store, commands, list),
        }
    }
}
