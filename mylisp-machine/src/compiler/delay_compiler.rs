use std::rc::Rc;

use crate::code::{Code, LambdaInfo};

use super::Closure;
use super::Command;
use super::CompileResult;
use super::Compiler;
use super::ProgramStore;

pub(super) fn compile(
    compiler: &mut Compiler,
    store: &mut ProgramStore,
    commands: &mut Vec<Command>,
    list: &[Rc<Code>],
) -> CompileResult {
    if list.len() != 1 {
        return Err("illegal argument count Delay".to_string());
    }

    let mut delay_commands: Vec<Command> = Vec::new();

    let mut delay_compiler = Compiler::new(compiler.constants);

    delay_compiler.compile_value(store, false, &mut delay_commands, &list[0])?;

    let delay_request_bindings = delay_compiler.request_bindings;

    delay_commands.push(Command::BindDelay);
    delay_commands.push(Command::Ret);

    let mut temp_bind_map: Vec<(usize, Rc<String>)> = Vec::new();

    for (k, v) in delay_request_bindings.iter() {
        if let Command::PushBindCopy(pos) = v {
            temp_bind_map.push((*pos, k.clone()));
        } else {
            return Err(bug!());
        }
    }

    temp_bind_map.sort();

    let mut bind_map: Vec<Rc<String>> = Vec::new();

    for (i, (pos, key)) in temp_bind_map.into_iter().enumerate() {
        if i != pos {
            return Err(bug!());
        }
        if let Some(cmd) = compiler.scope_bindings.get(&*key) {
            commands.push(cmd.clone());
        } else if let Some(cmd) = compiler.request_bindings.get(&*key) {
            commands.push(cmd.clone());
        } else {
            let id = compiler.request_bindings.len();
            let cmd = Command::PushBindCopy(id);
            compiler.request_bindings.insert(key.clone(), cmd.clone());
            commands.push(cmd);
        }
        bind_map.push(key);
    }

    commands.push(Command::PushArgCount(bind_map.len() as isize));

    let cl_id = store.closures.len();
    let cl = Closure {
        bind_map,
        commands: Rc::new(delay_commands),
        info: LambdaInfo {
            id: cl_id as u32,
            arg_min_size: 0,
            arg_max_size: 0,
            args: compiler.constants.code_nil(),
            body: list[0].clone(),
        },
    };
    store.closures.push(Rc::new(cl));

    commands.push(Command::PushLambdaId(cl_id));
    commands.push(Command::MakeClosure);
    commands.push(Command::MakeDelay);

    Ok(())
}
