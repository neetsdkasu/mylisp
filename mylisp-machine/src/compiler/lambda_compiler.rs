use std::rc::Rc;

use crate::code::{Code, LambdaInfo};

use super::Bindings;
use super::Closure;
use super::Command;
use super::CompileResult;
use super::Compiler;
use super::ProgramStore;
use super::MAX_ARG_SIZE;

pub(super) fn compile(
    compiler: &mut Compiler,
    store: &mut ProgramStore,
    commands: &mut Vec<Command>,
    list: &[Rc<Code>],
) -> CompileResult {
    if list.len() != 2 {
        return Err("illegal argument count Lambda".to_string());
    }

    // lambda_commands、引数分解からbodyのコンパイル物、そしてret命令を含む
    // lambda_bindings、引数分解後の各引数のスタック位置を保持
    let mut lambda_commands: Vec<Command> = Vec::new();
    let mut lambda_arg_bindings = Bindings::new();

    // list[0] 引数部
    // コールスタックの受け取りを引数のパターンにあわせて分解する命令群を生成しなければならない
    // (lambda A ..)
    // (lambda () ..)
    // (lambda (A B . C) ..) -> A,Bでさらにパターン ※Cがパターンはありえない C=(D.E)なら (A B . (D . E)) は (A B D . E) と同じ(Parserで同じになる)
    // (lambda (A B C) ..) -> A,B,Cでさらにパターン

    let arg_min_size: usize;
    let arg_max_size: usize;

    if list[0].is_nil() {
        arg_min_size = 0;
        arg_max_size = 0;
    } else if let Some(sym) = list[0].get_symbol() {
        if !compiler.validate_name(&*sym) {
            return Err("invalid argument name".to_string());
        }
        lambda_commands.push(Command::MakeListFromAllArgs);
        lambda_commands.push(Command::PushStackCopy(-1));
        lambda_commands.push(Command::PushArgCount(2));
        lambda_commands.push(Command::SumArgCount);
        lambda_arg_bindings.insert(sym, Command::PushStackCopy(0));
        arg_min_size = 0;
        arg_max_size = MAX_ARG_SIZE;
    } else if let Some(arg_list) = list[0].to_vec() {
        arg_min_size = arg_list.len();
        arg_max_size = arg_min_size;
        let arg_pos: isize = -1 - arg_list.len() as isize;
        let mut spos: isize = 0;
        for (i, c) in arg_list.iter().enumerate() {
            let pos: isize = arg_pos + i as isize;
            if let Some(sym) = c.get_symbol() {
                if !compiler.validate_name(&*sym) {
                    return Err("invalid argument name".to_string());
                }
                lambda_arg_bindings.insert(sym, Command::PushStackCopy(pos));
            } else if let Code::Cons(cell) = &**c {
                let mut temp = Vec::new();
                temp.push((vec![Command::Cdr], &cell.cdr));
                temp.push((vec![Command::Car], &cell.car));
                while let Some((mut cmds, code)) = temp.pop() {
                    if let Some(sym) = code.get_symbol() {
                        if !compiler.validate_name(&*sym) {
                            return Err("invalid argument name".to_string());
                        }
                        lambda_commands.push(Command::PushStackCopy(pos));
                        lambda_commands.append(&mut cmds);
                        lambda_arg_bindings.insert(sym, Command::PushStackCopy(spos));
                        spos += 1;
                    } else if let Code::Cons(cell) = &**code {
                        let mut cmds_cdr = cmds.clone();
                        cmds_cdr.push(Command::Cdr);
                        temp.push((cmds_cdr, &cell.cdr));
                        cmds.push(Command::Car);
                        temp.push((cmds, &cell.car));
                    } else if !code.is_nil() {
                        return Err("invalid argument".to_string());
                    }
                }
            } else {
                return Err("invalid argument name".to_string());
            }
        }
        if spos > 0 {
            lambda_commands.push(Command::PushStackCopy(-1));
            lambda_commands.push(Command::PushArgCount(spos + 1));
            lambda_commands.push(Command::SumArgCount);
        }
    } else if let Code::Cons(ref cell) = &*list[0] {
        let mut temp_arg_min_size: isize = 1;
        lambda_commands.push(Command::MakeListFromAllArgs);
        let mut spos: isize = 1;
        let mut temp = vec![
            (vec![Command::Cdr], &cell.cdr, 1),
            (vec![Command::Car], &cell.car, -1),
        ];
        while let Some((mut cmds, code, len)) = temp.pop() {
            temp_arg_min_size = temp_arg_min_size.max(len);
            if let Some(sym) = code.get_symbol() {
                if !compiler.validate_name(&*sym) {
                    return Err("invalid argument name".to_string());
                }
                lambda_commands.push(Command::PushStackCopy(0));
                lambda_commands.append(&mut cmds);
                lambda_arg_bindings.insert(sym, Command::PushStackCopy(spos));
                spos += 1;
            } else if let Code::Cons(ref cell) = &**code {
                let mut cmds_cdr = cmds.clone();
                cmds_cdr.push(Command::Cdr);
                temp.push((cmds_cdr, &cell.cdr, len + len.signum()));
                cmds.push(Command::Car);
                temp.push((cmds, &cell.car, -1));
            } else if !code.is_nil() {
                return Err("invalid argument name".to_string());
            }
        }
        if spos > 0 {
            lambda_commands.push(Command::PushStackCopy(-1));
            lambda_commands.push(Command::PushArgCount(spos + 1));
            lambda_commands.push(Command::SumArgCount);
        }
        arg_min_size = temp_arg_min_size as usize;
        arg_max_size = MAX_ARG_SIZE;
    } else {
        return Err("invalid argument list".to_string());
    }

    let mut lambda_compiler =
        Compiler::with_scope_bindings(compiler.constants, lambda_arg_bindings);

    // list[1] body部
    // lambda_commandsとlambda_bindingsを用いて
    // compiler.compile_valueを呼び出す
    // ※compile_blockの存在意義が無い…
    lambda_compiler.compile_value(store, false, &mut lambda_commands, &list[1])?;

    let lambda_request_bindings = lambda_compiler.request_bindings;

    lambda_commands.push(Command::RetWithCleanStack);

    // lambda_bindingsに引数以外が含まれる場合に
    // その分をbindingsから引っ張ってくるように
    // commandsに命令を追加する
    let mut temp_bind_map: Vec<(usize, Rc<String>)> = Vec::new();

    for (k, v) in lambda_request_bindings.iter() {
        if let Command::PushBindCopy(pos) = v {
            temp_bind_map.push((*pos, k.clone()));
        } else {
            return Err(bug!());
        }
    }

    temp_bind_map.sort();

    let mut bind_map: Vec<Rc<String>> = Vec::new();

    for (i, (pos, key)) in temp_bind_map.into_iter().enumerate() {
        if i != pos {
            return Err(bug!());
        }
        if let Some(cmd) = compiler.scope_bindings.get(&*key) {
            commands.push(cmd.clone());
        } else if let Some(cmd) = compiler.request_bindings.get(&*key) {
            commands.push(cmd.clone());
        } else {
            let id = compiler.request_bindings.len();
            let cmd = Command::PushBindCopy(id);
            compiler.request_bindings.insert(key.clone(), cmd.clone());
            commands.push(cmd);
        }
        bind_map.push(key);
    }

    commands.push(Command::PushArgCount(bind_map.len() as isize));

    let lambda_id = store.closures.len();

    let closure = Closure {
        bind_map,
        commands: Rc::new(lambda_commands),
        info: LambdaInfo {
            id: lambda_id as u32,
            arg_min_size,
            arg_max_size,
            args: list[0].clone(),
            body: list[1].clone(),
        },
    };

    store.closures.push(Rc::new(closure));

    commands.push(Command::PushLambdaId(lambda_id));
    commands.push(Command::MakeClosure);

    Ok(())
}
