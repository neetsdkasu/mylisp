use std::rc::Rc;

use crate::code::Code;

use super::Command;
use super::CompileResult;
use super::Compiler;
use super::ProgramStore;

pub(super) fn compile(
    compiler: &mut Compiler,
    store: &mut ProgramStore,
    commands: &mut Vec<Command>,
    list: &[Rc<Code>],
) -> CompileResult {
    if list.len() != 1 {
        return Err("illegal argument count DelayGet".to_string());
    }

    compiler.compile_value(store, false, commands, &list[0])?;

    commands.push(Command::DelayGet);

    Ok(())
}
