use std::rc::Rc;

use crate::code::Code;

use super::Command;
use super::CompileResult;
use super::Compiler;
use super::ProgramStore;

pub(super) fn compile(
    compiler: &mut Compiler,
    store: &mut ProgramStore,
    top_level: bool,
    commands: &mut Vec<Command>,
    list: &[Rc<Code>],
) -> CompileResult {
    if !top_level {
        return Err("not top-level Define".to_string());
    }

    if list.len() != 2 {
        return Err("illegal argument count of Define".to_string());
    }

    let sym = list[0]
        .get_symbol()
        .ok_or_else(|| "first argument must symbol Define".to_string())?;

    if !compiler.validate_name(&*sym) {
        return Err("invalid name Define".to_string());
    }

    let pos = match compiler.scope_bindings.get(&*sym) {
        Some(Command::PushGlobalVar(pos)) => *pos,
        Some(_) => return Err(bug!()),
        None => compiler.scope_bindings.len(),
    };

    // ラムダ・クロージャならここに記述で再帰関数を組めるようになるが…
    // 値やコンス・リストの生成で自己参照の場合は無限ループになる…
    // let cmd = Command::PushGlobalVar(pos);
    // bindings.insert(sym, cmd);

    let len = compiler.request_bindings.len();

    compiler.compile_value(store, false, commands, &list[1])?;

    if compiler.request_bindings.len() != len {
        // Okで返して呼び出し元のほうで処理するほうがいいか？
        // Errで返すほうが直感的なんだが…
        return Ok(());
    }

    // ここに記述だと自己参照不可能扱いになる
    let cmd = Command::PushGlobalVar(pos);
    compiler.scope_bindings.insert(sym, cmd);

    commands.push(Command::BindGlobal(pos));

    Ok(())
}
