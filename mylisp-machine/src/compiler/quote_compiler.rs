use std::rc::Rc;

use crate::code::Code;

use super::Command;
use super::CompileResult;

pub(super) fn compile(commands: &mut Vec<Command>, list: &[Rc<Code>]) -> CompileResult {
    if list.len() != 1 {
        return Err("illegal argument count of Quote".to_string());
    }

    commands.push(Command::PushCode(list[0].clone()));

    Ok(())
}
