use std::rc::Rc;

use crate::code::Code;

use super::Command;
use super::CompileResult;
use super::Compiler;
use super::ProgramStore;
use super::MAX_ARG_SIZE;

pub(super) fn compile(
    compiler: &mut Compiler,
    store: &mut ProgramStore,
    commands: &mut Vec<Command>,
    list: &[Rc<Code>],
) -> CompileResult {
    if list.len() > MAX_ARG_SIZE {
        return Err("too many arg count Cond".to_string());
    }

    if list.is_empty() {
        commands.push(Command::PushCode(compiler.constants.code_nil()));
        return Ok(());
    }

    let mut jump_end_cmd_poss: Vec<usize> = vec![];

    for elem in list.iter() {
        let (cr, _) = elem
            .split_at(2)
            .filter(|(_, rest)| rest.is_nil())
            .ok_or_else(|| "illegal argument Cond".to_string())?;

        compiler.compile_value(store, false, commands, cr[0])?;

        let jump_next_cmd_pos = commands.len();
        commands.push(Command::JumpIfNil(0));

        compiler.compile_value(store, false, commands, cr[1])?;

        jump_end_cmd_poss.push(commands.len());
        commands.push(Command::Jump(0));

        let cur_pos = commands.len();
        commands[jump_next_cmd_pos] = Command::JumpIfNil(cur_pos);
    }

    commands.push(Command::PushCode(compiler.constants.code_nil()));

    let end_pos = commands.len();
    for pos in jump_end_cmd_poss {
        commands[pos] = Command::Jump(end_pos);
    }

    Ok(())
}
