use std::rc::Rc;

use crate::code::Code;

use super::Command;
use super::CompileResult;
use super::Compiler;
use super::ProgramStore;

pub(super) fn compile(
    compiler: &mut Compiler,
    store: &mut ProgramStore,
    commands: &mut Vec<Command>,
    list: &[Rc<Code>],
) -> CompileResult {
    if list.len() != 3 {
        return Err("illegal argument count Recur".to_string());
    }

    compiler.compile_value(store, false, commands, &list[0])?;

    let args = list[1]
        .to_vec()
        .ok_or_else(|| "illegal argument inits Recur".to_string())?;

    for code in args.iter() {
        compiler.compile_value(store, false, commands, code)?;
    }

    compiler.compile_value(store, false, commands, &list[2])?;

    commands.push(Command::Dup);
    commands.push(Command::RotateForward(args.len() + 2));
    commands.push(Command::RotateForward(args.len() + 1));
    commands.push(Command::PushArgCount(args.len() as isize + 1));
    commands.push(Command::CallUnknownFunc);
    commands.push(Command::CheckNext);

    Ok(())
}
