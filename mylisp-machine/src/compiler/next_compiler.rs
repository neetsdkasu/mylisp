use std::rc::Rc;

use crate::code::Code;

use super::Command;
use super::CompileResult;
use super::Compiler;
use super::ProgramStore;

pub(super) fn compile(
    compiler: &mut Compiler,
    store: &mut ProgramStore,
    commands: &mut Vec<Command>,
    list: &[Rc<Code>],
) -> CompileResult {
    if list.is_empty() {
        return Err("illegal argument count Next".to_string());
    }

    for code in list.iter() {
        compiler.compile_value(store, false, commands, code)?;
    }

    commands.push(Command::PushArgCount(list.len() as isize));
    commands.push(Command::MakeNext);

    Ok(())
}
