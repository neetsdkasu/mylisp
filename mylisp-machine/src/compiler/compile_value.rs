use std::rc::Rc;

use crate::code::{Code, LambdaInfo};

use super::Command;
use super::CompileResult;
use super::Compiler;
use super::ProgramStore;

impl<'a> Compiler<'a> {
    pub(super) fn compile_value(
        &mut self,
        store: &mut ProgramStore,
        top_level: bool,
        commands: &mut Vec<Command>,
        elem: &Rc<Code>,
    ) -> CompileResult {
        if let Some(list) = elem.to_vec().filter(|list| !list.is_empty()) {
            self.compile_list(store, top_level, commands, &list)?;
        } else if let Some(sym) = elem.get_symbol() {
            if *sym == "t" {
                commands.push(Command::PushCode(self.constants.code_t()));
            } else if *sym == "nil" {
                commands.push(Command::PushCode(self.constants.code_nil()));
            } else if *sym == "true" {
                commands.push(Command::PushCode(self.constants.code_true()));
            } else if *sym == "false" {
                commands.push(Command::PushCode(self.constants.code_false()));
            } else if let Some(f) = self.constants.builtin_funcs.get(&*sym) {
                commands.push(Command::PushBuiltinFunc(*f));
            } else if let Some(f) = self.constants.special_forms.get(&*sym) {
                commands.push(Command::PushSpecialForm(*f));
            } else if let Some(f) = self.constants.io_funcs.get(&*sym) {
                commands.push(Command::PushIOFunc(*f));
            } else if let Some(cmd) = self.scope_bindings.get(&*sym) {
                commands.push(cmd.clone());
            } else if let Some(cmd) = self.request_bindings.get(&*sym) {
                commands.push(cmd.clone());
            } else if let Some(binded_cl) = store.macros.get(&*sym) {
                let cl = &store.closures[binded_cl.id];
                let info = LambdaInfo {
                    id: cl.info.id,
                    arg_min_size: cl.info.arg_min_size,
                    arg_max_size: cl.info.arg_max_size,
                    args: cl.info.args.clone(),
                    body: cl.info.body.clone(),
                };
                let code = Code::Macro(Rc::new(info));
                commands.push(Command::PushCode(code.rc()));
            } else {
                let pos = self.request_bindings.len();
                let cmd = Command::PushBindCopy(pos);
                self.request_bindings.insert(sym, cmd.clone());
                commands.push(cmd);
            }
        } else if !matches!(elem.as_ref(), Code::Cons(_)) {
            // シンボル・リスト以外の評価可能値が来る
            // 通常は、数値・文字列・真偽値・nilのソースコード上のリテラルデータ
            // マクロ展開した場合は、関数・ラムダ・遅延ホルダなどの各種実体がくるかも
            commands.push(Command::PushCode(Rc::clone(elem)));
        } else {
            return Err(format!("cannot evaluate [ {} ]", elem));
        }

        Ok(())
    }
}
