use std::rc::Rc;

use crate::code::Code;

use super::Command;
use super::CompileResult;
use super::Compiler;
use super::ProgramStore;

pub(super) fn compile(
    compiler: &mut Compiler,
    store: &mut ProgramStore,
    commands: &mut Vec<Command>,
    list: &[Rc<Code>],
) -> CompileResult {
    if !(2..=3).contains(&list.len()) {
        return Err("illegal arguments If".to_string());
    }

    compiler.compile_value(store, false, commands, &list[0])?;

    let jump_next_cmd_pos = commands.len();
    commands.push(Command::JumpIfFalse(0));

    compiler.compile_value(store, false, commands, &list[1])?;

    let jump_end_cmd_poss = commands.len();
    commands.push(Command::Jump(0));

    let cur_pos = commands.len();
    commands[jump_next_cmd_pos] = Command::JumpIfFalse(cur_pos);

    if let Some(code) = list.get(2) {
        compiler.compile_value(store, false, commands, code)?;
    } else {
        commands.push(Command::PushCode(compiler.constants.code_nil()));
    }

    let cur_pos = commands.len();
    commands[jump_end_cmd_poss] = Command::Jump(cur_pos);

    Ok(())
}
