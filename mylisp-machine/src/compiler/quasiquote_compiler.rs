use std::rc::Rc;

use crate::code::Code;
use crate::special_form::SpecialForm;

use super::Command;
use super::CompileResult;
use super::Compiler;
use super::ProgramStore;

enum QuasiquoteState {
    NoEvaluate,
    Evaluated,
    Unquote,
    UnquoteSplicing,
}

pub(super) fn compile(
    compiler: &mut Compiler,
    store: &mut ProgramStore,
    commands: &mut Vec<Command>,
    list: &[Rc<Code>],
) -> CompileResult {
    if list.len() != 1 {
        return Err("illegal argument count Quasiquote".to_string());
    }

    let res = resolve_quasiquotee(compiler, 0, store, commands, &list[0])?;

    match res {
        QuasiquoteState::NoEvaluate => {
            commands.push(Command::PushCode(list[0].clone()));
        }
        QuasiquoteState::Evaluated => {}
        QuasiquoteState::Unquote => {}
        QuasiquoteState::UnquoteSplicing => {
            return Err("invalid UnquoteSplicing".to_string());
        }
    }

    Ok(())
}

fn resolve_quasiquotee(
    compiler: &mut Compiler,
    nest: usize,
    store: &mut ProgramStore,
    commands: &mut Vec<Command>,
    code: &Rc<Code>,
) -> Result<QuasiquoteState, String> {
    match &**code {
        Code::Cons(ref cell) => {
            let nest = match cell.car.get_symbol() {
                Some(sym) if *sym == SpecialForm::Quasiquote.symbol() => nest + 1,
                Some(sym) if *sym == SpecialForm::Unquote.symbol() => {
                    if nest == 0 {
                        if let Some((cadr, _)) = cell.cdr.split().filter(|(_, rest)| rest.is_nil())
                        {
                            compiler.compile_value(store, false, commands, cadr)?;
                            return Ok(QuasiquoteState::Unquote);
                        } else {
                            return Err("invlid argument Unquote".to_string());
                        }
                    }
                    nest - 1
                }
                Some(sym) if *sym == SpecialForm::UnquoteSplicing.symbol() => {
                    if nest == 0 {
                        if let Some((cadr, _)) = cell.cdr.split().filter(|(_, rest)| rest.is_nil())
                        {
                            compiler.compile_value(store, false, commands, cadr)?;
                            return Ok(QuasiquoteState::UnquoteSplicing);
                        } else {
                            return Err("invlid argument UnquoteSplicing".to_string());
                        }
                    }
                    nest - 1
                }
                _ => nest,
            };
            let res_car = resolve_quasiquotee(compiler, nest, store, commands, &cell.car)?;
            let res_cdr = resolve_quasiquotee(compiler, nest, store, commands, &cell.cdr)?;
            match (res_car, res_cdr) {
                (_, QuasiquoteState::UnquoteSplicing) => {
                    // cdr に UnquoteSplicing は不正
                    // (? . ,@A)
                    Err("invalid UnquoteSplicing".to_string())
                }
                (QuasiquoteState::NoEvaluate, QuasiquoteState::NoEvaluate) => {
                    // car も cdr も 入力まま
                    // (A . B) ((A) B C D E)
                    Ok(QuasiquoteState::NoEvaluate)
                }
                (QuasiquoteState::NoEvaluate, _) => {
                    // car は 入力まま
                    // cdr は 評価されてる
                    // (A . ,B) ((A) . (B ,C D ,@E F))
                    commands.push(Command::PushCode(cell.car.clone()));
                    commands.push(Command::SwapStackHead);
                    commands.push(Command::Cons);
                    Ok(QuasiquoteState::Evaluated)
                }
                (QuasiquoteState::UnquoteSplicing, QuasiquoteState::NoEvaluate) => {
                    // car は 評価されてて 展開
                    // cdr は 入力まま
                    // (,@A . B) (,@A B C D ...)
                    commands.push(Command::PushCode(cell.cdr.clone()));
                    commands.push(Command::AppendList);
                    Ok(QuasiquoteState::Evaluated)
                }
                (_, QuasiquoteState::NoEvaluate) => {
                    // car は 評価されてる
                    // cdr は 入力まま
                    // (,A . B) ((,A) B C D ...)
                    commands.push(Command::PushCode(cell.cdr.clone()));
                    commands.push(Command::Cons);
                    Ok(QuasiquoteState::Evaluated)
                }
                (QuasiquoteState::UnquoteSplicing, _) => {
                    // car は 評価されてて 展開
                    // cdr は 評価されてる
                    // (,@A . ,B) (,@A B ,C D ,@E ...)
                    commands.push(Command::AppendList);
                    Ok(QuasiquoteState::Evaluated)
                }
                _ => {
                    // car も cdr も 評価されてる
                    // (,A . ,B) ((,A X) . (B ,C D ,@E F))
                    commands.push(Command::Cons);
                    Ok(QuasiquoteState::Evaluated)
                }
            }
        }
        _ => Ok(QuasiquoteState::NoEvaluate),
    }
}
