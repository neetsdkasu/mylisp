use std::rc::Rc;

use crate::code::Code;

use super::ArgCountValidator;
use super::Command;
use super::CompileResult;
use super::Compiler;
use super::ProgramStore;

impl<'a> Compiler<'a> {
    pub(super) fn compile_list(
        &mut self,
        store: &mut ProgramStore,
        top_level: bool,
        commands: &mut Vec<Command>,
        list: &[Rc<Code>],
    ) -> CompileResult {
        let mut iter = list.iter();
        let mut call: Option<Command> = None;

        // Callable
        let first = iter.next().unwrap();
        if let Some(list) = first.to_vec().filter(|list| !list.is_empty()) {
            self.compile_list(store, top_level, commands, &list)?;
            call = Some(Command::CallUnknownFunc);
        } else if let Some(sym) = first.get_symbol() {
            if let Some(f) = self.constants.builtin_funcs.get(&*sym) {
                if !f.validate_arg_count(list.len() - 1) {
                    return Err(format!("wrong arg count {:?} {}", f, list.len() - 1));
                }
                commands.push(Command::PushBuiltinFunc(*f));
                call = Some(Command::CallBuiltinFunc);
            }
            if let Some(f) = self.constants.special_forms.get(&*sym) {
                return self.compile_special_form(store, top_level, commands, &list[1..], *f);
            }
            if let Some(f) = self.constants.io_funcs.get(&*sym) {
                if !f.validate_arg_count(list.len() - 1) {
                    return Err(format!("wrong arg count {:?}, {}", f, list.len() - 1));
                }
                commands.push(Command::PushIOFunc(*f));
                call = Some(Command::CallIOFunc);
            }
            if let Some(cmd) = self.scope_bindings.get(&*sym) {
                commands.push(cmd.clone());
                call = Some(Command::CallUnknownFunc);
            }
            if let Some(cmd) = self.request_bindings.get(&*sym) {
                commands.push(cmd.clone());
                call = Some(Command::CallUnknownFunc);
            }
            if call.is_none() {
                let pos = self.request_bindings.len();
                let cmd = Command::PushBindCopy(pos);
                self.request_bindings.insert(sym, cmd.clone());
                commands.push(cmd);
                call = Some(Command::CallUnknownFunc);
            }
        } else {
            // マクロで関数やラムダの実体がおかれるかもしれないのでそれ除外しないと…
            match &**first {
                Code::BuiltinFunc(f) => {
                    if !f.validate_arg_count(list.len() - 1) {
                        return Err(format!("wrong arg count {:?} {}", f, list.len() - 1));
                    }
                    commands.push(Command::PushBuiltinFunc(*f));
                    call = Some(Command::CallBuiltinFunc);
                }
                Code::IOFunc(f) => {
                    if !f.validate_arg_count(list.len() - 1) {
                        return Err(format!("wrong arg count {:?} {}", f, list.len() - 1));
                    }
                    commands.push(Command::PushIOFunc(*f));
                    call = Some(Command::CallIOFunc);
                }
                Code::Lambda(ref info) => {
                    if !info.validate_arg_count(list.len() - 1) {
                        return Err(format!("wrong arg count {:?} {}", info, list.len() - 1));
                    }
                    commands.push(Command::PushCode(first.clone()));
                    call = Some(Command::CallUnknownFunc);
                }
                _ => return Err(format!("not callable [ {} ]", first)),
            }
        }

        // Arguments
        for elem in iter {
            self.compile_value(store, false, commands, elem)?;
        }

        if let Some(last) = call.take() {
            commands.push(Command::PushArgCount(list.len() as isize));
            commands.push(last);
        }

        Ok(())
    }
}
