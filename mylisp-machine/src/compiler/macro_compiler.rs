use std::rc::Rc;

use crate::code::{Code, LambdaInfo};

use super::Bindings;
use super::Closure;
use super::Command;
use super::CompileResult;
use super::Compiler;
use super::ProgramStore;
use super::MAX_ARG_SIZE;

pub(super) fn compile(
    compiler: &mut Compiler,
    store: &mut ProgramStore,
    top_level: bool,
    commands: &mut Vec<Command>,
    list: &[Rc<Code>],
) -> CompileResult {
    if !top_level {
        return Err("not top-level Macro".to_string());
    }

    if list.len() != 3 {
        return Err("illegal argument count Macro".to_string());
    }

    let macro_name = list[0]
        .get_symbol()
        .ok_or_else(|| "illegal argument SYMBOL Macor".to_string())?;

    if !compiler.validate_name(&*macro_name) {
        return Err("invalid argument macro name".to_string());
    }

    let mut macro_commands: Vec<Command> = Vec::new();
    let mut macro_arg_bindings = Bindings::new();

    let arg_min_size: usize;
    let arg_max_size: usize;

    if list[1].is_nil() {
        arg_min_size = 0;
        arg_max_size = 0;
    } else if let Some(sym) = list[1].get_symbol() {
        if !compiler.validate_name(&*sym) {
            return Err("invalid argument name".to_string());
        }
        macro_commands.push(Command::MakeListFromAllArgs);
        macro_commands.push(Command::PushStackCopy(-1));
        macro_commands.push(Command::PushArgCount(2));
        macro_commands.push(Command::SumArgCount);
        macro_arg_bindings.insert(sym, Command::PushStackCopy(0));
        arg_min_size = 0;
        arg_max_size = MAX_ARG_SIZE;
    } else if let Some(arg_list) = list[1].to_vec() {
        arg_min_size = arg_list.len();
        arg_max_size = arg_min_size;
        let arg_pos: isize = -1 - arg_list.len() as isize;
        let mut spos: isize = 0;
        for (i, c) in arg_list.iter().enumerate() {
            let pos: isize = arg_pos + i as isize;
            if let Some(sym) = c.get_symbol() {
                if !compiler.validate_name(&*sym) {
                    return Err("invalid argument name".to_string());
                }
                macro_arg_bindings.insert(sym, Command::PushStackCopy(pos));
            } else if let Code::Cons(cell) = &**c {
                let mut temp = Vec::new();
                temp.push((vec![Command::Cdr], &cell.cdr));
                temp.push((vec![Command::Car], &cell.car));
                while let Some((mut cmds, code)) = temp.pop() {
                    if let Some(sym) = code.get_symbol() {
                        if !compiler.validate_name(&*sym) {
                            return Err("invalid argument name".to_string());
                        }
                        macro_commands.push(Command::PushStackCopy(pos));
                        macro_commands.append(&mut cmds);
                        macro_arg_bindings.insert(sym, Command::PushStackCopy(spos));
                        spos += 1;
                    } else if let Code::Cons(cell) = &**code {
                        let mut cmds_cdr = cmds.clone();
                        cmds_cdr.push(Command::Cdr);
                        temp.push((cmds_cdr, &cell.cdr));
                        cmds.push(Command::Car);
                        temp.push((cmds, &cell.car));
                    } else if !code.is_nil() {
                        return Err("invalid argument".to_string());
                    }
                }
            } else {
                return Err("invalid argument name".to_string());
            }
        }
        if spos > 0 {
            macro_commands.push(Command::PushStackCopy(-1));
            macro_commands.push(Command::PushArgCount(spos + 1));
            macro_commands.push(Command::SumArgCount);
        }
    } else if let Code::Cons(ref cell) = &*list[1] {
        let mut temp_arg_min_size: isize = 1;
        macro_commands.push(Command::MakeListFromAllArgs);
        let mut spos: isize = 1;
        let mut temp = vec![
            (vec![Command::Cdr], &cell.cdr, 1),
            (vec![Command::Car], &cell.car, -1),
        ];
        while let Some((mut cmds, code, len)) = temp.pop() {
            temp_arg_min_size = temp_arg_min_size.max(len);
            if let Some(sym) = code.get_symbol() {
                if !compiler.validate_name(&*sym) {
                    return Err("invalid argument name".to_string());
                }
                macro_commands.push(Command::PushStackCopy(0));
                macro_commands.append(&mut cmds);
                macro_arg_bindings.insert(sym, Command::PushStackCopy(spos));
                spos += 1;
            } else if let Code::Cons(ref cell) = &**code {
                let mut cmds_cdr = cmds.clone();
                cmds_cdr.push(Command::Cdr);
                temp.push((cmds_cdr, &cell.cdr, len + len.signum()));
                cmds.push(Command::Car);
                temp.push((cmds, &cell.car, -1));
            } else if !code.is_nil() {
                return Err("invalid argument name".to_string());
            }
        }
        if spos > 0 {
            macro_commands.push(Command::PushStackCopy(-1));
            macro_commands.push(Command::PushArgCount(spos + 1));
            macro_commands.push(Command::SumArgCount);
        }
        arg_min_size = temp_arg_min_size as usize;
        arg_max_size = MAX_ARG_SIZE;
    } else {
        return Err("invalid argument list".to_string());
    }

    let mut macro_compiler = Compiler::with_scope_bindings(compiler.constants, macro_arg_bindings);

    macro_compiler.compile_value(store, false, &mut macro_commands, &list[2])?;

    let macro_request_bindings = macro_compiler.request_bindings;

    macro_commands.push(Command::RetWithCleanStack);

    let mut temp_bind_map: Vec<(usize, Rc<String>)> = Vec::new();

    for (k, v) in macro_request_bindings.iter() {
        if let Command::PushBindCopy(pos) = v {
            temp_bind_map.push((*pos, k.clone()));
        } else {
            return Err(bug!());
        }
    }

    temp_bind_map.sort();

    let mut bind_map: Vec<Rc<String>> = Vec::new();

    for (i, (pos, key)) in temp_bind_map.into_iter().enumerate() {
        if i != pos {
            return Err(bug!());
        }
        if let Some(cmd) = compiler.scope_bindings.get(&*key) {
            commands.push(cmd.clone());
        } else if let Some(cmd) = compiler.request_bindings.get(&*key) {
            commands.push(cmd.clone());
        } else {
            let id = compiler.request_bindings.len();
            let cmd = Command::PushBindCopy(id);
            compiler.request_bindings.insert(key.clone(), cmd.clone());
            commands.push(cmd);
        }
        bind_map.push(key);
    }

    commands.push(Command::PushArgCount(bind_map.len() as isize));

    let lambda_id = store.closures.len();

    let closure = Closure {
        bind_map,
        commands: Rc::new(macro_commands),
        info: LambdaInfo {
            id: lambda_id as u32,
            arg_min_size,
            arg_max_size,
            args: list[1].clone(),
            body: list[2].clone(),
        },
    };

    store.closures.push(Rc::new(closure));

    commands.push(Command::PushLambdaId(lambda_id));
    commands.push(Command::MakeMacro(macro_name));

    Ok(())
}
