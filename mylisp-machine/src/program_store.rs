use std::collections::HashMap;
use std::fmt;
use std::rc::Rc;

use crate::code::LambdaInfo;

use super::command::Command;
use super::value::Value;

pub struct Closure {
    pub(super) bind_map: Vec<Rc<String>>,
    pub(super) commands: Rc<Vec<Command>>,
    pub(super) info: LambdaInfo,
}

#[derive(Debug)]
pub struct BindedClosure {
    pub(super) id: usize,
    pub(super) bindings: Rc<Vec<Rc<Value>>>,
}

pub struct ProgramStore {
    pub(super) closures: Vec<Rc<Closure>>,
    pub(super) global_bindings: Vec<Rc<Value>>,
    pub(super) binded_closures: Vec<Rc<BindedClosure>>,
    pub(super) macros: HashMap<Rc<String>, Rc<BindedClosure>>,
}

impl ProgramStore {
    pub fn new() -> ProgramStore {
        ProgramStore {
            closures: Vec::new(),
            global_bindings: Vec::new(),
            binded_closures: Vec::new(),
            macros: HashMap::new(),
        }
    }
}

impl Default for ProgramStore {
    fn default() -> Self {
        Self::new()
    }
}

impl fmt::Debug for Closure {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Closure{{ LambdaInfo: {:?}, BindMap: {:?}, Commands: {:?} }}",
            self.info, self.bind_map, self.commands
        )
    }
}
