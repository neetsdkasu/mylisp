use std::rc::Rc;

use crate::builtin_func::BuiltinFunc;
use crate::code::Code;
use crate::io_func::IOFunc;
use crate::special_form::SpecialForm;

#[derive(Debug, Clone)]
pub enum Command {
    AppendList,
    BindDelay,
    BindGlobal(usize),
    CallBuiltinFunc,
    CallIOFunc,
    CallUnknownFunc,
    Car,
    Cdr,
    CheckNext,
    Cons,
    DelayGet,
    Dup,
    Jump(usize),
    JumpIfFalse(usize),
    JumpIfNil(usize),
    MakeClosure,
    MakeDelay,
    MakeListFromAllArgs,
    MakeMacro(Rc<String>),
    MakeNext,
    PushArgCount(isize),
    PushBindCopy(usize),
    PushBuiltinFunc(BuiltinFunc),
    PushCode(Rc<Code>),
    PushGlobalVar(usize),
    PushIOFunc(IOFunc),
    PushLambdaId(usize),
    PushSpecialForm(SpecialForm),
    PushStackCopy(isize),
    Ret,
    RetWithCleanStack,
    RotateForward(usize),
    SumArgCount,
    SwapStackHead,
}
