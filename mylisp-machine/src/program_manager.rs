use std::io;
use std::rc::Rc;

use crate::code::{Case, Code, Syntax};
use crate::engine::IOSet;

use super::command::Command;
use super::compiler::{self, Compiler};
use super::constants::Constants;
use super::machine_impl::{Machine, MacroExtender};
use super::program_store::ProgramStore;

type Error = String;

pub struct Program<'a, R, W, E> {
    manager: &'a mut ProgramManager<R, W, E>,
    commands: Vec<Command>,
}

pub struct ProgramManager<R, W, E> {
    constants: Constants,
    store: ProgramStore,
    global_scope_bindings: Option<compiler::Bindings>,
    io_set: IOSet<R, W, E>,
}

impl<R, W, E> ProgramManager<R, W, E> {
    #[allow(dead_code)]
    pub fn store(&self) -> &ProgramStore {
        &self.store
    }

    pub fn io_set(&mut self) -> &mut IOSet<R, W, E> {
        &mut self.io_set
    }

    pub fn reset(&mut self) {
        self.global_scope_bindings = Some(compiler::Bindings::new());
        self.store = ProgramStore::new();
    }

    pub fn get_case(&self) -> Case {
        self.io_set.case
    }

    pub fn set_case(&mut self, new_case: Case) {
        self.constants.case = new_case;
        self.io_set.case = new_case;
    }

    pub fn get_syntax(&self) -> Syntax {
        self.io_set.syntax
    }

    pub fn set_syntax(&mut self, new_syntax: Syntax) {
        self.constants.syntax = new_syntax;
        self.io_set.syntax = new_syntax;
    }
}

impl<R, W, E> ProgramManager<R, W, E>
where
    R: io::BufRead,
    W: io::Write,
    E: io::Write,
{
    pub fn new(io_set: IOSet<R, W, E>) -> Self {
        let constants = Constants::new_with(io_set.case, io_set.syntax);
        ProgramManager {
            constants,
            store: ProgramStore::new(),
            global_scope_bindings: Some(compiler::Bindings::new()),
            io_set,
        }
    }

    fn apply_macro(&mut self, code: Rc<Code>) -> Result<Rc<Code>, Error> {
        let extender = MacroExtender::new(&self.constants);
        extender.apply_macro(&mut self.store, code, &mut self.io_set)
    }

    pub fn compile(&mut self, code: Rc<Code>) -> Result<Program<'_, R, W, E>, Error> {
        let code = self.apply_macro(code)?;
        let global_scope_bindings = self.global_scope_bindings.take().ok_or_else(|| bug!())?;
        let mut compiler = Compiler::with_scope_bindings(&self.constants, global_scope_bindings);
        let res = compiler.compile(&mut self.store, &code);
        self.global_scope_bindings = Some(compiler.release_scope_bindings());
        res.map(move |commands| Program::new(self, commands))
    }
}

impl<'a, R, W, E> Program<'a, R, W, E> {
    fn new(manager: &'a mut ProgramManager<R, W, E>, commands: Vec<Command>) -> Self {
        Program { manager, commands }
    }

    #[allow(dead_code)]
    pub fn commands(&self) -> &[Command] {
        &self.commands
    }

    #[allow(dead_code)]
    pub fn manager(&self) -> &ProgramManager<R, W, E> {
        self.manager
    }
}

impl<'a, R, W, E> Program<'a, R, W, E>
where
    R: io::BufRead,
    W: io::Write,
    E: io::Write,
{
    pub fn run(self) -> Result<Rc<Code>, Error> {
        let Program { manager, commands } = self;
        let mut machine = Machine::new(&manager.constants);
        machine.run(&mut manager.store, commands, &mut manager.io_set)
    }
}
