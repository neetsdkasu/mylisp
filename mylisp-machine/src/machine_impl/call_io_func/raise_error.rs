use std::rc::Rc;

use crate::code::Code;
use crate::io_func::IOFunc::RaiseError;

use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    let mut code = constants.code_nil();
    for car in args.iter().rev() {
        code = Code::cons(Rc::clone(car), code);
    }
    let display = constants.get_display(&code);
    Err(format!(
        "[ {} ] {}",
        &constants.case.apply(RaiseError.symbol()),
        display
    ))
}
