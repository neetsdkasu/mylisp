use std::io;

use crate::code::Code;
use crate::engine::IOSet;
use crate::io_func::IOFunc::Println;

use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func<R, W, E>(
    io_set: &mut IOSet<R, W, E>,
    constants: &Constants,
    args: ArgList,
) -> Result
where
    W: io::Write,
{
    macro_rules! io_error {
        ($err:expr) => {
            format!("[ {} ] {}", &constants.case.apply(Println.symbol()), $err)
        };
    }

    for (i, code) in args.iter().enumerate() {
        if i > 0 {
            write!(&mut io_set.output, " ").or_else(|err| Err(io_error!(err)))?;
        }
        match &***code {
            Code::Str(ref s) => {
                write!(&mut io_set.output, "{}", s).or_else(|err| Err(io_error!(err)))?;
            }
            _ => {
                let display = constants.get_display(code);
                write!(&mut io_set.output, "{}", display).or_else(|err| Err(io_error!(err)))?;
            }
        }
    }

    writeln!(&mut io_set.output).or_else(|err| Err(io_error!(err)))?;

    Ok(constants.code_nil())
}
