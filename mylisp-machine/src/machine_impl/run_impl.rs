use std::io;
use std::rc::Rc;

use crate::code::{self, Code, LambdaInfo};
use crate::engine::IOSet;

use super::BindedClosure;
use super::Command;
use super::Machine;
use super::ProgramStore;
use super::RunResult;
use super::Value;

pub(super) struct CallStackState {
    pub(super) commands: Rc<Vec<Command>>,
    pub(super) bindings: Rc<Vec<Rc<Value>>>,
    pub(super) base_spos: usize,
    pub(super) pos: usize,
}

impl CallStackState {
    fn inc_pos(&mut self) {
        self.pos += 1;
    }

    fn dec_pos(&mut self) {
        self.pos -= 1;
    }

    fn get_command(&self) -> Option<Command> {
        self.commands.get(self.pos).cloned()
    }

    fn jump(&mut self, new_pos: usize) {
        self.pos = new_pos;
    }

    fn get_spos(&self, rpos: isize) -> usize {
        (self.base_spos as isize + rpos) as usize
    }
}

impl<'a> Machine<'a> {
    pub(super) fn run_impl<R, W, E>(
        &mut self,
        store: &mut ProgramStore,
        mut state: CallStackState,
        io_set: &mut IOSet<R, W, E>,
    ) -> RunResult
    where
        R: io::BufRead,
        W: io::Write,
        E: io::Write,
    {
        use Command::*;
        let mut state_stack: Vec<CallStackState> = Vec::new();

        while let Some(cmd) = state.get_command() {
            // eprintln!("[{}] STACK: {:?}", pos, self.call_stack);
            match &cmd {
                AppendList => {
                    let cdr_value = unwrap!(self.call_stack.pop());
                    let mut code = unwrap!(cdr_value.get_code()).clone();
                    let car_value = unwrap!(self.call_stack.pop());
                    let car = unwrap!(car_value.get_code());
                    let mut list = car
                        .to_vec()
                        .ok_or_else(|| "invalid UnquoteSplicing Arg".to_string())?;
                    while let Some(car) = list.pop() {
                        code = Code::cons(car.clone(), code);
                    }
                    self.call_stack.push(Value::Code(code).rc());
                }
                BindDelay => {
                    let ret_value = unwrap!(self.call_stack.pop());
                    let ret_code = unwrap!(ret_value.get_code());
                    let delay_value = unwrap!(self.call_stack.pop());
                    let delay_code = unwrap!(delay_value.get_code());
                    if let Code::Delayed(ref dv) = &**delay_code {
                        if !matches!(&*dv.borrow(), code::DelayedValue::Evaluating) {
                            return Err(bug!());
                        }
                        *dv.borrow_mut() = code::DelayedValue::Evaluated(ret_code.clone());
                        self.call_stack.push(ret_value);
                    } else {
                        return Err(bug!());
                    }
                }
                BindGlobal(id) => {
                    use std::cmp::Ordering;
                    let value = unwrap!(self.call_stack.pop());
                    match store.global_bindings.len().cmp(id) {
                        Ordering::Less => return Err(bug!()),
                        Ordering::Equal => store.global_bindings.push(value),
                        Ordering::Greater => store.global_bindings[*id] = value,
                    }
                    self.call_stack
                        .push(Rc::new(Value::Code(self.constants.code_nil())));
                }
                CallBuiltinFunc => self.call_builtin_func()?,
                CallIOFunc => self.call_io_func(io_set)?,
                CallUnknownFunc => {
                    if let Some(new_state) = self.call_unknown_func(store, io_set)? {
                        state_stack.push(state);
                        state = new_state;
                        continue;
                    }
                }
                Car => {
                    let value = unwrap!(self.call_stack.pop());
                    let code = unwrap!(value.get_code());
                    if let Code::Cons(cell) = &**code {
                        let car = cell.car.clone();
                        self.call_stack.push(Value::Code(car).rc());
                    } else {
                        return Err("failed car".to_string());
                    }
                }
                Cdr => {
                    let value = unwrap!(self.call_stack.pop());
                    let code = unwrap!(value.get_code());
                    if let Code::Cons(cell) = &**code {
                        let cdr = cell.cdr.clone();
                        self.call_stack.push(Value::Code(cdr).rc());
                    } else {
                        return Err("failed cdr".to_string());
                    }
                }
                CheckNext => {
                    let ret_value = unwrap!(self.call_stack.pop());
                    let ret_code = unwrap!(ret_value.get_code());
                    let sym_pos = self.call_stack.len() - 2;
                    let symbol = unwrap!(self
                        .call_stack
                        .get(sym_pos)
                        .and_then(|value| value.get_code())
                        .and_then(|code| code.get_symbol()));
                    match &**ret_code {
                        Code::Next(ref info) if *info.symbol == *symbol => {
                            let cl = unwrap!(self.call_stack.last()).clone();
                            self.call_stack.push(cl);
                            let args = unwrap!(info.args.to_vec());
                            for code in args.iter() {
                                self.call_stack.push(Value::Code(code.clone()).rc());
                            }
                            self.call_stack
                                .push(Value::ArgCount(args.len() as isize + 1).rc());
                            state.dec_pos();
                            continue;
                        }
                        _ => {
                            unwrap!(self.call_stack.pop());
                            unwrap!(self.call_stack.pop());
                            self.call_stack.push(ret_value);
                        }
                    }
                }
                Cons => {
                    let cdr_value = unwrap!(self.call_stack.pop());
                    let cdr = unwrap!(cdr_value.get_code());
                    let car_value = unwrap!(self.call_stack.pop());
                    let car = unwrap!(car_value.get_code());
                    let code = Code::cons(car.clone(), cdr.clone());
                    self.call_stack.push(Value::Code(code).rc());
                }
                DelayGet => {
                    let value = unwrap!(self.call_stack.pop());
                    let code = unwrap!(value.get_code());
                    if let Code::Delayed(ref dv) = &**code {
                        let (save, ret) = match &*dv.borrow() {
                            code::DelayedValue::UnEvaluated(ref ret, _) => (true, ret.clone()),
                            code::DelayedValue::Evaluating => {
                                return Err("delayed value cycled reference".to_string());
                            }
                            code::DelayedValue::Evaluated(ref ret) => (false, ret.clone()),
                        };
                        if save {
                            *dv.borrow_mut() = code::DelayedValue::Evaluating;
                            self.call_stack.push(Value::Code(code.clone()).rc());
                            let bid = match &*ret {
                                Code::Lambda(ref info) => info.id as usize,
                                _ => return Err(bug!()),
                            };
                            let binded_cl = &store.binded_closures[bid];
                            let id = binded_cl.id;
                            let cl = store.closures[id].clone();
                            let bindings = binded_cl.bindings.clone();
                            let base_spos = self.call_stack.len();
                            let new_state = CallStackState {
                                commands: cl.commands.clone(),
                                bindings,
                                base_spos,
                                pos: 0,
                            };
                            state_stack.push(state);
                            state = new_state;
                            continue;
                        } else {
                            self.call_stack.push(Value::Code(ret).rc());
                        }
                    } else {
                        return Err("invalid arg DelayGet".to_string());
                    }
                }
                Dup => {
                    let value = unwrap!(self.call_stack.last()).clone();
                    self.call_stack.push(value);
                }
                Jump(jump_pos) => {
                    state.jump(*jump_pos);
                    continue;
                }
                JumpIfFalse(jump_pos) => {
                    let value = unwrap!(self.call_stack.pop());
                    if let Value::Code(code) = &*value {
                        if matches!(&**code, Code::False) {
                            state.jump(*jump_pos);
                            continue;
                        }
                    }
                }
                JumpIfNil(jump_pos) => {
                    let value = unwrap!(self.call_stack.pop());
                    if let Value::Code(code) = &*value {
                        if code.is_nil() {
                            state.jump(*jump_pos);
                            continue;
                        }
                    }
                }
                MakeClosure => {
                    let lambda_id = unwrap!(self.call_stack.pop());
                    let id = match &*lambda_id {
                        Value::LambdaId(id) => *id,
                        _ => return Err(bug!()),
                    };
                    let ac = unwrap!(self.call_stack.pop());
                    let count = match &*ac {
                        Value::ArgCount(count) => *count as usize,
                        _ => return Err(bug!()),
                    };
                    let mut lambda_bindings: Vec<Rc<Value>> = Vec::new();
                    for _ in 0..count {
                        let value = unwrap!(self.call_stack.pop());
                        lambda_bindings.push(value);
                    }
                    lambda_bindings.reverse();
                    let cl = BindedClosure {
                        id,
                        bindings: Rc::new(lambda_bindings),
                    };
                    let cl_id = store.binded_closures.len();
                    store.binded_closures.push(Rc::new(cl));
                    let base_info = &store.closures[id].info;
                    let lambda_info = LambdaInfo {
                        id: cl_id as u32,
                        arg_min_size: base_info.arg_min_size,
                        arg_max_size: base_info.arg_max_size,
                        args: base_info.args.clone(),
                        body: base_info.body.clone(),
                    };
                    let code = Code::Lambda(Rc::new(lambda_info)).rc();
                    self.call_stack.push(Value::Code(code).rc());
                }
                MakeDelay => {
                    let value = unwrap!(self.call_stack.pop());
                    let code = unwrap!(value.get_code());
                    let code = Code::make_delayed(code.clone(), code::Bindings::with_capacity(0));
                    self.call_stack.push(Value::Code(code).rc());
                }
                MakeListFromAllArgs => {
                    let ac = unwrap!(self.call_stack.last());
                    if let Value::ArgCount(count) = &**ac {
                        let mut list = self.constants.code_nil();
                        let mut spos = self.call_stack.len() - 1;
                        for _ in 1..*count {
                            spos -= 1;
                            if let Value::Code(code) = &*self.call_stack[spos] {
                                list = Code::cons(code.clone(), list);
                            } else {
                                return Err(bug!("{:?}", self.call_stack));
                            }
                        }
                        self.call_stack.push(Value::Code(list).rc());
                    } else {
                        return Err(bug!());
                    }
                }
                MakeMacro(ref macro_name) => {
                    let lambda_id = unwrap!(self.call_stack.pop());
                    let id = match &*lambda_id {
                        Value::LambdaId(id) => *id,
                        _ => return Err(bug!()),
                    };
                    let ac = unwrap!(self.call_stack.pop());
                    let count = match &*ac {
                        Value::ArgCount(count) => *count as usize,
                        _ => return Err(bug!()),
                    };
                    let mut lambda_bindings: Vec<Rc<Value>> = Vec::new();
                    for _ in 0..count {
                        let value = unwrap!(self.call_stack.pop());
                        lambda_bindings.push(value);
                    }
                    lambda_bindings.reverse();
                    let cl = Rc::new(BindedClosure {
                        id,
                        bindings: Rc::new(lambda_bindings),
                    });
                    let cl_id = store.binded_closures.len();
                    store.macros.insert(macro_name.clone(), cl.clone());
                    store.binded_closures.push(cl);
                    let base_info = &store.closures[id].info;
                    let lambda_info = LambdaInfo {
                        id: cl_id as u32,
                        arg_min_size: base_info.arg_min_size,
                        arg_max_size: base_info.arg_max_size,
                        args: base_info.args.clone(),
                        body: base_info.body.clone(),
                    };
                    let code = Code::Macro(Rc::new(lambda_info)).rc();
                    self.call_stack.push(Value::Code(code).rc());
                }
                MakeNext => {
                    let value = unwrap!(self.call_stack.pop());
                    let count = unwrap!(value.arg_count());
                    let mut args = self.constants.code_nil();
                    for _ in 1..count {
                        let code_value = unwrap!(self.call_stack.pop());
                        let code = unwrap!(code_value.get_code());
                        args = Code::cons(code.clone(), args);
                    }
                    let sym_value = unwrap!(self.call_stack.pop());
                    let sym_code = unwrap!(sym_value.get_code());
                    if let Some(symbol) = sym_code.get_symbol() {
                        let ret = Code::Next(Box::new(code::NextInfo { symbol, args })).rc();
                        self.call_stack.push(Value::Code(ret).rc());
                    } else {
                        return Err("illegal argument symbol Next".to_string());
                    }
                }
                PushArgCount(count) => {
                    self.call_stack.push(Value::ArgCount(*count).rc());
                }
                PushBindCopy(id) => {
                    if let Some(value) = state.bindings.get(*id) {
                        self.call_stack.push(value.clone());
                    } else {
                        return Err(bug!());
                    }
                }
                PushBuiltinFunc(bf) => {
                    self.call_stack
                        .push(Value::Code(Code::BuiltinFunc(*bf).rc()).rc());
                }
                PushCode(code) => {
                    self.call_stack.push(Rc::new(Value::Code(code.clone())));
                }
                PushGlobalVar(id) => {
                    if let Some(value) = store.global_bindings.get(*id) {
                        self.call_stack.push(value.clone());
                    } else {
                        return Err(bug!());
                    }
                }
                PushIOFunc(iof) => {
                    self.call_stack
                        .push(Value::Code(Code::IOFunc(*iof).rc()).rc());
                }
                PushLambdaId(id) => {
                    self.call_stack.push(Value::LambdaId(*id).rc());
                }
                PushSpecialForm(sf) => {
                    self.call_stack
                        .push(Value::Code(Code::SpecialForm(*sf).rc()).rc());
                }
                PushStackCopy(rpos) => {
                    let spos = state.get_spos(*rpos);
                    let value = self.call_stack[spos].clone();
                    self.call_stack.push(value);
                }
                Ret => {
                    if let Some(new_state) = state_stack.pop() {
                        state = new_state;
                    } else {
                        return Ok(());
                    }
                }
                RetWithCleanStack => {
                    let value = unwrap!(self.call_stack.pop());
                    let ac = unwrap!(self.call_stack.pop());
                    if let Value::ArgCount(count) = &*ac {
                        for _ in 0..*count {
                            unwrap!(self.call_stack.pop());
                        }
                    }
                    self.call_stack.push(value);
                    if let Some(new_state) = state_stack.pop() {
                        state = new_state;
                    } else {
                        return Ok(());
                    }
                }
                RotateForward(len) => {
                    let pos = self.call_stack.len() - len;
                    self.call_stack[pos..].rotate_right(1);
                }
                SumArgCount => {
                    let ac1 = unwrap!(self.call_stack.pop());
                    let ac2 = unwrap!(self.call_stack.pop());
                    match (&*ac1, &*ac2) {
                        (Value::ArgCount(c1), Value::ArgCount(c2)) => {
                            let c = *c1 + *c2;
                            self.call_stack.push(Value::ArgCount(c).rc());
                        }
                        _ => return Err(bug!()),
                    }
                }
                SwapStackHead => {
                    let old_head = self.call_stack.len() - 1;
                    let new_head = old_head - 1;
                    self.call_stack.swap(old_head, new_head);
                }
            }
            state.inc_pos();
        }

        Err(bug!())
    }
}
