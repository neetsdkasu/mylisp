use std::io;
use std::rc::Rc;

use crate::code::Code;
use crate::engine::IOSet;
use crate::io_func::IOFunc;

use super::ArgList;
use super::Constants;
use super::Machine;
use super::Result;
use super::RunResult;
use super::Value;

mod eprintln;
mod println;
mod raise_error;
mod readln;

impl<'a> Machine<'a> {
    pub(super) fn call_io_func<R, W, E>(&mut self, io_set: &mut IOSet<R, W, E>) -> RunResult
    where
        R: io::BufRead,
        W: io::Write,
        E: io::Write,
    {
        let ac = unwrap!(self.call_stack.pop());
        let count = match &*ac {
            Value::ArgCount(count) => *count as usize,
            _ => return Err(bug!()),
        };
        let callable_pos = self.call_stack.len() - count;
        let list: Vec<_> = self.call_stack.drain(callable_pos..).collect();
        let iof = match &**unwrap!(list[0].get_code()) {
            Code::IOFunc(iof) => *iof,
            _ => return Err(bug!()),
        };
        let args: Vec<&Rc<Code>> = list[1..]
            .iter()
            .try_fold(Vec::new(), |mut acc, v| match &**v {
                Value::Code(code) => {
                    acc.push(code);
                    Some(acc)
                }
                _ => None,
            })
            .ok_or_else(|| bug!())?;
        use IOFunc::*;
        let ret_code = match iof {
            Println => println::func(io_set, self.constants, &args)?,
            Readln => readln::func(io_set, self.constants, &args)?,
            Eprintln => eprintln::func(io_set, self.constants, &args)?,
            RaiseError => raise_error::func(self.constants, &args)?,
        };
        self.call_stack.push(Value::Code(ret_code).rc());
        Ok(())
    }
}
