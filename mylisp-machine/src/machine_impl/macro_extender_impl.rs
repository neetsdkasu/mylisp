use std::io;
use std::rc::Rc;
use std::result;

use crate::code::Code;
use crate::engine::IOSet;

use super::CallStackState;
use super::Constants;
use super::Error;
use super::Machine;
use super::MacroExtender;
use super::ProgramStore;
use super::Value;

impl<'a> MacroExtender<'a> {
    pub fn new(constants: &'a Constants) -> MacroExtender {
        MacroExtender { constants }
    }

    pub fn apply_macro<R, W, E>(
        &self,
        store: &mut ProgramStore,
        code: Rc<Code>,
        io_set: &mut IOSet<R, W, E>,
    ) -> result::Result<Rc<Code>, Error>
    where
        R: io::BufRead,
        W: io::Write,
        E: io::Write,
    {
        match &*code {
            Code::Cons(ref cell) => {
                let car = self.apply_macro(store, cell.car.clone(), io_set)?;
                let cdr = self.apply_macro(store, cell.cdr.clone(), io_set)?;
                let symbol = match car.get_symbol() {
                    Some(symbol) => symbol,
                    None => return Ok(Code::cons(car, cdr)),
                };
                let bind_cl = match store.macros.get(&symbol) {
                    Some(bind_cl) => bind_cl.clone(),
                    None => return Ok(Code::cons(car, cdr)),
                };
                let cl = store.closures[bind_cl.id].clone();
                if !matches_args(&cl.info.args, &cdr) {
                    return Ok(Code::cons(car, cdr));
                }
                let args = cdr.to_vec().ok_or_else(|| bug!())?;
                let mut machine = Machine::new(self.constants);
                machine.call_stack.push(Value::ArgCount(0).rc()); // dummy value
                for code in args.iter() {
                    machine.call_stack.push(Value::Code(code.clone()).rc());
                }
                machine
                    .call_stack
                    .push(Value::ArgCount(args.len() as isize + 1).rc());
                let base_spos = machine.call_stack.len();
                let state = CallStackState {
                    commands: cl.commands.clone(),
                    bindings: bind_cl.bindings.clone(),
                    base_spos,
                    pos: 0,
                };
                machine.run_impl(store, state, io_set)?;
                if machine.call_stack.len() != 1 {
                    return Err(bug!());
                }
                if let Value::Code(code) = &*machine.call_stack[0] {
                    self.apply_macro(store, code.clone(), io_set)
                } else {
                    Err(bug!())
                }
            }
            _ => Ok(code),
        }
    }
}

fn matches_args(args_def: &Rc<Code>, args_val: &Rc<Code>) -> bool {
    let mut defs = vec![args_def];
    let mut vals = vec![args_val];

    while let Some(def_arg) = defs.pop() {
        let val_arg = match vals.pop() {
            Some(val_arg) => val_arg,
            None => return false,
        };
        match &**def_arg {
            Code::Nil => {
                if !val_arg.is_nil() {
                    return false;
                }
            }
            Code::Cons(ref cell_def) => {
                if let Code::Cons(ref cell_val) = &**val_arg {
                    defs.push(&cell_def.cdr);
                    vals.push(&cell_val.cdr);
                    defs.push(&cell_def.car);
                    vals.push(&cell_val.car);
                } else {
                    return false;
                }
            }
            Code::Symbol(_) => {}
            _ => return false,
        }
    }

    vals.is_empty()
}
