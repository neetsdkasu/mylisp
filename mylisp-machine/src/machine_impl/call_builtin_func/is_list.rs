use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    for arg in args.iter() {
        if !arg.is_list() {
            return Ok(constants.code_false());
        }
    }
    Ok(constants.code_true())
}
