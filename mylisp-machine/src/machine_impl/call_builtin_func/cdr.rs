use crate::builtin_func::BuiltinFunc::Cdr;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    match &**args[0] {
        Code::Nil => Ok(constants.code_nil()),
        Code::Cons(ref cell) => Ok(cell.cdr.clone()),
        _ => Err(illegal_argument!(
            "not cons cell",
            &constants.case.apply(Cdr.symbol()),
            0,
            constants.get_short_display(&*args[0])
        )),
    }
}
