use crate::builtin_func::BuiltinFunc::NumAdd;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    let mut sum: i32 = 0;
    for (i, arg) in args.iter().enumerate() {
        match &***arg {
            Code::Int32(v) => {
                if let Some(new_sum) = sum.checked_add(*v) {
                    sum = new_sum;
                } else {
                    return add_int64(constants, sum as i64, i, args);
                }
            }
            Code::Int64(_) => return add_int64(constants, sum as i64, i, args),
            Code::Float64(_) => return add_float64(constants, sum as f64, i, args),
            _ => {
                return Err(illegal_argument!(
                    "not number",
                    constants.case.apply(NumAdd.symbol()),
                    i,
                    constants.get_short_display(arg)
                ))
            }
        }
    }
    Ok(Code::Int32(sum).rc())
}

fn add_int64(constants: &Constants, mut sum: i64, start: usize, args: ArgList) -> Result {
    for (i, arg) in args.iter().enumerate().skip(start) {
        match &***arg {
            Code::Int32(v) => {
                if let Some(new_sum) = sum.checked_add(*v as i64) {
                    sum = new_sum;
                } else {
                    return add_float64(constants, sum as f64, i, args);
                }
            }
            Code::Int64(ref v) => {
                if let Some(new_sum) = sum.checked_add(**v) {
                    sum = new_sum;
                } else {
                    return add_float64(constants, sum as f64, i, args);
                }
            }
            Code::Float64(_) => return add_float64(constants, sum as f64, i, args),
            _ => {
                return Err(illegal_argument!(
                    "not number",
                    constants.case.apply(NumAdd.symbol()),
                    i,
                    constants.get_short_display(arg)
                ))
            }
        }
    }
    Ok(Code::Int64(Box::new(sum)).rc())
}

fn add_float64(constants: &Constants, mut sum: f64, start: usize, args: ArgList) -> Result {
    for (i, arg) in args.iter().enumerate().skip(start) {
        match &***arg {
            Code::Int32(v) => sum += *v as f64,
            Code::Int64(ref v) => sum += **v as f64,
            Code::Float64(ref v) => sum += **v,
            _ => {
                return Err(illegal_argument!(
                    "not number",
                    constants.case.apply(NumAdd.symbol()),
                    i,
                    constants.get_short_display(arg)
                ))
            }
        }
    }
    Ok(Code::Float64(Box::new(sum)).rc())
}
