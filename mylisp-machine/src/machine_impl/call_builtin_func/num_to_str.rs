use crate::builtin_func::BuiltinFunc::NumToStr;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    let s = match &**args[0] {
        Code::Int32(v) => v.to_string(),
        Code::Int64(ref v) => v.to_string(),
        Code::Float64(ref v) => v.to_string(),
        _ => {
            return Err(illegal_argument!(
                "not number",
                &constants.case.apply(NumToStr.symbol()),
                0,
                constants.get_short_display(&*args[0])
            ));
        }
    };
    Ok(Code::Str(Box::new(s)).rc())
}
