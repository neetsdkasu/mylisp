use crate::builtin_func::BuiltinFunc::And;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    let mut res = true;
    for (i, arg) in args.iter().enumerate() {
        match &***arg {
            Code::True => {}
            Code::False => res = false,
            _ => {
                return Err(illegal_argument!(
                    "not boolean",
                    constants.case.apply(And.symbol()),
                    i,
                    constants.get_short_display(arg)
                ));
            }
        }
    }
    Ok(constants.code_bool(res))
}
