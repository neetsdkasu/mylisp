use crate::builtin_func::BuiltinFunc::StrSplit;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    let s = match &**args[0] {
        Code::Str(ref s) => s,
        _ => {
            return Err(illegal_argument!(
                "arg TARGET not string",
                &constants.case.apply(StrSplit.symbol()),
                0,
                constants.get_short_display(&*args[0])
            ));
        }
    };
    let mut ss: Vec<_> = if let Some(code) = args.get(1) {
        if let Code::Str(ref sep) = &***code {
            s.split(&**sep).collect()
        } else {
            return Err(illegal_argument!(
                "arg SEPARATOR not string",
                &constants.case.apply(StrSplit.symbol()),
                1,
                constants.get_short_display(&*args[1])
            ));
        }
    } else {
        s.split_whitespace().collect()
    };
    let mut ret = constants.code_nil();
    while let Some(s) = ss.pop() {
        let code = Code::Str(Box::new(s.to_owned())).rc();
        ret = Code::cons(code, ret);
    }
    Ok(ret)
}
