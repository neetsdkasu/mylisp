use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    Ok(match &**args[0] {
        Code::Nil => constants.code_false(),
        Code::False => constants.code_false(),
        Code::Str(ref s) => constants.code_bool(!s.is_empty()),
        Code::Int32(v) => constants.code_bool(*v != 0),
        Code::Int64(ref v) => constants.code_bool(**v != 0),
        Code::Float64(ref v) => constants.code_bool(**v != 0.0),
        _ => constants.code_true(),
    })
}
