use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(constatns: &Constants, args: ArgList) -> Result {
    if args[0].is_atom() {
        Ok(constatns.code_t())
    } else {
        Ok(constatns.code_nil())
    }
}
