use crate::builtin_func::BuiltinFunc::Not;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    match &**args[0] {
        Code::True => Ok(constants.code_false()),
        Code::False => Ok(constants.code_true()),
        _ => Err(illegal_argument!(
            "not boolean",
            &constants.case.apply(Not.symbol()),
            0,
            constants.get_short_display(&*args[0])
        )),
    }
}
