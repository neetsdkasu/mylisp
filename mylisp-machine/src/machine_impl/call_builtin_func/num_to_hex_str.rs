use crate::builtin_func::BuiltinFunc::NumToHexStr;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    let s = match &**args[0] {
        Code::Int32(v) => format!("{:X}", *v),
        Code::Int64(ref v) => format!("{:X}", **v),
        _ => {
            return Err(illegal_argument!(
                "not integer",
                &constants.case.apply(NumToHexStr.symbol()),
                0,
                constants.get_short_display(&*args[0])
            ));
        }
    };
    Ok(Code::Str(Box::new(s)).rc())
}
