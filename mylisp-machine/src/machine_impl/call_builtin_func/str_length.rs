use crate::builtin_func::BuiltinFunc::StrLength;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    let len = match &**args[0] {
        Code::Str(ref s) => s.chars().count(),
        _ => {
            return Err(illegal_argument!(
                "not string",
                &constants.case.apply(StrLength.symbol()),
                0,
                constants.get_short_display(&*args[0])
            ));
        }
    };
    let len_i32 = len as i32;
    if len_i32 as usize == len {
        return Ok(Code::Int32(len_i32).rc());
    }
    let len_i64 = len as i64;
    if len_i64 as usize == len {
        Ok(Code::Int64(Box::new(len_i64)).rc())
    } else {
        Err(illegal_argument!(
            "too big string length",
            &constants.case.apply(StrLength.symbol()),
            0,
            constants.get_short_display(&*args[0])
        ))
    }
}
