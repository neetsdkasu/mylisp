use crate::builtin_func::BuiltinFunc::StrSubstr;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    let s = match &**args[0] {
        Code::Str(ref s) => s,
        _ => {
            return Err(illegal_argument!(
                "arg TARGET not string",
                &constants.case.apply(StrSubstr.symbol()),
                0,
                constants.get_short_display(&*args[0])
            ));
        }
    };
    let start = get_value(&*args[1]).ok_or_else(|| {
        illegal_argument!(
            "arg START-POS wrong value",
            &constants.case.apply(StrSubstr.symbol()),
            1,
            constants.get_short_display(&*args[1])
        )
    })?;
    let chars = s.chars();
    let chars = chars.skip(start);
    let s = if let Some(code) = args.get(2) {
        let len = get_value(code).ok_or_else(|| {
            illegal_argument!(
                "arg LENGTH wrong value",
                &constants.case.apply(StrSubstr.symbol()),
                2,
                constants.get_short_display(&*args[2])
            )
        })?;
        chars.take(len).collect()
    } else {
        chars.collect()
    };
    Ok(Code::Str(Box::new(s)).rc())
}

fn get_value(code: &Code) -> Option<usize> {
    let v = match code {
        Code::Int32(v) => *v as i64,
        Code::Int64(ref v) => **v,
        Code::Float64(ref v) => {
            let w = **v as i64;
            if (w as f64 - **v).abs() > f64::EPSILON {
                return None;
            } else {
                w
            }
        }
        _ => return None,
    };
    if v < 0 {
        return None;
    }
    let u = v as usize;
    if u as i64 == v {
        Some(u)
    } else {
        None
    }
}
