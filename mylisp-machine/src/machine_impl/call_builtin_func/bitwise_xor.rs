use crate::builtin_func::BuiltinFunc::BitwiseXor;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    let mut tmp: i32 = 0;
    for (i, arg) in args.iter().enumerate() {
        match &***arg {
            Code::Int32(v) => tmp ^= *v,
            Code::Int64(_) => return xor_int64(constants, tmp as i64, i, args),
            Code::Float64(ref v) => {
                let w = **v as i32;
                if (w as f64 - **v).abs() > f64::EPSILON {
                    return xor_int64(constants, tmp as i64, i, args);
                }
                tmp ^= w;
            }
            _ => {
                return Err(illegal_argument!(
                    "not number",
                    constants.case.apply(BitwiseXor.symbol()),
                    i,
                    constants.get_short_display(arg)
                ))
            }
        }
    }
    Ok(Code::Int32(tmp).rc())
}

fn xor_int64(constants: &Constants, mut tmp: i64, start: usize, args: ArgList) -> Result {
    for (i, arg) in args.iter().enumerate().skip(start) {
        match &***arg {
            Code::Int32(v) => tmp ^= *v as i64,
            Code::Int64(ref v) => tmp ^= **v,
            Code::Float64(ref v) => {
                let w = **v as i64;
                if (w as f64 - **v).abs() > f64::EPSILON {
                    return Err(illegal_argument!(
                        "wrong value",
                        constants.case.apply(BitwiseXor.symbol()),
                        i,
                        constants.get_short_display(arg)
                    ));
                }
                tmp ^= w;
            }
            _ => {
                return Err(illegal_argument!(
                    "not number",
                    constants.case.apply(BitwiseXor.symbol()),
                    i,
                    constants.get_short_display(arg)
                ))
            }
        }
    }
    Ok(Code::Int64(Box::new(tmp)).rc())
}
