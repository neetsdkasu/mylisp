use crate::builtin_func::BuiltinFunc::NumIDiv;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    let mut tmp: i32;
    match &**args[0] {
        Code::Int32(v) => tmp = *v,
        Code::Int64(ref v) => return div_int64(constants, **v, 1, args),
        Code::Float64(ref v) => {
            if let Some(w) = f64_to_i32(**v) {
                tmp = w;
            } else if let Some(w) = f64_to_i64(**v) {
                return div_int64(constants, w, 1, args);
            } else {
                return Err(illegal_argument!(
                    "wrong value",
                    constants.case.apply(NumIDiv.symbol()),
                    0,
                    constants.get_short_display(args[0])
                ));
            }
        }
        _ => {
            return Err(illegal_argument!(
                "not number",
                constants.case.apply(NumIDiv.symbol()),
                0,
                constants.get_short_display(args[0])
            ))
        }
    }
    for (i, arg) in args.iter().enumerate().skip(1) {
        match &***arg {
            Code::Int32(v) => {
                if let Some(new_tmp) = tmp.checked_div(*v) {
                    tmp = new_tmp;
                } else {
                    return div_int64(constants, tmp as i64, i, args);
                }
            }
            Code::Int64(_) => return div_int64(constants, tmp as i64, i, args),
            Code::Float64(ref v) => {
                if let Some(v) = f64_to_i32(**v) {
                    if let Some(new_tmp) = tmp.checked_div(v) {
                        tmp = new_tmp;
                    } else {
                        return div_int64(constants, tmp as i64, i, args);
                    }
                } else {
                    return div_int64(constants, tmp as i64, i, args);
                }
            }
            _ => {
                return Err(illegal_argument!(
                    "not number",
                    constants.case.apply(NumIDiv.symbol()),
                    i,
                    constants.get_short_display(*arg)
                ))
            }
        }
    }
    Ok(Code::Int32(tmp).rc())
}

fn div_int64(constants: &Constants, mut tmp: i64, start: usize, args: ArgList) -> Result {
    for (i, arg) in args.iter().enumerate().skip(start) {
        match &***arg {
            Code::Int32(v) => {
                if let Some(new_tmp) = tmp.checked_div(*v as i64) {
                    tmp = new_tmp;
                } else {
                    return Err(illegal_argument!(
                        "wrong value",
                        constants.case.apply(NumIDiv.symbol()),
                        i,
                        constants.get_short_display(*arg)
                    ));
                }
            }
            Code::Int64(ref v) => {
                if let Some(new_tmp) = tmp.checked_div(**v) {
                    tmp = new_tmp;
                } else {
                    return Err(illegal_argument!(
                        "wrong value",
                        constants.case.apply(NumIDiv.symbol()),
                        i,
                        constants.get_short_display(*arg)
                    ));
                }
            }
            Code::Float64(ref v) => {
                if let Some(v) = f64_to_i64(**v) {
                    if let Some(new_tmp) = tmp.checked_div(v) {
                        tmp = new_tmp;
                    } else {
                        return Err(illegal_argument!(
                            "wrong value",
                            constants.case.apply(NumIDiv.symbol()),
                            i,
                            constants.get_short_display(*arg)
                        ));
                    }
                } else {
                    return Err(illegal_argument!(
                        "wrong value",
                        constants.case.apply(NumIDiv.symbol()),
                        i,
                        constants.get_short_display(*arg)
                    ));
                }
            }
            _ => {
                return Err(illegal_argument!(
                    "not number",
                    constants.case.apply(NumIDiv.symbol()),
                    i,
                    constants.get_short_display(*arg)
                ))
            }
        }
    }
    Ok(Code::Int64(Box::new(tmp)).rc())
}

fn f64_to_i32(v: f64) -> Option<i32> {
    let x = v as i32;
    if ((x as f64) - v).abs() > f64::EPSILON {
        None
    } else {
        Some(x)
    }
}

fn f64_to_i64(v: f64) -> Option<i64> {
    let x = v as i64;
    if ((x as f64) - v).abs() > f64::EPSILON {
        None
    } else {
        Some(x)
    }
}
