use crate::builtin_func::BuiltinFunc::StrToNum;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    let s = match &**args[0] {
        Code::Str(ref s) => s,
        _ => {
            return Err(illegal_argument!(
                "arg TARGET not string",
                &constants.case.apply(StrToNum.symbol()),
                0,
                constants.get_short_display(&*args[0])
            ));
        }
    };
    let r = if let Some(code) = args.get(1) {
        get_value(code).ok_or_else(|| {
            illegal_argument!(
                "arg RADIX wrong value",
                &constants.case.apply(StrToNum.symbol()),
                1,
                constants.get_short_display(&*args[1])
            )
        })?
    } else {
        10
    };

    if !(2..=36).contains(&r) {
        return Err(illegal_argument!(
            "arg RADIX wrong value (require 2 .. 36 for integer)",
            &constants.case.apply(StrToNum.symbol()),
            1,
            constants.get_short_display(&*args[1])
        ));
    }

    if let Ok(v) = i32::from_str_radix(&**s, r) {
        return Ok(Code::Int32(v).rc());
    }

    if let Ok(v) = i64::from_str_radix(&**s, r) {
        return Ok(Code::Int64(Box::new(v)).rc());
    }

    if r != 10 {
        return Err(illegal_argument!(
            "arg RADIX wrong value (must be 10 for float)",
            &constants.case.apply(StrToNum.symbol()),
            1,
            constants.get_short_display(&*args[1])
        ));
    }

    if let Ok(v) = s.parse::<f64>() {
        Ok(Code::Float64(Box::new(v)).rc())
    } else {
        Err(illegal_argument!(
            "arg TARGET not number",
            &constants.case.apply(StrToNum.symbol()),
            0,
            constants.get_short_display(&*args[0])
        ))
    }
}

fn get_value(code: &Code) -> Option<u32> {
    let v = match code {
        Code::Int32(v) => *v as i64,
        Code::Int64(ref v) => **v,
        Code::Float64(ref v) => {
            let w = **v as i64;
            if (w as f64 - **v).abs() > f64::EPSILON {
                return None;
            } else {
                w
            }
        }
        _ => return None,
    };
    if v < 0 {
        return None;
    }
    let u = v as u32;
    if u as i64 == v {
        Some(u)
    } else {
        None
    }
}
