use std::cmp::Ordering;

use crate::builtin_func::BuiltinFunc::StrCompare;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    let s0 = match &**args[0] {
        Code::Str(ref s) => s,
        _ => {
            return Err(illegal_argument!(
                "not string",
                constants.case.apply(StrCompare.symbol()),
                0,
                constants.get_short_display(args[0])
            ))
        }
    };
    let s1 = match &**args[1] {
        Code::Str(ref s) => s,
        _ => {
            return Err(illegal_argument!(
                "not string",
                constants.case.apply(StrCompare.symbol()),
                1,
                constants.get_short_display(args[1])
            ))
        }
    };

    let ret = match s0.cmp(s1) {
        Ordering::Less => constants.code_negative_one(),
        Ordering::Equal => constants.code_zero(),
        Ordering::Greater => constants.code_positive_one(),
    };
    Ok(ret)
}
