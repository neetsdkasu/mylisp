use crate::builtin_func::BuiltinFunc::StrConcat;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    let mut buf = String::new();
    for (i, arg) in args.iter().enumerate() {
        match &***arg {
            Code::Cons(_) => {
                let codes = arg.to_vec().ok_or_else(|| {
                    illegal_argument!(
                        "not list:",
                        constants.case.apply(StrConcat.symbol()),
                        i,
                        constants.get_short_display(arg)
                    )
                })?;
                for code in &codes {
                    if let Code::Str(ref s) = &**code {
                        buf += &**s;
                    } else {
                        return Err(illegal_argument!(
                            "not string",
                            constants.case.apply(StrConcat.symbol()),
                            i,
                            constants.get_short_display(arg)
                        ));
                    }
                }
            }
            Code::Str(ref s) => buf += &**s,
            _ => {
                return Err(illegal_argument!(
                    "not string",
                    constants.case.apply(StrConcat.symbol()),
                    i,
                    constants.get_short_display(arg)
                ))
            }
        }
    }
    Ok(Code::Str(Box::new(buf)).rc())
}
