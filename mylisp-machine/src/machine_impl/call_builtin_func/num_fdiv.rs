use crate::builtin_func::BuiltinFunc::NumFDiv;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    let mut tmp: f64;
    match &**args[0] {
        Code::Int32(v) => tmp = *v as f64,
        Code::Int64(ref v) => tmp = **v as f64,
        Code::Float64(ref v) => tmp = **v,
        _ => {
            return Err(illegal_argument!(
                "not number",
                constants.case.apply(NumFDiv.symbol()),
                0,
                constants.get_short_display(args[0])
            ))
        }
    }
    for (i, arg) in args.iter().enumerate().skip(1) {
        match &***arg {
            Code::Int32(v) => tmp /= *v as f64,
            Code::Int64(ref v) => tmp /= **v as f64,
            Code::Float64(ref v) => tmp /= **v,
            _ => {
                return Err(illegal_argument!(
                    "not number",
                    constants.case.apply(NumFDiv.symbol()),
                    i,
                    constants.get_short_display(*arg)
                ))
            }
        }
    }
    Ok(Code::Float64(Box::new(tmp)).rc())
}
