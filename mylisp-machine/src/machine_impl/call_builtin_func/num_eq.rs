use crate::builtin_func::BuiltinFunc::NumEq;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    let tmp: i32;
    match &**args[0] {
        Code::Int32(v) => tmp = *v,
        Code::Int64(ref v) => return eq_int64(constants, **v, 1, args),
        Code::Float64(ref v) => return eq_float64(constants, **v, 1, args),
        _ => {
            return Err(illegal_argument!(
                "not number",
                constants.case.apply(NumEq.symbol()),
                0,
                constants.get_short_display(args[0])
            ))
        }
    }
    for (i, arg) in args.iter().enumerate().skip(1) {
        match &***arg {
            Code::Int32(v) => {
                if tmp != *v {
                    return Ok(constants.code_false());
                }
            }
            Code::Int64(_) => return eq_int64(constants, tmp as i64, i, args),
            Code::Float64(_) => return eq_float64(constants, tmp as f64, i, args),
            _ => {
                return Err(illegal_argument!(
                    "not number",
                    constants.case.apply(NumEq.symbol()),
                    i,
                    constants.get_short_display(*arg)
                ))
            }
        }
    }
    Ok(constants.code_true())
}

fn eq_int64(constants: &Constants, tmp: i64, start: usize, args: ArgList) -> Result {
    for (i, arg) in args.iter().enumerate().skip(start) {
        match &***arg {
            Code::Int32(v) => {
                if tmp != (*v as i64) {
                    return Ok(constants.code_false());
                }
            }
            Code::Int64(ref v) => {
                if tmp != **v {
                    return Ok(constants.code_false());
                }
            }
            Code::Float64(_) => return eq_float64(constants, tmp as f64, i, args),
            _ => {
                return Err(illegal_argument!(
                    "not number",
                    constants.case.apply(NumEq.symbol()),
                    i,
                    constants.get_short_display(*arg)
                ))
            }
        }
    }
    Ok(constants.code_true())
}

fn eq_float64(constants: &Constants, tmp: f64, start: usize, args: ArgList) -> Result {
    for (i, arg) in args.iter().enumerate().skip(start) {
        match &***arg {
            Code::Int32(v) => {
                if (tmp - (*v as f64)).abs() > f64::EPSILON {
                    return Ok(constants.code_false());
                }
            }
            Code::Int64(ref v) => {
                if (tmp - (**v as f64)).abs() > f64::EPSILON {
                    return Ok(constants.code_false());
                }
            }
            Code::Float64(ref v) => {
                if (tmp - **v).abs() > f64::EPSILON {
                    return Ok(constants.code_false());
                }
            }
            _ => {
                return Err(illegal_argument!(
                    "not number",
                    constants.case.apply(NumEq.symbol()),
                    i,
                    constants.get_short_display(*arg)
                ))
            }
        }
    }
    Ok(constants.code_true())
}
