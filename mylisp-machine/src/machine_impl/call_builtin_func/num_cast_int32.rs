use crate::builtin_func::BuiltinFunc::NumCastInt32;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    let v = match &**args[0] {
        Code::Int32(v) => *v,
        Code::Int64(ref v) => **v as i32,
        Code::Float64(ref v) => **v as i32,
        _ => {
            return Err(illegal_argument!(
                "not number",
                &constants.case.apply(NumCastInt32.symbol()),
                0,
                constants.get_short_display(&*args[0])
            ));
        }
    };
    Ok(Code::Int32(v).rc())
}
