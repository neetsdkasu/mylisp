use std::rc::Rc;

use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    let mut list = constants.code_nil();
    for code in args.iter().rev() {
        list = Code::cons(Rc::clone(code), list);
    }
    Ok(list)
}
