use crate::builtin_func::BuiltinFunc::BitwiseShiftRight;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    macro_rules! illegal_shift {
        ($cause:literal) => {
            illegal_argument!(
                $cause,
                &constants.case.apply(BitwiseShiftRight.symbol()),
                1,
                constants.get_short_display(&*args[1])
            )
        };
    }

    let shift_tmp = match &**args[1] {
        Code::Int32(v) => *v as i64,
        Code::Int64(ref v) => **v,
        Code::Float64(ref v) => {
            let w = **v as i64;
            if (w as f64 - **v).abs() > f64::EPSILON {
                return Err(illegal_shift!("arg SHIFT wrong value"));
            } else {
                w
            }
        }
        _ => return Err(illegal_shift!("arg SHIFT not number")),
    };
    if shift_tmp < 0 {
        return Err(illegal_shift!("arg SHIFT wrong value"));
    }
    let shift = shift_tmp as u32;
    if shift as i64 != shift_tmp {
        return Err(illegal_shift!("arg SHIFT wrong value"));
    }

    match &**args[0] {
        Code::Int32(v) => {
            if let Some(res) = v.checked_shr(shift) {
                return Ok(Code::Int32(res).rc());
            }
            if let Some(res) = (*v as i64).checked_shr(shift) {
                return Ok(Code::Int64(Box::new(res)).rc());
            }
        }
        Code::Int64(ref v) => {
            if let Some(res) = v.checked_shr(shift) {
                return Ok(Code::Int64(Box::new(res)).rc());
            }
        }
        Code::Float64(ref v) => {
            let w = **v as i32;
            if (w as f64 - **v).abs() <= f64::EPSILON {
                if let Some(res) = w.checked_shr(shift) {
                    return Ok(Code::Int32(res).rc());
                }
            }
            let w = **v as i64;
            if (w as f64 - **v).abs() <= f64::EPSILON {
                if let Some(res) = w.checked_shr(shift) {
                    return Ok(Code::Int64(Box::new(res)).rc());
                }
            }
        }
        _ => {
            return Err(illegal_argument!(
                "arg VALUE not number",
                &constants.case.apply(BitwiseShiftRight.symbol()),
                0,
                constants.get_short_display(&*args[0])
            ))
        }
    }
    Err(illegal_argument!(
        "arg VALUE wrong value",
        &constants.case.apply(BitwiseShiftRight.symbol()),
        0,
        constants.get_short_display(&*args[0])
    ))
}
