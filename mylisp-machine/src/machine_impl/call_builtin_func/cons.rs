use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(_constants: &Constants, args: ArgList) -> Result {
    Ok(Code::cons(args[0].clone(), args[1].clone()))
}
