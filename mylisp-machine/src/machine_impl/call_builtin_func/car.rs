use crate::builtin_func::BuiltinFunc::Car;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    if let Code::Cons(ref cell) = &**args[0] {
        Ok(cell.car.clone())
    } else {
        Err(illegal_argument!(
            "not cons cell",
            &constants.case.apply(Car.symbol()),
            0,
            constants.get_short_display(&*args[0])
        ))
    }
}
