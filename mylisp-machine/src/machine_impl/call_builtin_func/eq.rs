use crate::builtin_func::BuiltinFunc::Eq;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    if let Some(ret) = Code::eq(&*args[0], &*args[1]) {
        if ret {
            Ok(constants.code_t())
        } else {
            Ok(constants.code_nil())
        }
    } else {
        for (i, arg) in args.iter().enumerate().take(2) {
            if Code::eq(*arg, *arg).is_none() {
                return Err(illegal_argument!(
                    "not symbol arg",
                    &constants.case.apply(Eq.symbol()),
                    i,
                    constants.get_short_display(&**arg)
                ));
            }
        }
        Err(bug!())
    }
}
