use std::rc::Rc;

use crate::builtin_func::BuiltinFunc;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Machine;
use super::Result;
use super::RunResult;
use super::Value;

mod and_impl;
mod atom;
mod bitwise_and;
mod bitwise_not;
mod bitwise_or;
mod bitwise_shift_left;
mod bitwise_shift_right;
mod bitwise_xor;
mod bool_impl;
mod car;
mod cdr;
mod cons;
mod eq;
mod equals;
mod is_list;
mod is_nil;
mod make_list;
mod not_impl;
mod num_add;
mod num_cast_float64;
mod num_cast_int32;
mod num_cast_int64;
mod num_eq;
mod num_fdiv;
mod num_ge;
mod num_gt;
mod num_idiv;
mod num_le;
mod num_lt;
mod num_mul;
mod num_rem;
mod num_sub;
mod num_to_bin_str;
mod num_to_hex_str;
mod num_to_str;
mod or_impl;
mod str_compare;
mod str_concat;
mod str_length;
mod str_split;
mod str_substr;
mod str_to_num;
mod time_now;

impl<'a> Machine<'a> {
    pub(super) fn call_builtin_func(&mut self) -> RunResult {
        let ac = unwrap!(self.call_stack.pop());
        let count = match &*ac {
            Value::ArgCount(count) => *count as usize,
            _ => return Err(bug!()),
        };
        let callable_pos = self.call_stack.len() - count;
        let list: Vec<_> = self.call_stack.drain(callable_pos..).collect();
        let bf = match &**unwrap!(list[0].get_code()) {
            Code::BuiltinFunc(bf) => *bf,
            _ => return Err(bug!()),
        };
        let args: Vec<&Rc<Code>> = list[1..]
            .iter()
            .try_fold(Vec::new(), |mut acc, v| match &**v {
                Value::Code(code) => {
                    acc.push(code);
                    Some(acc)
                }
                _ => None,
            })
            .ok_or_else(|| bug!())?;
        use BuiltinFunc::*;
        let ret_code = match bf {
            Atom => atom::func(self.constants, &args)?,
            Car => car::func(self.constants, &args)?,
            Cdr => cdr::func(self.constants, &args)?,
            Cons => cons::func(self.constants, &args)?,
            Eq => eq::func(self.constants, &args)?,
            NumAdd => num_add::func(self.constants, &args)?,
            NumSub => num_sub::func(self.constants, &args)?,
            NumMul => num_mul::func(self.constants, &args)?,
            NumIDiv => num_idiv::func(self.constants, &args)?,
            NumFDiv => num_fdiv::func(self.constants, &args)?,
            NumRem => num_rem::func(self.constants, &args)?,
            NumEq => num_eq::func(self.constants, &args)?,
            NumLT => num_lt::func(self.constants, &args)?,
            NumLE => num_le::func(self.constants, &args)?,
            NumGT => num_gt::func(self.constants, &args)?,
            NumGE => num_ge::func(self.constants, &args)?,
            Bool => bool_impl::func(self.constants, &args)?,
            And => and_impl::func(self.constants, &args)?,
            Or => or_impl::func(self.constants, &args)?,
            Not => not_impl::func(self.constants, &args)?,
            TimeNow => time_now::func(self.constants, &args)?,
            StrConcat => str_concat::func(self.constants, &args)?,
            StrSubstr => str_substr::func(self.constants, &args)?,
            StrSplit => str_split::func(self.constants, &args)?,
            StrToNum => str_to_num::func(self.constants, &args)?,
            NumToStr => num_to_str::func(self.constants, &args)?,
            StrCompare => str_compare::func(self.constants, &args)?,
            StrLength => str_length::func(self.constants, &args)?,
            NumCastInt32 => num_cast_int32::func(self.constants, &args)?,
            NumCastInt64 => num_cast_int64::func(self.constants, &args)?,
            NumCastFloat64 => num_cast_float64::func(self.constants, &args)?,
            BitwiseAnd => bitwise_and::func(self.constants, &args)?,
            BitwiseOr => bitwise_or::func(self.constants, &args)?,
            BitwiseXor => bitwise_xor::func(self.constants, &args)?,
            BitwiseNot => bitwise_not::func(self.constants, &args)?,
            BitwiseShiftLeft => bitwise_shift_left::func(self.constants, &args)?,
            BitwiseShiftRight => bitwise_shift_right::func(self.constants, &args)?,
            NumToBinStr => num_to_bin_str::func(self.constants, &args)?,
            NumToHexStr => num_to_hex_str::func(self.constants, &args)?,
            IsNil => is_nil::func(self.constants, &args)?,
            IsList => is_list::func(self.constants, &args)?,
            MakeList => make_list::func(self.constants, &args)?,
            Equals => equals::func(self.constants, &args)?,
        };
        self.call_stack.push(Value::Code(ret_code).rc());
        Ok(())
    }
}
