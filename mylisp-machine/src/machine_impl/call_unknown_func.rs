use std::io;

use crate::code::Code;
use crate::engine::IOSet;

use super::ArgCountValidator;
use super::CallStackState;
use super::Error;
use super::Machine;
use super::ProgramStore;
use super::Value;

impl<'a> Machine<'a> {
    pub(super) fn call_unknown_func<R, W, E>(
        &mut self,
        store: &mut ProgramStore,
        io_set: &mut IOSet<R, W, E>,
    ) -> Result<Option<CallStackState>, Error>
    where
        R: io::BufRead,
        W: io::Write,
        E: io::Write,
    {
        let ac = unwrap!(self.call_stack.last());
        let count = match &**ac {
            Value::ArgCount(count) => *count as usize,
            _ => return Err(bug!()),
        };
        let spos = self.call_stack.len() - count - 1;
        let callable = unwrap!(self.call_stack.get(spos));
        let callable = unwrap!(callable.get_code());
        let info = match &**callable {
            Code::BuiltinFunc(bf) => {
                if !bf.validate_arg_count(count - 1) {
                    return Err("illegal arguments".to_string());
                }
                self.call_builtin_func()?;
                return Ok(None);
            }
            Code::IOFunc(iof) => {
                if !iof.validate_arg_count(count - 1) {
                    return Err("illegal arguments".to_string());
                }
                self.call_io_func(io_set)?;
                return Ok(None);
            }
            Code::Lambda(ref info) => info,
            _ => return Err(bug!()),
        };
        let binded_cl = &store.binded_closures[info.id as usize];
        let id = binded_cl.id;
        let cl = store.closures[id].clone();
        if !cl.info.validate_arg_count(count - 1) {
            return Err("illegal arguments".to_string());
        }
        let bindings = binded_cl.bindings.clone();
        let base_spos = self.call_stack.len();
        Ok(Some(CallStackState {
            commands: cl.commands.clone(),
            bindings,
            base_spos,
            pos: 0,
        }))
    }
}
