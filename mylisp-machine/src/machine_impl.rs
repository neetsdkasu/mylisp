use std::io;
use std::rc::Rc;
use std::result;

use crate::code::Code;
use crate::engine::IOSet;

use super::arg_count_validator::ArgCountValidator;
use super::command::Command;
use super::constants::Constants;
use super::program_store::{BindedClosure, ProgramStore};
use super::value::Value;

use run_impl::CallStackState;

type Error = String;

type RunResult = result::Result<(), Error>;

// for func
type Result = result::Result<Rc<Code>, Error>;
type ArgList<'a, 'b> = &'a [&'b Rc<Code>];

macro_rules! illegal_argument {
    ($msg:literal, $func:expr, $i:expr, $arg:expr) => {
        format!(
            concat!("Illegal Argument( {} ) ", $msg, " : ({}) [ {} ]"),
            $func, $i, $arg
        )
    };
}

macro_rules! unwrap {
    ($opt:expr) => {
        $opt.ok_or_else(|| bug!())?
    };
}

pub struct MacroExtender<'a> {
    constants: &'a Constants,
}

mod macro_extender_impl;

pub struct Machine<'a> {
    call_stack: Vec<Rc<Value>>,
    constants: &'a Constants,
}

impl<'a> Machine<'a> {
    pub fn new(constants: &'a Constants) -> Machine {
        Machine {
            call_stack: Vec::new(),
            constants,
        }
    }
}

mod call_builtin_func;
mod call_io_func;
mod call_unknown_func;
mod run_impl;

impl<'a> Machine<'a> {
    pub fn run<R, W, E>(
        &mut self,
        store: &mut ProgramStore,
        commands: Vec<Command>,
        io_set: &mut IOSet<R, W, E>,
    ) -> result::Result<Rc<Code>, Error>
    where
        R: io::BufRead,
        W: io::Write,
        E: io::Write,
    {
        let state = CallStackState {
            commands: Rc::new(commands),
            bindings: Rc::new(Vec::with_capacity(0)),
            base_spos: self.call_stack.len(),
            pos: 0,
        };
        self.run_impl(store, state, io_set)?;
        let res = match self.call_stack.pop() {
            Some(res) if self.call_stack.is_empty() => res,
            value => {
                return Err(bug!(
                    "invalid call stack [ {:?} ], code [ {:?} ]",
                    self.call_stack,
                    value
                ));
            }
        };
        match &*res {
            Value::Code(code) => Ok(code.clone()),
            value => Err(bug!("invalid return value [ {:?} ]", value)),
        }
    }
}
