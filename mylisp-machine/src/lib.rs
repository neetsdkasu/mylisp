pub use program_manager::ProgramManager;

use mylisp_code as code;
use mylisp_common::*;
use mylisp_engine as engine;

const MAX_ARG_SIZE: usize = 100;

#[macro_use]
mod utils;

mod arg_count_validator;
mod command;
mod compiler;
mod constants;
mod machine_impl;
mod program_manager;
mod program_store;
mod value;
