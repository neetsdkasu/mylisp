use std::io;

use crate::code::Code;
use crate::io_func::IOFunc::Readln;

use super::ArgList;
use super::Constants;
use super::Error;
use super::IOSet;
use super::Result;

const OMITS: &[char] = &['\r', '\n'];

pub(super) fn func<R, W, E>(
    io_set: &mut IOSet<R, W, E>,
    _constants: &Constants,
    _args: ArgList,
) -> Result
where
    R: io::BufRead,
{
    let mut buf = String::new();

    io_set
        .input
        .read_line(&mut buf)
        .map_err(|err| Error::io_error(&io_set.case.apply(Readln.symbol()), err))?;

    buf = buf.trim_end_matches(OMITS).to_owned();

    Ok(Code::Str(Box::new(buf)).rc())
}
