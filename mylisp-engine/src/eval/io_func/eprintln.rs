use std::io;

use crate::code::Code;
use crate::io_func::IOFunc::Eprintln;

use super::ArgList;
use super::Constants;
use super::Error;
use super::IOSet;
use super::Result;

pub(super) fn func<R, W, E>(
    io_set: &mut IOSet<R, W, E>,
    constants: &Constants,
    args: ArgList,
) -> Result
where
    E: io::Write,
{
    macro_rules! io_error {
        ($err:expr) => {
            Error::io_error(&io_set.case.apply(Eprintln.symbol()), $err)
        };
    }

    for (i, code) in args.iter().enumerate() {
        if i > 0 {
            write!(&mut io_set.error, " ").or_else(|err| Err(io_error!(err)))?;
        }
        match &**code {
            Code::Str(ref s) => {
                write!(&mut io_set.error, "{}", s).or_else(|err| Err(io_error!(err)))?;
            }
            _ => {
                let display = io_set.get_display(code);
                write!(&mut io_set.error, "{}", display).or_else(|err| Err(io_error!(err)))?;
            }
        }
    }

    writeln!(&mut io_set.error).or_else(|err| Err(io_error!(err)))?;

    Ok(constants.nil())
}
