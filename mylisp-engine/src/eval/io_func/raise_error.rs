use crate::code::Code;
use crate::io_func::IOFunc::RaiseError;

use super::ArgList;
use super::Constants;
use super::Error;
use super::IOSet;
use super::Result;

pub(super) fn func<R, W, E>(
    io_set: &mut IOSet<R, W, E>,
    constants: &Constants,
    mut args: ArgList,
) -> Result {
    let mut code = constants.nil();
    while let Some(car) = args.pop() {
        code = Code::cons(car, code);
    }
    let display = io_set.get_display(&code);
    Err(Error::with_cause(
        &io_set.case.apply(RaiseError.symbol()),
        &format!("{}", display),
    ))
}
