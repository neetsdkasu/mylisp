use crate::builtin_func::BuiltinFunc;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Error;
use super::Result;
use super::MAX_ARG_SIZE;

mod and_impl;
mod atom;
mod bitwise_and;
mod bitwise_not;
mod bitwise_or;
mod bitwise_shift_left;
mod bitwise_shift_right;
mod bitwise_xor;
mod bool_impl;
mod car;
mod cdr;
mod cons;
mod eq;
mod equals;
mod is_list;
mod is_nil;
mod make_list;
mod not_impl;
mod num_add;
mod num_cast_float64;
mod num_cast_int32;
mod num_cast_int64;
mod num_eq;
mod num_fdiv;
mod num_ge;
mod num_gt;
mod num_idiv;
mod num_le;
mod num_lt;
mod num_mul;
mod num_rem;
mod num_sub;
mod num_to_bin_str;
mod num_to_hex_str;
mod num_to_str;
mod or_impl;
mod str_compare;
mod str_concat;
mod str_length;
mod str_split;
mod str_substr;
mod str_to_num;
mod time_now;

type CallBack = fn(constants: &Constants, args: ArgList) -> Result;

pub(super) struct BuiltinFuncInfo {
    func_type: BuiltinFunc,
    arg_min_size: usize,
    arg_max_size: usize,
    call_back: CallBack,
}

impl BuiltinFuncInfo {
    pub(super) fn func_type(&self) -> BuiltinFunc {
        self.func_type
    }

    pub(super) fn get_call_back(&self) -> CallBack {
        self.call_back
    }

    pub(super) fn validate_args_count(&self, args: &Code) -> Option<bool> {
        args.list_depth()
            .map(|depth| self.arg_min_size <= depth && depth <= self.arg_max_size)
    }
}

pub(super) fn get_builtin_func_info(func: BuiltinFunc) -> &'static BuiltinFuncInfo {
    use BuiltinFunc::*;
    match func {
        Atom => &BuiltinFuncInfo {
            func_type: Atom,
            arg_min_size: 1,
            arg_max_size: 1,
            call_back: atom::func,
        },
        Car => &BuiltinFuncInfo {
            func_type: Car,
            arg_min_size: 1,
            arg_max_size: 1,
            call_back: car::func,
        },
        Cdr => &BuiltinFuncInfo {
            func_type: Cdr,
            arg_min_size: 1,
            arg_max_size: 1,
            call_back: cdr::func,
        },
        Cons => &BuiltinFuncInfo {
            func_type: Cons,
            arg_min_size: 2,
            arg_max_size: 2,
            call_back: cons::func,
        },
        Eq => &BuiltinFuncInfo {
            func_type: Eq,
            arg_min_size: 2,
            arg_max_size: 2,
            call_back: eq::func,
        },
        NumAdd => &BuiltinFuncInfo {
            func_type: NumAdd,
            arg_min_size: 0,
            arg_max_size: MAX_ARG_SIZE,
            call_back: num_add::func,
        },
        NumSub => &BuiltinFuncInfo {
            func_type: NumSub,
            arg_min_size: 1,
            arg_max_size: MAX_ARG_SIZE,
            call_back: num_sub::func,
        },
        NumMul => &BuiltinFuncInfo {
            func_type: NumSub,
            arg_min_size: 0,
            arg_max_size: MAX_ARG_SIZE,
            call_back: num_mul::func,
        },
        NumIDiv => &BuiltinFuncInfo {
            func_type: NumIDiv,
            arg_min_size: 2,
            arg_max_size: MAX_ARG_SIZE,
            call_back: num_idiv::func,
        },
        NumFDiv => &BuiltinFuncInfo {
            func_type: NumFDiv,
            arg_min_size: 2,
            arg_max_size: MAX_ARG_SIZE,
            call_back: num_fdiv::func,
        },
        NumRem => &BuiltinFuncInfo {
            func_type: NumRem,
            arg_min_size: 2,
            arg_max_size: MAX_ARG_SIZE,
            call_back: num_rem::func,
        },
        NumEq => &BuiltinFuncInfo {
            func_type: NumEq,
            arg_min_size: 2,
            arg_max_size: MAX_ARG_SIZE,
            call_back: num_eq::func,
        },
        NumLT => &BuiltinFuncInfo {
            func_type: NumLT,
            arg_min_size: 2,
            arg_max_size: MAX_ARG_SIZE,
            call_back: num_lt::func,
        },
        NumLE => &BuiltinFuncInfo {
            func_type: NumLE,
            arg_min_size: 2,
            arg_max_size: MAX_ARG_SIZE,
            call_back: num_le::func,
        },
        NumGT => &BuiltinFuncInfo {
            func_type: NumGT,
            arg_min_size: 2,
            arg_max_size: MAX_ARG_SIZE,
            call_back: num_gt::func,
        },
        NumGE => &BuiltinFuncInfo {
            func_type: NumGE,
            arg_min_size: 2,
            arg_max_size: MAX_ARG_SIZE,
            call_back: num_ge::func,
        },
        Bool => &BuiltinFuncInfo {
            func_type: Bool,
            arg_min_size: 1,
            arg_max_size: 1,
            call_back: bool_impl::func,
        },
        And => &BuiltinFuncInfo {
            func_type: And,
            arg_min_size: 0,
            arg_max_size: MAX_ARG_SIZE,
            call_back: and_impl::func,
        },
        Or => &BuiltinFuncInfo {
            func_type: Or,
            arg_min_size: 0,
            arg_max_size: MAX_ARG_SIZE,
            call_back: or_impl::func,
        },
        Not => &BuiltinFuncInfo {
            func_type: Not,
            arg_min_size: 1,
            arg_max_size: 1,
            call_back: not_impl::func,
        },
        TimeNow => &BuiltinFuncInfo {
            func_type: TimeNow,
            arg_min_size: 0,
            arg_max_size: 0,
            call_back: time_now::func,
        },
        StrConcat => &BuiltinFuncInfo {
            func_type: StrConcat,
            arg_min_size: 1,
            arg_max_size: MAX_ARG_SIZE,
            call_back: str_concat::func,
        },
        StrSubstr => &BuiltinFuncInfo {
            func_type: StrSubstr,
            arg_min_size: 2,
            arg_max_size: 3,
            call_back: str_substr::func,
        },
        StrSplit => &BuiltinFuncInfo {
            func_type: StrSplit,
            arg_min_size: 1,
            arg_max_size: 2,
            call_back: str_split::func,
        },
        StrToNum => &BuiltinFuncInfo {
            func_type: StrToNum,
            arg_min_size: 1,
            arg_max_size: 2,
            call_back: str_to_num::func,
        },
        NumToStr => &BuiltinFuncInfo {
            func_type: NumToStr,
            arg_min_size: 1,
            arg_max_size: 1,
            call_back: num_to_str::func,
        },
        StrCompare => &BuiltinFuncInfo {
            func_type: StrCompare,
            arg_min_size: 2,
            arg_max_size: 2,
            call_back: str_compare::func,
        },
        StrLength => &BuiltinFuncInfo {
            func_type: StrLength,
            arg_min_size: 1,
            arg_max_size: 1,
            call_back: str_length::func,
        },
        NumCastInt32 => &BuiltinFuncInfo {
            func_type: NumCastInt32,
            arg_min_size: 1,
            arg_max_size: 1,
            call_back: num_cast_int32::func,
        },
        NumCastInt64 => &BuiltinFuncInfo {
            func_type: NumCastInt64,
            arg_min_size: 1,
            arg_max_size: 1,
            call_back: num_cast_int64::func,
        },
        NumCastFloat64 => &BuiltinFuncInfo {
            func_type: NumCastFloat64,
            arg_min_size: 1,
            arg_max_size: 1,
            call_back: num_cast_float64::func,
        },
        BitwiseAnd => &BuiltinFuncInfo {
            func_type: BitwiseAnd,
            arg_min_size: 0,
            arg_max_size: MAX_ARG_SIZE,
            call_back: bitwise_and::func,
        },
        BitwiseOr => &BuiltinFuncInfo {
            func_type: BitwiseOr,
            arg_min_size: 0,
            arg_max_size: MAX_ARG_SIZE,
            call_back: bitwise_or::func,
        },
        BitwiseXor => &BuiltinFuncInfo {
            func_type: BitwiseXor,
            arg_min_size: 0,
            arg_max_size: MAX_ARG_SIZE,
            call_back: bitwise_xor::func,
        },
        BitwiseNot => &BuiltinFuncInfo {
            func_type: BitwiseAnd,
            arg_min_size: 1,
            arg_max_size: 1,
            call_back: bitwise_not::func,
        },
        BitwiseShiftLeft => &BuiltinFuncInfo {
            func_type: BitwiseShiftLeft,
            arg_min_size: 2,
            arg_max_size: 2,
            call_back: bitwise_shift_left::func,
        },
        BitwiseShiftRight => &BuiltinFuncInfo {
            func_type: BitwiseShiftRight,
            arg_min_size: 2,
            arg_max_size: 2,
            call_back: bitwise_shift_right::func,
        },
        NumToBinStr => &BuiltinFuncInfo {
            func_type: NumToBinStr,
            arg_min_size: 1,
            arg_max_size: 1,
            call_back: num_to_bin_str::func,
        },
        NumToHexStr => &BuiltinFuncInfo {
            func_type: NumToHexStr,
            arg_min_size: 1,
            arg_max_size: 1,
            call_back: num_to_hex_str::func,
        },
        IsNil => &BuiltinFuncInfo {
            func_type: IsNil,
            arg_min_size: 1,
            arg_max_size: MAX_ARG_SIZE,
            call_back: is_nil::func,
        },
        IsList => &BuiltinFuncInfo {
            func_type: IsList,
            arg_min_size: 1,
            arg_max_size: MAX_ARG_SIZE,
            call_back: is_list::func,
        },
        MakeList => &BuiltinFuncInfo {
            func_type: IsNil,
            arg_min_size: 0,
            arg_max_size: MAX_ARG_SIZE,
            call_back: make_list::func,
        },
        Equals => &BuiltinFuncInfo {
            func_type: Equals,
            arg_min_size: 2,
            arg_max_size: MAX_ARG_SIZE,
            call_back: equals::func,
        },
    }
}
