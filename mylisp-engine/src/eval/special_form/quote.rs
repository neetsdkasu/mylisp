use crate::special_form::SpecialForm::Quote;

use super::Engine;
use super::Error;
use super::SpecialFormRequest;

pub(super) fn call<R, W, E>(
    engine: &mut Engine<R, W, E>,
    mut request: Box<SpecialFormRequest<R, W, E>>,
) -> Result<Box<SpecialFormRequest<R, W, E>>, Error> {
    let (car, _) = request
        .remaining_args
        .split()
        .filter(|(_, cdr)| cdr.is_nil())
        .ok_or_else(|| {
            Error::illegal_argument_with_detail(
                &engine.io_set.case.apply(Quote.symbol()),
                format!(
                    "invalid argument count: {} (must 1)",
                    request.remaining_args.list_depth().unwrap_or(0)
                ),
            )
        })?;
    request.code_is_result = true;
    request.code = Some(car.clone());
    Ok(request)
}

pub(super) fn trace<R, W, E>(
    _engine: &Engine<R, W, E>,
    _request: &SpecialFormRequest<R, W, E>,
) -> String {
    waiting_to_call!()
}
