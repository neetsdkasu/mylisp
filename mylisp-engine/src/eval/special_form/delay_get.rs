use crate::code::{Code, DelayedValue};
use crate::special_form::SpecialForm::DelayGet;

use super::Engine;
use super::Environment;
use super::Error;
use super::SpecialFormRequest;

#[allow(clippy::boxed_local)]
pub(super) fn call<R, W, E>(
    engine: &mut Engine<R, W, E>,
    mut request: Box<SpecialFormRequest<R, W, E>>,
) -> Result<Box<SpecialFormRequest<R, W, E>>, Error> {
    if let Some(code) = request.code.take() {
        if let Some(codes) = request.temporary_codes.take() {
            engine.pop_scope()?;
            let dv = match &*codes[0] {
                Code::Delayed(ref dv) => dv,
                _ => return Err(Error::wrap(bug!("in {:?}", DelayGet))),
            };
            if !matches!(&*dv.borrow(), DelayedValue::Evaluating) {
                // assert!でやるべき
                return Err(Error::wrap(bug!("in {:?}", DelayGet)));
            }
            *dv.borrow_mut() = DelayedValue::Evaluated(code.clone());
            request.code_is_result = true;
            request.code = Some(code);
        } else {
            let dv = match &*code {
                Code::Delayed(ref dv) => dv,
                _ => {
                    return Err(Error::illegal_argument_with_detail(
                        &engine.io_set.case.apply(DelayGet.symbol()),
                        format!(
                            "not delayed value: [ {} ]",
                            engine.io_set.get_display(&*code)
                        ),
                    ))
                }
            };
            let (value, bindings) = match &*dv.borrow() {
                DelayedValue::UnEvaluated(ref value, ref bindings) => {
                    (value.clone(), bindings.clone())
                }
                DelayedValue::Evaluating => {
                    return Err(Error::with_cause(
                        &engine.io_set.case.apply(DelayGet.symbol()),
                        "Delayed value cycled",
                    ))
                }
                DelayedValue::Evaluated(ref value) => {
                    request.code_is_result = true;
                    request.code = Some(value.clone());
                    return Ok(request);
                }
            };
            *dv.borrow_mut() = DelayedValue::Evaluating;
            request.code_is_result = false;
            request.code = Some(value);
            request.temporary_codes = Some(vec![code]);
            engine.push_scope(Environment::with_parent(bindings));
        }
        return Ok(request);
    }

    let (code, _) = request
        .remaining_args
        .split()
        .filter(|(_, rest)| rest.is_nil())
        .ok_or_else(|| {
            Error::illegal_argument_with_detail(
                &engine.io_set.case.apply(DelayGet.symbol()),
                format!(
                    "invalid ARG: [ {} ] (must be 1 argument)",
                    engine.io_set.get_display(&*request.remaining_args)
                ),
            )
        })?;

    request.code_is_result = false;
    request.code = Some(code.clone());
    Ok(request)
}

pub(super) fn trace<R, W, E>(
    engine: &Engine<R, W, E>,
    request: &SpecialFormRequest<R, W, E>,
) -> String {
    if let Some(ref code) = request.code {
        if request.temporary_codes.is_some() {
            format!(
                "eval DELAYED-VALUE [ {} ]",
                engine.io_set.get_short_display(&**code)
            )
        } else {
            format!("eval ARG [ {} ]", engine.io_set.get_short_display(&**code))
        }
    } else {
        waiting_to_call!()
    }
}
