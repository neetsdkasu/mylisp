use crate::code::Code;
use crate::special_form::SpecialForm::Recur;

use super::super::CallStackState;
use super::super::RecurRequest;

use super::Engine;
use super::Error;
use super::SpecialFormRequest;

pub(super) fn call<R, W, E>(
    engine: &mut Engine<R, W, E>,
    mut request: Box<SpecialFormRequest<R, W, E>>,
) -> Result<Box<SpecialFormRequest<R, W, E>>, Error> {
    if let Some(code) = request.code.take() {
        if let Some(mut args) = request.temporary_codes.take() {
            if request.remaining_args.is_nil() {
                let symbol = args[0]
                    .get_symbol()
                    .ok_or_else(|| Error::wrap(bug!("in {:?}", Recur)))?;
                let recur_reqest = Box::new(RecurRequest {
                    symbol,
                    recur_code: code.clone(),
                });
                engine
                    .call_stack
                    .push(CallStackState::Recurring(recur_reqest));
                let code = Code::cons(code, args[1].clone());
                request.code_is_result = false;
                request.code = Some(code);
            } else {
                if !code.is_symbol() {
                    return Err(Error::illegal_argument_with_detail(
                        &engine.io_set.case.apply(Recur.symbol()),
                        format!(
                            "first argument must be symbol: [ {} ]",
                            engine.io_set.get_display(&*code)
                        ),
                    ));
                }
                args[0] = code;
                request.code_is_result = false;
                request.code = Some(args[2].clone());
                request.temporary_codes = Some(args);
                request.remaining_args = engine.constants.nil();
            }
        } else {
            request.code_is_result = true;
            request.code = Some(code);
        }
        return Ok(request);
    }

    let args = request
        .remaining_args
        .to_vec()
        .filter(|args| args.len() == 3)
        .ok_or_else(|| {
            Error::illegal_argument_with_detail(
                &engine.io_set.case.apply(Recur.symbol()),
                format!(
                    "invalid ARGS: [ {} ] (size must be 3)",
                    engine.io_set.get_display(&*request.remaining_args)
                ),
            )
        })?;

    request.code_is_result = false;
    request.code = Some(args[0].clone());
    request.temporary_codes = Some(args);
    Ok(request)
}

pub(super) fn trace<R, W, E>(
    engine: &Engine<R, W, E>,
    request: &SpecialFormRequest<R, W, E>,
) -> String {
    if let Some(ref code) = request.code {
        let code = engine.io_set.get_short_display(&**code);
        if let Some(ref args) = request.temporary_codes {
            if request.remaining_args.is_nil() {
                let symbol = engine.io_set.get_short_display(&*args[0]);
                format!("symbol: [ {} ], eval MAKE-LAMBDA [ {} ]", symbol, code)
            } else {
                format!("eval SYMBOL [ {} ]", code)
            }
        } else {
            format!("eval LAMBDA-FIRST-TIME [ {} ]", code)
        }
    } else {
        waiting_to_call!()
    }
}
