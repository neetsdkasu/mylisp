use std::rc::Rc;

use crate::code::Code;
use crate::special_form::SpecialForm::{Quasiquote, Unquote, UnquoteSplicing};

use super::Engine;
use super::Error;
use super::SpecialFormRequest;

#[derive(Debug)]
enum State {
    Top,
    Car,
    Cdr,
    Splicing,
}

impl State {
    fn trans_state(first: &Rc<Code>, second: &Rc<Code>) -> State {
        match (first.is_nil(), second.is_nil()) {
            (true, true) => State::Top,
            (true, false) => State::Car,
            (false, true) => State::Cdr,
            (false, false) => State::Splicing,
        }
    }

    fn set_to<R, W, E>(&self, engine: &Engine<R, W, E>, stack: &mut Vec<Rc<Code>>) {
        match self {
            State::Top => {
                stack.push(engine.constants.nil()); // second
                stack.push(engine.constants.nil()); // first
            }
            State::Car => {
                stack.push(engine.constants.t()); // second
                stack.push(engine.constants.nil()); // first
            }
            State::Cdr => {
                stack.push(engine.constants.nil()); // second
                stack.push(engine.constants.t()); // first
            }
            State::Splicing => {
                stack.push(engine.constants.t()); // second
                stack.push(engine.constants.t()); // first
            }
        }
    }
}

pub(super) fn call<R, W, E>(
    engine: &mut Engine<R, W, E>,
    mut request: Box<SpecialFormRequest<R, W, E>>,
) -> Result<Box<SpecialFormRequest<R, W, E>>, Error> {
    macro_rules! nil {
        () => {
            engine.constants.nil()
        };
    }
    macro_rules! non_nil {
        () => {
            engine.constants.t()
        };
    }

    let mut stack = match request.temporary_codes.take() {
        Some(stack) => stack,
        None => {
            let mut stack = vec![nil!()]; // eval_check
            State::Top.set_to(engine, &mut stack);
            let (car, _) = request
                .remaining_args
                .split()
                .filter(|(_, cdr)| cdr.is_nil())
                .ok_or_else(|| {
                    Error::illegal_argument_with_detail(
                        &engine.io_set.case.apply(Quasiquote.symbol()),
                        format!(
                            "invalid ARGS size: [ {} ] (must be 1)",
                            engine.io_set.get_display(&*request.remaining_args)
                        ),
                    )
                })?;
            stack.push(car.clone());
            stack
        }
    };

    if let Some(code) = request.code.take() {
        let splicing_check = stack
            .pop()
            .ok_or_else(|| Error::wrap(bug!("in {:?}", Quasiquote)))?;
        if splicing_check.is_nil() {
            // unquote
            stack.push(code);
        } else {
            // unquote-splicing
            if !code.is_list() {
                return Err(Error::illegal_argument_with_detail(
                    &format!(
                        "{} in {}",
                        engine.io_set.case.apply(UnquoteSplicing.symbol()),
                        engine.io_set.case.apply(Quasiquote.symbol())
                    ),
                    format!("ARG not list: [ {} ]", code),
                ));
            }
            let _first = stack
                .pop()
                .ok_or_else(|| Error::wrap(bug!("in {:?}", Quasiquote)))?;
            let _second = stack
                .pop()
                .ok_or_else(|| Error::wrap(bug!("in {:?}", Quasiquote)))?;
            let _eval_check = stack
                .pop()
                .ok_or_else(|| Error::wrap(bug!("in {:?}", Quasiquote)))?;
            let cdr = stack
                .pop()
                .ok_or_else(|| Error::wrap(bug!("in {:?}", Quasiquote)))?;
            stack.push(code);
            stack.push(nil!()); // eval_check
            State::Splicing.set_to(engine, &mut stack);
            stack.push(cdr);
        }
    }

    let mut quasiquote_count = 0;

    while let Some(code) = stack.pop() {
        let first = stack
            .pop()
            .ok_or_else(|| Error::wrap(bug!("in {:?}", Quasiquote)))?;
        let second = stack
            .pop()
            .ok_or_else(|| Error::wrap(bug!("in {:?}", Quasiquote)))?;
        let eval_check = stack
            .pop()
            .ok_or_else(|| Error::wrap(bug!("in {:?}", Quasiquote)))?;

        let state = State::trans_state(&first, &second);

        if !eval_check.is_nil() {
            match state {
                State::Top => {
                    request.code_is_result = true;
                    request.code = Some(code);
                    return Ok(request);
                }
                State::Car => {
                    let cdr = stack
                        .pop()
                        .ok_or_else(|| Error::wrap(bug!("in {:?}", Quasiquote)))?;
                    stack.push(code); // car
                    stack.push(nil!()); // eval_check
                    State::Cdr.set_to(engine, &mut stack);
                    stack.push(cdr);
                }
                State::Cdr => {
                    let car = stack
                        .pop()
                        .ok_or_else(|| Error::wrap(bug!("in {:?}", Quasiquote)))?;
                    let cons = Code::cons(car, code);
                    stack.push(cons);
                }
                State::Splicing => {
                    let mut cdr = code;
                    let splicing = stack
                        .pop()
                        .ok_or_else(|| Error::wrap(bug!("in {:?}", Quasiquote)))?;
                    let mut front = splicing
                        .to_vec()
                        .ok_or_else(|| Error::wrap(bug!("in {:?}", Quasiquote)))?;
                    while let Some(car) = front.pop() {
                        cdr = Code::cons(car, cdr);
                    }
                    stack.push(cdr);
                }
            }
            continue;
        }

        stack.push(non_nil!()); // eval_check
        stack.push(second); // second
        stack.push(first); // first

        match &*code {
            Code::Cons(ref cell) => {
                match cell.car.get_symbol() {
                    Some(ref s) if **s == Quasiquote.symbol() => quasiquote_count += 1,
                    Some(ref s) if **s == Unquote.symbol() => {
                        if quasiquote_count == 0 {
                            let (cadr, _) = cell
                                .cdr
                                .split()
                                .filter(|(_, rest)| rest.is_nil())
                                .ok_or_else(|| {
                                    Error::illegal_argument_with_detail(
                                        &format!(
                                            "{} in {}",
                                            engine.io_set.case.apply(Unquote.symbol()),
                                            engine.io_set.case.apply(Quasiquote.symbol())
                                        ),
                                        format!(
                                            "invalid ARG: [ {} ]",
                                            engine.io_set.get_display(&*cell.cdr)
                                        ),
                                    )
                                })?;
                            stack.push(nil!()); // unquote
                            request.code_is_result = false;
                            request.code = Some(cadr.clone());
                            request.temporary_codes = Some(stack);
                            return Ok(request);
                        }
                        quasiquote_count -= 1;
                    }
                    Some(ref s) if **s == UnquoteSplicing.symbol() => {
                        if quasiquote_count == 0 {
                            let (cadr, _) = cell
                                .cdr
                                .split()
                                .filter(|(_, rest)| rest.is_nil())
                                .ok_or_else(|| {
                                    Error::illegal_argument_with_detail(
                                        &format!(
                                            "{} in {}",
                                            engine.io_set.case.apply(UnquoteSplicing.symbol()),
                                            engine.io_set.case.apply(Quasiquote.symbol())
                                        ),
                                        format!(
                                            "invalid ARG: [ {} ]",
                                            engine.io_set.get_display(&*cell.cdr)
                                        ),
                                    )
                                })?;
                            if !matches!(state, State::Car) {
                                return Err(Error::illegal_argument_with_detail(
                                    &format!(
                                        "{} in {}",
                                        engine.io_set.case.apply(UnquoteSplicing.symbol()),
                                        engine.io_set.case.apply(Quasiquote.symbol())
                                    ),
                                    format!(
                                        "invalid place: [ {} ]",
                                        engine.io_set.get_display(&*code)
                                    ),
                                ));
                            }
                            stack.push(non_nil!()); // unquote-splicing
                            request.code_is_result = false;
                            request.code = Some(cadr.clone());
                            request.temporary_codes = Some(stack);
                            return Ok(request);
                        }
                        quasiquote_count -= 1;
                    }
                    _ => {}
                }
                stack.push(cell.cdr.clone());
                stack.push(nil!()); // eval_check
                State::Car.set_to(engine, &mut stack);
                stack.push(cell.car.clone());
            }
            _ => stack.push(code),
        }
    }

    Err(Error::wrap(bug!("in {:?}", Quasiquote)))
}

pub(super) fn trace<R, W, E>(
    engine: &Engine<R, W, E>,
    request: &SpecialFormRequest<R, W, E>,
) -> String {
    let arg = engine.io_set.get_short_display(&*request.remaining_args);
    if let Some(ref code) = request.code {
        let code = engine.io_set.get_short_display(&**code);
        if let Some(last) = request
            .temporary_codes
            .as_ref()
            .and_then(|stack| stack.last())
        {
            if last.is_nil() {
                format!(
                    "eval {} [ {} ], arg: [ {} ]",
                    Unquote.symbol().to_ascii_uppercase(),
                    code,
                    arg
                )
            } else {
                format!(
                    "eval {} [ {} ], arg: [ {} ]",
                    UnquoteSplicing.symbol().to_ascii_uppercase(),
                    code,
                    arg
                )
            }
        } else {
            bug!("unknown")
        }
    } else {
        waiting_to_call!()
    }
}
