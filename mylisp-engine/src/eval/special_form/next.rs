use crate::code::{Code, NextInfo};
use crate::special_form::SpecialForm::Next;

use super::super::CallStackState;

use super::Engine;
use super::Error;
use super::SpecialFormRequest;

pub(super) fn call<R, W, E>(
    engine: &mut Engine<R, W, E>,
    mut request: Box<SpecialFormRequest<R, W, E>>,
) -> Result<Box<SpecialFormRequest<R, W, E>>, Error> {
    let mut args = request.temporary_codes.take().unwrap_or_default();

    if let Some(code) = request.code.take() {
        if args.is_empty() {
            // if !is_valid(engine, &code) {
            if !code.is_symbol() {
                return Err(Error::illegal_argument_with_detail(
                    &engine.io_set.case.apply(Next.symbol()),
                    format!(
                        "first arg must be SYMBOL: [ {} ]",
                        engine.io_set.get_display(&*request.remaining_args)
                    ),
                ));
            }
        }
        args.push(code);
    }

    match &*request.remaining_args {
        Code::Nil => {
            if args.is_empty() {
                return Err(Error::illegal_argument_with_detail(
                    &engine.io_set.case.apply(Next.symbol()),
                    format!(
                        "first arg must be SYMBOL: [ {} ]",
                        engine.io_set.get_display(&*request.remaining_args)
                    ),
                ));
            }
            let mut code = engine.constants.nil();
            while let Some(car) = args.pop() {
                code = Code::cons(car, code);
            }
            let (symbol, args) = code
                .split()
                .ok_or_else(|| Error::wrap(bug!("in {:?}", Next)))?;
            let symbol = symbol
                .get_symbol()
                .ok_or_else(|| Error::wrap(bug!("in {:?}", Next)))?;
            let info = NextInfo {
                symbol,
                args: args.clone(),
            };
            let code = Code::Next(Box::new(info)).rc();
            request.code_is_result = true;
            request.code = Some(code);
            Ok(request)
        }
        Code::Cons(ref cell) => {
            let code = cell.car.clone();
            request.remaining_args = cell.cdr.clone();
            request.code_is_result = false;
            request.code = Some(code);
            request.temporary_codes = Some(args);
            Ok(request)
        }
        _ => Err(Error::illegal_argument_with_detail(
            &engine.io_set.case.apply(Next.symbol()),
            format!(
                "ARGS must be list: ({}) [ {} ]",
                args.len(),
                engine.io_set.get_display(&*request.remaining_args)
            ),
        )),
    }
}

#[allow(dead_code)]
fn is_valid<R, W, E>(engine: &Engine<R, W, E>, code: &Code) -> bool {
    let symbol = match code.get_symbol() {
        Some(symbol) => symbol,
        None => return false,
    };
    engine.call_stack.iter().rev().any(|state| {
        if let CallStackState::Recurring(ref recur_request) = state {
            recur_request.symbol == symbol
        } else {
            false
        }
    })
}

pub(super) fn trace<R, W, E>(
    engine: &Engine<R, W, E>,
    request: &SpecialFormRequest<R, W, E>,
) -> String {
    let remaining_count = request.remaining_args.list_depth().unwrap_or(0);
    let remaining_args = engine.io_set.get_short_display(&*request.remaining_args);
    if let Some(ref code) = request.code {
        let code = engine.io_set.get_short_display(&**code);
        if let Some(ref codes) = request.temporary_codes {
            if codes.is_empty() {
                format!(
                    "eval SYMBOL [ {} ], remain_args: ({}) [ {} ]",
                    code, remaining_count, remaining_args
                )
            } else {
                let symbol = engine.io_set.get_short_display(&*codes[0]);
                let pos = codes.len() - 1;
                format!(
                    "symbol: [ {} ], eval ARG ({}) [ {} ], remain_args: ({}) [ {} ]",
                    symbol, pos, code, remaining_count, remaining_args
                )
            }
        } else {
            bug!("unknown")
        }
    } else {
        waiting_to_call!()
    }
}
