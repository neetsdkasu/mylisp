use crate::special_form::SpecialForm::Unquote;

use super::Engine;
use super::Error;
use super::SpecialFormRequest;

#[allow(clippy::boxed_local)]
pub(super) fn call<R, W, E>(
    engine: &mut Engine<R, W, E>,
    _request: Box<SpecialFormRequest<R, W, E>>,
) -> Result<Box<SpecialFormRequest<R, W, E>>, Error> {
    Err(Error::with_cause(
        &engine.io_set.case.apply(Unquote.symbol()),
        "cannot evaluate",
    ))
}

pub(super) fn trace<R, W, E>(
    _engine: &Engine<R, W, E>,
    _request: &SpecialFormRequest<R, W, E>,
) -> String {
    waiting_to_call!()
}
