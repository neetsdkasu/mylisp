use crate::code::Code;
use crate::special_form::SpecialForm::Define;

use super::Engine;
use super::Error;
use super::SpecialFormRequest;

pub(super) fn call<R, W, E>(
    engine: &mut Engine<R, W, E>,
    mut request: Box<SpecialFormRequest<R, W, E>>,
) -> Result<Box<SpecialFormRequest<R, W, E>>, Error> {
    if let Some(code) = request.code.take() {
        let symbol_code = request
            .temporary_codes
            .take()
            .filter(|codes| codes.len() == 1)
            .and_then(|mut codes| codes.pop())
            .ok_or_else(|| Error::wrap(bug!("in {:?}", Define)))?;
        let symbol = match &*symbol_code {
            Code::Symbol(ref symbol) => symbol.clone(),
            _ => return Err(Error::wrap(bug!("in {:?} : {}", Define, symbol_code))),
        };
        engine.bind(symbol, code)?;
        request.code_is_result = true;
        request.code = Some(symbol_code);
    } else {
        if !engine.top_level {
            return Err(Error::with_cause(
                &engine.io_set.case.apply(Define.symbol()),
                "require top-level",
            ));
        }
        let (args, _) = request
            .remaining_args
            .split_at(2)
            .filter(|(_, rest)| rest.is_nil())
            .ok_or_else(|| {
                Error::illegal_argument_with_detail(
                    &engine.io_set.case.apply(Define.symbol()),
                    format!(
                        "invalid ARGS: [ {} ]",
                        engine.io_set.get_display(&request.remaining_args)
                    ),
                )
            })?;
        let symbol_code = args[0].clone();
        let code = args[1].clone();
        if !matches!(&*symbol_code, Code::Symbol(_)) {
            return Err(Error::illegal_argument_with_detail(
                &engine.io_set.case.apply(Define.symbol()),
                format!(
                    "invalid SYMBOL: [ {} ] (must be symbol)",
                    engine.io_set.get_short_display(&*symbol_code)
                ),
            ));
        }
        request.temporary_codes = Some(vec![symbol_code]);
        request.code_is_result = false;
        request.code = Some(code);
    }
    Ok(request)
}

pub(super) fn trace<R, W, E>(
    engine: &Engine<R, W, E>,
    request: &SpecialFormRequest<R, W, E>,
) -> String {
    if let Some(ref code) = request.code {
        if let Some(symbol) = request
            .temporary_codes
            .as_ref()
            .and_then(|codes| codes.first())
        {
            let code = engine.io_set.get_short_display(&**code);
            let symbol = engine.io_set.get_short_display(&**symbol);
            format!("symbol: [ {} ], eval VALUE [ {} ]", symbol, code)
        } else {
            bug!("unknown")
        }
    } else {
        waiting_to_call!()
    }
}
