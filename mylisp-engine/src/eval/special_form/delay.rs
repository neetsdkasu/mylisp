use crate::code::Code;
use crate::special_form::SpecialForm::Delay;

use super::Engine;
use super::Error;
use super::SpecialFormRequest;

#[allow(clippy::boxed_local)]
pub(super) fn call<R, W, E>(
    engine: &mut Engine<R, W, E>,
    mut request: Box<SpecialFormRequest<R, W, E>>,
) -> Result<Box<SpecialFormRequest<R, W, E>>, Error> {
    let (code, _) = request
        .remaining_args
        .split()
        .filter(|(_, rest)| rest.is_nil())
        .ok_or_else(|| {
            Error::illegal_argument_with_detail(
                &engine.io_set.case.apply(Delay.symbol()),
                format!(
                    "invalid ARG: [ {} ] (must be 1 argument)",
                    engine.io_set.get_display(&*request.remaining_args)
                ),
            )
        })?;

    let bindings = engine.get_scope_bindings();

    request.code_is_result = true;
    request.code = Some(Code::make_delayed(code.clone(), bindings));
    Ok(request)
}

pub(super) fn trace<R, W, E>(
    _engine: &Engine<R, W, E>,
    _request: &SpecialFormRequest<R, W, E>,
) -> String {
    waiting_to_call!()
}
