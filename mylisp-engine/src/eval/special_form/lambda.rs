use std::rc::Rc;

use crate::code::{ClosureInfo, Code, LambdaInfo};
use crate::special_form::SpecialForm::Lambda;

use super::Engine;
use super::Error;
use super::SpecialFormRequest;
use super::MAX_ARG_SIZE;

pub(super) fn call<R, W, E>(
    engine: &mut Engine<R, W, E>,
    mut request: Box<SpecialFormRequest<R, W, E>>,
) -> Result<Box<SpecialFormRequest<R, W, E>>, Error> {
    let (body, args) = request
        .remaining_args
        .split_at(2)
        .filter(|(_, rest)| rest.is_nil())
        .map(|(mut codes, _)| (codes.pop(), codes.pop()))
        .ok_or_else(|| {
            Error::illegal_argument_with_detail(
                &engine.io_set.case.apply(Lambda.symbol()),
                format!(
                    "invalid ARGS: [ {} ]",
                    engine.io_set.get_display(&*request.remaining_args)
                ),
            )
        })?;

    let args = args.ok_or_else(|| Error::wrap(bug!("in Code::split_at")))?;
    let body = body.ok_or_else(|| Error::wrap(bug!("in Code::split_at")))?;

    let mut cur = args;
    let mut arg_min_size = 0;
    let arg_max_size;
    loop {
        match &**cur {
            Code::Nil => {
                arg_max_size = arg_min_size;
                break;
            }
            Code::Cons(ref cell) => {
                if !contains_all_symbols(&cell.car) {
                    return Err(Error::illegal_argument_with_detail(
                        &engine.io_set.case.apply(Lambda.symbol()),
                        format!(
                            "invalid LAMBDA-ARG: ({}) [ {} ]",
                            arg_min_size,
                            engine.io_set.get_display(&**cur)
                        ),
                    ));
                }
                arg_min_size += 1;
                cur = &cell.cdr;
            }
            Code::Symbol(_) => {
                arg_max_size = MAX_ARG_SIZE;
                break;
            }
            _ => {
                return Err(Error::illegal_argument_with_detail(
                    &engine.io_set.case.apply(Lambda.symbol()),
                    format!(
                        "invalid LAMBDA-ARG: ({}) [ {} ]",
                        arg_min_size,
                        engine.io_set.get_display(&**cur)
                    ),
                ))
            }
        }
    }

    if arg_min_size > MAX_ARG_SIZE {
        return Err(Error::illegal_argument_with_detail(
            &engine.io_set.case.apply(Lambda.symbol()),
            format!(
                concat!(
                    "too many LAMBDA-ARGS: ({}) [ {} ]",
                    " (argument list max size is {})"
                ),
                arg_min_size,
                engine.io_set.get_display(&**args),
                MAX_ARG_SIZE
            ),
        ));
    }

    let info = LambdaInfo {
        id: engine.new_lambda_id(),
        arg_min_size,
        arg_max_size,
        args: args.clone(),
        body: body.clone(),
    };

    let code = if engine.lexical {
        let scope = engine.get_scope_bindings();
        Code::Closure(Rc::new(ClosureInfo {
            lambda_info: info,
            scope: Rc::new(scope),
        }))
    } else {
        Code::Lambda(Rc::new(info))
    };

    request.code_is_result = true;
    request.code = Some(code.rc());

    Ok(request)
}

fn contains_all_symbols(code: &Rc<Code>) -> bool {
    let mut codes = vec![code];
    while let Some(code) = codes.pop() {
        match &**code {
            Code::Cons(ref cell) => {
                codes.push(&cell.car);
                if !cell.cdr.is_nil() {
                    codes.push(&cell.cdr);
                }
            }
            Code::Symbol(_) => {}
            _ => return false,
        }
    }
    true
}

pub(super) fn trace<R, W, E>(
    _engine: &Engine<R, W, E>,
    _request: &SpecialFormRequest<R, W, E>,
) -> String {
    waiting_to_call!()
}
