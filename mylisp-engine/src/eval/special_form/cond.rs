use crate::code::Code;
use crate::special_form::SpecialForm::Cond;

use super::Engine;
use super::Error;
use super::SpecialFormRequest;
use super::MAX_ARG_SIZE;

pub(super) fn call<R, W, E>(
    engine: &mut Engine<R, W, E>,
    mut request: Box<SpecialFormRequest<R, W, E>>,
) -> Result<Box<SpecialFormRequest<R, W, E>>, Error> {
    if let Some(code) = request.code.take() {
        if let Some(mut rest) = request.temporary_codes.take() {
            if rest.len() != 1 {
                return Err(Error::wrap(bug!("in {:?}", Cond)));
            }
            let rest_code = rest
                .pop()
                .ok_or_else(|| Error::wrap(bug!("in {:?}", Cond)))?;
            if !code.is_nil() {
                request.code_is_result = false;
                request.code = Some(rest_code);
                return Ok(request);
            }
        } else {
            request.code_is_result = true;
            request.code = Some(code);
            return Ok(request);
        }
    } else {
        let mut args = &request.remaining_args;
        let mut arg_pos = 0;
        loop {
            match &**args {
                Code::Nil => break,
                Code::Cons(ref cell) => {
                    if !matches!(cell.car.list_depth(), Some(2)) {
                        return Err(Error::illegal_argument_with_detail(
                            &engine.io_set.case.apply(Cond.symbol()),
                            format!(
                                "invalid ARG: ({}) [ {} ] (must be size 2 list)",
                                arg_pos,
                                engine.io_set.get_display(&cell.car)
                            ),
                        ));
                    }
                    arg_pos += 1;
                    args = &cell.cdr;
                }
                _ => {
                    return Err(Error::illegal_argument_with_detail(
                        &engine.io_set.case.apply(Cond.symbol()),
                        format!(
                            "invalid ARGS not list: ({}) [ {} ]",
                            arg_pos,
                            engine.io_set.get_short_display(&**args)
                        ),
                    ))
                }
            }
        }
        if arg_pos > MAX_ARG_SIZE {
            return Err(Error::illegal_argument_with_detail(
                &engine.io_set.case.apply(Cond.symbol()),
                format!(
                    concat!(
                        "too many COND-ARGS: ({}) [ {} ]",
                        " (argument list max size is {})"
                    ),
                    arg_pos,
                    engine.io_set.get_display(&*request.remaining_args),
                    MAX_ARG_SIZE
                ),
            ));
        }
    }
    let cell = match &*request.remaining_args {
        Code::Nil => {
            request.code_is_result = true;
            request.code = Some(engine.constants.nil());
            return Ok(request);
        }
        Code::Cons(ref cell) => cell,
        _ => return Err(Error::wrap(bug!("in {:?}", Cond))),
    };
    let (codes, _) = cell
        .car
        .split_at(2)
        .ok_or_else(|| Error::wrap(bug!("WHY?")))?;

    request.code_is_result = false;
    request.code = Some(codes[0].clone());
    request.temporary_codes = Some(vec![codes[1].clone()]);
    request.remaining_args = cell.cdr.clone();
    Ok(request)
}

pub(super) fn trace<R, W, E>(
    engine: &Engine<R, W, E>,
    request: &SpecialFormRequest<R, W, E>,
) -> String {
    let remaining_count = request.remaining_args.list_depth().unwrap_or(0);
    let remaining_args = engine.io_set.get_short_display(&*request.remaining_args);
    if let Some(ref code) = request.code {
        let code = engine.io_set.get_short_display(&**code);
        if request.temporary_codes.is_some() {
            format!(
                "eval CONDITION [ {} ], remain_args: ({}) [ {} ]",
                code, remaining_count, remaining_args
            )
        } else {
            format!(
                "eval RETURN [ {} ], remain_args: ({}) [ {} ]",
                code, remaining_count, remaining_args
            )
        }
    } else {
        waiting_to_call!()
    }
}
