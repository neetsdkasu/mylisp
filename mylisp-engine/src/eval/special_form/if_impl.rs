use std::rc::Rc;

use crate::code::Code;
use crate::special_form::SpecialForm::If;

use super::Engine;
use super::Error;
use super::SpecialFormRequest;

pub(super) fn call<R, W, E>(
    engine: &mut Engine<R, W, E>,
    mut request: Box<SpecialFormRequest<R, W, E>>,
) -> Result<Box<SpecialFormRequest<R, W, E>>, Error> {
    if let Some(code) = request.code.take() {
        if let Some(args) = request.temporary_codes.take() {
            match &*code {
                Code::True => {
                    request.code_is_result = false;
                    request.code = Some(args[1].clone());
                    Ok(request)
                }
                Code::False => {
                    if let Some(else_code) = args.get(2) {
                        request.code_is_result = false;
                        request.code = Some(else_code.clone());
                    } else {
                        request.code_is_result = true;
                        request.code = Some(engine.constants.nil());
                    }
                    Ok(request)
                }
                _ => Err(Error::with_cause(
                    &engine.io_set.case.apply(If.symbol()),
                    &format!(
                        "CONDITION value must be boolean(True/False): [ {} ]",
                        engine.io_set.get_short_display(&*args[0])
                    ),
                )),
            }
        } else {
            request.code_is_result = true;
            request.code = Some(code);
            Ok(request)
        }
    } else {
        let args = request
            .remaining_args
            .to_vec()
            .filter(|args| (2..=3).contains(&args.len()))
            .ok_or_else(|| {
                Error::illegal_argument_with_detail(
                    &engine.io_set.case.apply(If.symbol()),
                    format!(
                        "invalid ARGS: [ {} ]",
                        engine.io_set.get_display(&*request.remaining_args)
                    ),
                )
            })?;
        request.code_is_result = false;
        request.code = Some(args[0].clone());
        request.temporary_codes = Some(args.iter().map(Rc::clone).collect());
        Ok(request)
    }
}

pub(super) fn trace<R, W, E>(
    engine: &Engine<R, W, E>,
    request: &SpecialFormRequest<R, W, E>,
) -> String {
    if let Some(ref code) = request.code {
        let code_str = engine.io_set.get_short_display(&**code);
        if request.temporary_codes.is_some() {
            format!("eval CONDITION [ {} ]", code_str)
        } else if let Some(args) = request.remaining_args.to_vec() {
            if Rc::ptr_eq(code, &args[1]) {
                format!("eval TRUE-VALUE [ {} ]", code_str)
            } else {
                format!("eval FALSE-VALUE [ {} ]", code_str)
            }
        } else {
            bug!("unknown")
        }
    } else {
        waiting_to_call!()
    }
}
