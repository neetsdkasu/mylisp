use crate::code::{self, ClosureInfo, Code, DelayedValue, LambdaInfo};

use super::CallStackState;
use super::Constants;
use super::Engine;
use super::IOSet;

const DEFAULT_STACK_TRACE_SIZE: usize = 15;
const SHORT_DISPLAY_SIZE: usize = 30;

pub(super) trait ShortDisplay {
    fn short_display(&self, case: code::Case, syntax: code::Syntax) -> String;
}

fn short_string(mut s: String) -> String {
    if s.len() > SHORT_DISPLAY_SIZE {
        s = s.chars().take(SHORT_DISPLAY_SIZE - 4).collect();
        s += " ...";
    }
    s
}

impl<R, W, E> IOSet<R, W, E> {
    pub(super) fn get_short_display<T: ShortDisplay>(&self, target: &T) -> String {
        target.short_display(self.case, self.syntax)
    }
}

impl Constants {
    pub(super) fn get_short_display<T: ShortDisplay>(&self, target: &T) -> String {
        target.short_display(self.case, self.syntax)
    }
}

impl<R, W, E> Engine<R, W, E> {
    pub fn get_stack_trace(&self) -> Vec<String> {
        self.get_stack_trace_with_size(DEFAULT_STACK_TRACE_SIZE)
    }

    #[allow(dead_code)]
    pub fn get_all_stack_trace(&self) -> Vec<String> {
        self.get_stack_trace_with_size(self.call_stack.len())
    }

    pub fn get_stack_trace_with_size(&self, size: usize) -> Vec<String> {
        let case = self.io_set.case;
        let mut ret = vec![];
        for (i, state) in self.call_stack.iter().enumerate().rev() {
            use CallStackState::*;
            let s = match state {
                ResolveFuncName(ref code) => {
                    format!("EvaluatingFuncName{{ {} }}", self.io_set.get_display(code))
                }
                ResolveArgsForBuiltinFunc(ref req) => format!(
                    "Evaluating Args{{ func: {}, arg_pos: {} }}",
                    case.apply(req.info.func_type().symbol()),
                    req.evaluated_args.len()
                ),
                ResolveArgsForIOFunc(ref req) => format!(
                    "Evaluating Args{{ func: {}, arg_pos: {} }}",
                    case.apply(req.info.func_type().symbol()),
                    req.evaluated_args.len()
                ),
                ResolveSpecialForm(ref req) => {
                    let call = req.info.get_call_trace();
                    let trace = call(self, req);
                    format!(
                        "Evaluating Args{{ func: {}, info: {} }}",
                        case.apply(req.info.form_type().symbol()),
                        trace
                    )
                }
                ResolveArgsForLambda(ref req) => format!(
                    "Evaluating Args{{ func: {}, arg_pos: {} }}",
                    self.io_set.get_short_display(&*req.info),
                    req.evaluated_args.len()
                ),
                ResolveArgsForClosure(ref req) => format!(
                    "Evaluating Args{{ func: {}, arg_pos: {} }}",
                    self.io_set.get_short_display(&*req.info),
                    req.evaluated_args.len()
                ),
                ResolveArgsForInjectFunc(ref req) => format!(
                    "Evaluating Args{{ func: {:?}, arg_pos: {} }}",
                    req.info,
                    req.evaluated_args.len()
                ),
                Recurring(ref req) => format!(
                    "Recurring{{ symbol: {}, code: [ {} ] }}",
                    case.apply(&**req.symbol),
                    self.io_set.get_short_display(&*req.recur_code)
                ),
                MacroEnd(ref name) => format!("Call Macro{{ {} }}", name),
                RemoveScope(ref opt_code) => {
                    if let Some(ref code) = opt_code {
                        format!("Call{{ {} }}", self.io_set.get_short_display(&**code))
                    } else {
                        continue;
                    }
                }
            };
            ret.push(format!("{}: {}", i, s));
            if ret.len() >= size {
                let rem = self.call_stack.len() - size;
                if rem > 0 {
                    ret.push(format!(" ... more {}", rem));
                }
                break;
            }
        }
        ret
    }
}

macro_rules! short_display_lambda_info {
    ($case:expr, $syntax:expr, $type_name:literal, $body_title: literal, $id:expr, $args:expr, $body:expr) => {{
        let args = $args.short_display($case, $syntax);
        let body = $body.short_display($case, $syntax);
        format!(
            concat!(
                $type_name,
                "{{ id: {}, args: [ {} ], ",
                $body_title,
                ": [ {} ] }}"
            ),
            $id, args, body
        )
    }};
}

impl ShortDisplay for LambdaInfo {
    fn short_display(&self, case: code::Case, syntax: code::Syntax) -> String {
        short_display_lambda_info!(case, syntax, "Lambda", "body", self.id, self.args, self.body)
    }
}

impl ShortDisplay for ClosureInfo {
    fn short_display(&self, case: code::Case, syntax: code::Syntax) -> String {
        short_display_lambda_info!(
            case,
            syntax,
            "Closure",
            "body",
            self.lambda_info.id,
            self.lambda_info.args,
            self.lambda_info.body
        )
    }
}

impl ShortDisplay for DelayedValue {
    fn short_display(&self, case: code::Case, syntax: code::Syntax) -> String {
        match self {
            DelayedValue::UnEvaluated(ref code, _) => format!(
                "Delayed{{ UnEvaluated: {} }}",
                code.short_display(case, syntax)
            ),
            DelayedValue::Evaluating => "Delayed{ Evaluating }".to_string(),
            DelayedValue::Evaluated(ref code) => format!(
                "Delayed{{ Evaluated: {} }}",
                code.short_display(case, syntax)
            ),
        }
    }
}

impl ShortDisplay for Code {
    fn short_display(&self, case: code::Case, syntax: code::Syntax) -> String {
        match self {
            Code::Lambda(ref info) => info.short_display(case, syntax),
            Code::Closure(ref info) => info.short_display(case, syntax),
            Code::Macro(ref info) => short_display_lambda_info!(
                case,
                syntax,
                "Macro",
                "generator",
                info.id,
                info.args,
                info.body
            ),
            Code::MacroWith(ref info) => short_display_lambda_info!(
                case,
                syntax,
                "Macro",
                "generator",
                info.lambda_info.id,
                info.lambda_info.args,
                info.lambda_info.body
            ),
            Code::Next(ref info) => format!(
                "Next{{ symbol: {}, args: {} }}",
                short_string(case.apply(&*info.symbol)),
                info.args.short_display(case, syntax)
            ),
            Code::Delayed(ref dv) => dv.borrow().short_display(case, syntax),
            _ => short_string(self.display(case, syntax).to_string()),
        }
    }
}
