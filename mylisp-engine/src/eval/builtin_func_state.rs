use std::rc::Rc;
use std::result;

use crate::code::{Code, ConsCell};

use super::BuiltinFuncRequest;
use super::CallStackState;
use super::Constants;
use super::Error;

type Result<R, W, E> = result::Result<(Rc<Code>, Option<CallStackState<R, W, E>>), Error>;

pub(super) fn get<R, W, E>(
    constants: &Constants,
    mut request: Box<BuiltinFuncRequest>,
) -> Result<R, W, E> {
    match &*request.remaining_args {
        Code::Cons(ref cell) => {
            let ConsCell { ref car, ref cdr } = &**cell;
            let code = car.clone();
            request.remaining_args = cdr.clone();
            let new_state = CallStackState::ResolveArgsForBuiltinFunc(request);
            Ok((code, Some(new_state)))
        }
        Code::Nil => {
            let BuiltinFuncRequest {
                info,
                evaluated_args,
                remaining_args: _,
            } = *request;
            let call = info.get_call_back();
            let new_code = call(constants, evaluated_args)?;
            Ok((new_code, None))
        }
        _ => Err(Error::wrap(bug!("not list: {}", request.remaining_args))),
    }
}
