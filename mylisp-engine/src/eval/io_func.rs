use std::io;

use crate::code::Code;
use crate::io_func::IOFunc;

use super::ArgList;
use super::Constants;
use super::Error;
use super::IOSet;
use super::Result;

mod eprintln;
mod println;
mod raise_error;
mod readln;

const MAX_ARG_SIZE: usize = 100;

type CallBack<R, W, E> =
    fn(io_set: &mut IOSet<R, W, E>, constants: &Constants, args: ArgList) -> Result;

pub(super) struct IOFuncInfo<R, W, E> {
    func_type: IOFunc,
    arg_min_size: usize,
    arg_max_size: usize,
    call_back: CallBack<R, W, E>,
}

impl<R, W, E> IOFuncInfo<R, W, E> {
    pub(super) fn func_type(&self) -> IOFunc {
        self.func_type
    }

    pub(super) fn get_call_back(&self) -> CallBack<R, W, E> {
        self.call_back
    }

    pub(super) fn validate_args_count(&self, args: &Code) -> Option<bool> {
        args.list_depth()
            .map(|depth| self.arg_min_size <= depth && depth <= self.arg_max_size)
    }
}

pub(super) fn get_io_func_info<R, W, E>(func: IOFunc) -> IOFuncInfo<R, W, E>
where
    R: io::BufRead,
    W: io::Write,
    E: io::Write,
{
    use IOFunc::*;
    match func {
        Println => IOFuncInfo {
            func_type: Println,
            arg_min_size: 0,
            arg_max_size: MAX_ARG_SIZE,
            call_back: println::func,
        },
        Readln => IOFuncInfo {
            func_type: Readln,
            arg_min_size: 0,
            arg_max_size: 0,
            call_back: readln::func,
        },
        Eprintln => IOFuncInfo {
            func_type: Eprintln,
            arg_min_size: 0,
            arg_max_size: MAX_ARG_SIZE,
            call_back: eprintln::func,
        },
        RaiseError => IOFuncInfo {
            func_type: RaiseError,
            arg_min_size: 0,
            arg_max_size: MAX_ARG_SIZE,
            call_back: raise_error::func,
        },
    }
}
