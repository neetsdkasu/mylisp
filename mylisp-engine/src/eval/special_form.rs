use std::rc::Rc;

use crate::code::Code;
use crate::special_form::SpecialForm;

use super::Engine;
use super::Environment;
use super::Error;
use super::SpecialFormRequest;
use super::MAX_ARG_SIZE;

macro_rules! waiting_to_call {
    () => {
        "waiting to call".to_string()
    };
}

mod cond;
mod define;
mod delay;
mod delay_get;
mod if_impl;
mod lambda;
mod macro_impl;
mod next;
mod quasiquote;
mod quote;
mod recur;
mod unquote;
mod unquote_splicing;

type CallBack<R, W, E> = fn(
    engine: &mut Engine<R, W, E>,
    request: Box<SpecialFormRequest<R, W, E>>,
) -> Result<Box<SpecialFormRequest<R, W, E>>, Error>;

type CallTrace<R, W, E> =
    fn(engine: &Engine<R, W, E>, request: &SpecialFormRequest<R, W, E>) -> String;

pub(super) struct SpecialFormInfo<R, W, E> {
    form_type: SpecialForm,
    call_back: CallBack<R, W, E>,
    call_trace: CallTrace<R, W, E>,
}

impl<R, W, E> SpecialFormInfo<R, W, E> {
    pub(super) fn form_type(&self) -> SpecialForm {
        self.form_type
    }

    pub(super) fn get_call_back(&self) -> CallBack<R, W, E> {
        self.call_back
    }

    pub(super) fn get_call_trace(&self) -> CallTrace<R, W, E> {
        self.call_trace
    }

    pub(super) fn make_request(self, args: Rc<Code>) -> Box<SpecialFormRequest<R, W, E>> {
        Box::new(SpecialFormRequest {
            info: self,
            code_is_result: false,
            code: None,
            temporary_codes: None,
            remaining_args: args,
        })
    }
}

pub(super) fn get_special_form_info<R, W, E>(form: SpecialForm) -> SpecialFormInfo<R, W, E> {
    use SpecialForm::*;
    match form {
        Cond => SpecialFormInfo {
            form_type: Cond,
            call_back: cond::call,
            call_trace: cond::trace,
        },
        Define => SpecialFormInfo {
            form_type: Define,
            call_back: define::call,
            call_trace: define::trace,
        },
        Lambda => SpecialFormInfo {
            form_type: Lambda,
            call_back: lambda::call,
            call_trace: lambda::trace,
        },
        Quote => SpecialFormInfo {
            form_type: Quote,
            call_back: quote::call,
            call_trace: quote::trace,
        },
        Quasiquote => SpecialFormInfo {
            form_type: Quasiquote,
            call_back: quasiquote::call,
            call_trace: quasiquote::trace,
        },
        Unquote => SpecialFormInfo {
            form_type: Unquote,
            call_back: unquote::call,
            call_trace: unquote::trace,
        },
        UnquoteSplicing => SpecialFormInfo {
            form_type: UnquoteSplicing,
            call_back: unquote_splicing::call,
            call_trace: unquote_splicing::trace,
        },
        Macro => SpecialFormInfo {
            form_type: Macro,
            call_back: macro_impl::call,
            call_trace: macro_impl::trace,
        },
        If => SpecialFormInfo {
            form_type: If,
            call_back: if_impl::call,
            call_trace: if_impl::trace,
        },
        Recur => SpecialFormInfo {
            form_type: Recur,
            call_back: recur::call,
            call_trace: recur::trace,
        },
        Next => SpecialFormInfo {
            form_type: Next,
            call_back: next::call,
            call_trace: next::trace,
        },
        Delay => SpecialFormInfo {
            form_type: Delay,
            call_back: delay::call,
            call_trace: delay::trace,
        },
        DelayGet => SpecialFormInfo {
            form_type: DelayGet,
            call_back: delay_get::call,
            call_trace: delay_get::trace,
        },
    }
}
