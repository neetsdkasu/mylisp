use crate::builtin_func::BuiltinFunc::BitwiseNot;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Error;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    match &*args[0] {
        Code::Int32(v) => Ok(Code::Int32(!*v).rc()),
        Code::Int64(ref v) => Ok(Code::Int64(Box::new(!**v)).rc()),
        Code::Float64(ref v) => {
            let w = **v as i32;
            if (w as f64 - **v).abs() <= f64::EPSILON {
                return Ok(Code::Int32(!w).rc());
            }
            let w = **v as i64;
            if (w as f64 - **v).abs() <= f64::EPSILON {
                Ok(Code::Int64(Box::new(!w)).rc())
            } else {
                Err(Error::illegal_argument_with_detail(
                    &constants.case.apply(BitwiseNot.symbol()),
                    format!(
                        "wrong value: [ {} ]",
                        constants.get_short_display(&*args[0])
                    ),
                ))
            }
        }
        _ => Err(Error::illegal_argument_with_detail(
            &constants.case.apply(BitwiseNot.symbol()),
            format!("not number: [ {} ]", constants.get_short_display(&*args[0])),
        )),
    }
}
