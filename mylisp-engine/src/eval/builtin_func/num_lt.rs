use crate::builtin_func::BuiltinFunc::NumLT;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Error;
use super::Result;

macro_rules! illegal_argument {
    ($constants:expr, $i:expr, $arg:expr) => {
        Error::illegal_argument_with_detail(
            &$constants.case.apply(NumLT.symbol()),
            format!(
                "not number: ({}) [ {} ]",
                $i,
                $constants.get_short_display(&*$arg)
            ),
        )
    };
}

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    let mut tmp: i32 = match &*args[0] {
        Code::Int32(v) => *v,
        Code::Int64(ref v) => return lt_int64(constants, **v, 1, args),
        Code::Float64(ref v) => return lt_float64(constants, **v, 1, args),
        _ => return Err(illegal_argument!(constants, 0, args[0])),
    };
    for (i, arg) in args.iter().enumerate().skip(1) {
        match &**arg {
            Code::Int32(v) => {
                if tmp >= *v {
                    return Ok(constants.val_false());
                }
                tmp = *v;
            }
            Code::Int64(_) => return lt_int64(constants, tmp as i64, i, args),
            Code::Float64(_) => return lt_float64(constants, tmp as f64, i, args),
            _ => return Err(illegal_argument!(constants, i, *arg)),
        }
    }
    Ok(constants.val_true())
}

fn lt_int64(constants: &Constants, mut tmp: i64, start: usize, args: ArgList) -> Result {
    for (i, arg) in args.iter().enumerate().skip(start) {
        match &**arg {
            Code::Int32(v) => {
                if tmp >= (*v as i64) {
                    return Ok(constants.val_false());
                }
                tmp = *v as i64;
            }
            Code::Int64(ref v) => {
                if tmp >= **v {
                    return Ok(constants.val_false());
                }
                tmp = **v;
            }
            Code::Float64(_) => return lt_float64(constants, tmp as f64, i, args),
            _ => return Err(illegal_argument!(constants, i, *arg)),
        }
    }
    Ok(constants.val_true())
}

fn lt_float64(constants: &Constants, mut tmp: f64, start: usize, args: ArgList) -> Result {
    for (i, arg) in args.iter().enumerate().skip(start) {
        match &**arg {
            Code::Int32(v) => {
                if tmp >= (*v as f64) {
                    return Ok(constants.val_false());
                }
                tmp = *v as f64;
            }
            Code::Int64(ref v) => {
                if tmp >= (**v as f64) {
                    return Ok(constants.val_false());
                }
                tmp = **v as f64;
            }
            Code::Float64(ref v) => {
                if tmp >= **v {
                    return Ok(constants.val_false());
                }
                tmp = **v;
            }
            _ => return Err(illegal_argument!(constants, i, *arg)),
        }
    }
    Ok(constants.val_true())
}
