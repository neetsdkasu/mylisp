use crate::builtin_func::BuiltinFunc::StrConcat;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Error;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    macro_rules! illegal_argument {
        ($cause:literal, $i:expr, $arg:expr) => {
            Error::illegal_argument_with_detail(
                &constants.case.apply(StrConcat.symbol()),
                format!(
                    concat!($cause, ": ({}) [ {} ]"),
                    $i,
                    constants.get_short_display(&**$arg)
                ),
            )
        };
    }

    let mut buf = String::new();
    for (i, arg) in args.iter().enumerate() {
        match &**arg {
            Code::Cons(_) => {
                let codes = arg
                    .to_vec()
                    .ok_or_else(|| illegal_argument!("not list:", i, arg))?;
                for code in &codes {
                    if let Code::Str(ref s) = &**code {
                        buf += &**s;
                    } else {
                        return Err(illegal_argument!("not string", i, arg));
                    }
                }
            }
            Code::Str(ref s) => buf += &**s,
            _ => return Err(illegal_argument!("not string", i, arg)),
        }
    }
    Ok(Code::Str(Box::new(buf)).rc())
}
