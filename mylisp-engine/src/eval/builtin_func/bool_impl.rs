use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    Ok(match &*args[0] {
        Code::Nil => constants.val_false(),
        Code::False => constants.val_false(),
        Code::Str(ref s) => constants.val_bool(!s.is_empty()),
        Code::Int32(v) => constants.val_bool(*v != 0),
        Code::Int64(ref v) => constants.val_bool(**v != 0),
        Code::Float64(ref v) => constants.val_bool(**v != 0.0),
        _ => constants.val_true(),
    })
}
