use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    for arg in &args {
        if !arg.is_list() {
            return Ok(constants.val_false());
        }
    }
    Ok(constants.val_true())
}
