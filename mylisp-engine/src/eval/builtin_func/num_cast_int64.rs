use crate::builtin_func::BuiltinFunc::NumCastInt64;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Error;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    let v = match &*args[0] {
        Code::Int32(v) => *v as i64,
        Code::Int64(ref v) => **v,
        Code::Float64(ref v) => **v as i64,
        _ => {
            return Err(Error::illegal_argument_with_detail(
                &constants.case.apply(NumCastInt64.symbol()),
                format!("not number: [ {} ]", constants.get_short_display(&*args[0])),
            ))
        }
    };
    Ok(Code::Int64(Box::new(v)).rc())
}
