use std::cmp::Ordering;

use crate::builtin_func::BuiltinFunc::StrCompare;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Error;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    macro_rules! illegal_argument {
        ($i:expr) => {
            Error::illegal_argument_with_detail(
                &constants.case.apply(StrCompare.symbol()),
                format!(
                    "not string: ({}) [ {} ]",
                    $i,
                    constants.get_short_display(&*args[$i])
                ),
            )
        };
    }
    let s0 = match &*args[0] {
        Code::Str(ref s) => s,
        _ => return Err(illegal_argument!(0)),
    };
    let s1 = match &*args[1] {
        Code::Str(ref s) => s,
        _ => return Err(illegal_argument!(1)),
    };

    let ret = match s0.cmp(s1) {
        Ordering::Less => constants.negative_one(),
        Ordering::Equal => constants.zero(),
        Ordering::Greater => constants.positive_one(),
    };
    Ok(ret)
}
