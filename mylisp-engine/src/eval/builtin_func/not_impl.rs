use crate::builtin_func::BuiltinFunc::Not;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Error;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    match &*args[0] {
        Code::True => Ok(constants.val_false()),
        Code::False => Ok(constants.val_true()),
        _ => Err(Error::illegal_argument_with_detail(
            &constants.case.apply(Not.symbol()),
            format!(
                "not boolean: [ {} ]",
                constants.get_short_display(&*args[0])
            ),
        )),
    }
}
