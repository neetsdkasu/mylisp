use crate::builtin_func::BuiltinFunc::StrLength;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Error;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    let len = match &*args[0] {
        Code::Str(ref s) => s.chars().count(),
        _ => {
            return Err(Error::illegal_argument_with_detail(
                &constants.case.apply(StrLength.symbol()),
                format!("not string: [ {} ]", constants.get_short_display(&*args[0])),
            ))
        }
    };
    let len_i32 = len as i32;
    if len_i32 as usize == len {
        return Ok(Code::Int32(len_i32).rc());
    }
    let len_i64 = len as i64;
    if len_i64 as usize == len {
        Ok(Code::Int64(Box::new(len_i64)).rc())
    } else {
        Err(Error::illegal_argument_with_detail(
            &constants.case.apply(StrLength.symbol()),
            format!("too big string length: {}", len),
        ))
    }
}
