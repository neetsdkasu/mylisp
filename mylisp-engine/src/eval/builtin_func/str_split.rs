use crate::builtin_func::BuiltinFunc::StrSplit;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Error;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    let s = match &*args[0] {
        Code::Str(ref s) => s,
        _ => {
            return Err(Error::illegal_argument_with_detail(
                &constants.case.apply(StrSplit.symbol()),
                format!(
                    "arg TARGET not string: [ {} ]",
                    constants.get_short_display(&*args[0])
                ),
            ))
        }
    };
    let mut ss: Vec<_> = if let Some(code) = args.get(1) {
        if let Code::Str(ref sep) = &**code {
            s.split(&**sep).collect()
        } else {
            return Err(Error::illegal_argument_with_detail(
                &constants.case.apply(StrSplit.symbol()),
                format!(
                    "arg SEPARATOR not string: [ {} ]",
                    constants.get_short_display(&*args[1])
                ),
            ));
        }
    } else {
        s.split_whitespace().collect()
    };
    let mut ret = constants.nil();
    while let Some(s) = ss.pop() {
        let code = Code::Str(Box::new(s.to_owned())).rc();
        ret = Code::cons(code, ret);
    }
    Ok(ret)
}
