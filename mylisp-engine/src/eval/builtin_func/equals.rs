use std::rc::Rc;

use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    for arg in args.iter().skip(1) {
        if Rc::ptr_eq(&args[0], arg) {
            continue;
        }
        if !equals(&args[0], arg) {
            return Ok(constants.val_false());
        }
    }
    Ok(constants.val_true())
}

fn equals(code1: &Rc<Code>, code2: &Rc<Code>) -> bool {
    use Code::*;
    let mut stack1 = vec![code1];
    let mut stack2 = vec![code2];
    while let Some(code1) = stack1.pop() {
        let code2 = match stack2.pop() {
            Some(code2) => code2,
            None => return false,
        };
        if Rc::ptr_eq(code1, code2) {
            continue;
        }
        let res = match &**code1 {
            Nil => code2.is_nil(),
            Cons(ref cell1) => match &**code2 {
                Code::Cons(ref cell2) => {
                    stack1.push(&cell1.cdr);
                    stack2.push(&cell2.cdr);
                    stack1.push(&cell1.car);
                    stack2.push(&cell2.car);
                    true
                }
                _ => false,
            },
            Symbol(ref s1) => matches!(&**code2, Symbol(ref s2) if s1 == s2),
            SpecialForm(form1) => matches!(&**code2, SpecialForm(form2) if form1 == form2),
            BuiltinFunc(func1) => matches!(&**code2, BuiltinFunc(func2) if func1 == func2),
            Lambda(_) | Closure(_) | Macro(_) | MacroWith(_) | Delayed(_) => false,
            Next(ref info1) => match &**code2 {
                Next(ref info2) => {
                    if info1.symbol != info2.symbol {
                        false
                    } else {
                        stack1.push(&info1.args);
                        stack2.push(&info2.args);
                        true
                    }
                }
                _ => false,
            },
            IOFunc(func1) => matches!(&**code2, IOFunc(func2) if func1 == func2),
            InjectFunc(ref info1) => {
                matches!(&**code2, InjectFunc(ref info2) if info1.id == info2.id)
            }
            True => matches!(&**code2, True),
            False => matches!(&**code2, False),
            Str(ref s1) => matches!(&**code2, Str(ref s2) if s1 == s2),
            Int32(v1) => equals_int32(*v1, code2),
            Int64(ref v1) => equals_int64(**v1, **v1 as f64, code2),
            Float64(ref v1) => equals_float64(**v1, code2),
        };
        if !res {
            return false;
        }
    }
    true
}

fn equals_int32(v1: i32, code2: &Code) -> bool {
    match code2 {
        Code::Int32(v2) if v1 == *v2 => true,
        Code::Int64(ref v2) if v1 as i64 == **v2 => true,
        Code::Float64(ref v2) if (v1 as f64 - **v2).abs() <= f64::EPSILON => true,
        _ => false,
    }
}

fn equals_int64(v1: i64, v1f: f64, code2: &Code) -> bool {
    match code2 {
        Code::Int32(v2) if v1 == *v2 as i64 => true,
        Code::Int64(ref v2) if v1 == **v2 => true,
        Code::Float64(ref v2) if (v1f as i64 == v1) && (v1f - **v2).abs() <= f64::EPSILON => true,
        _ => false,
    }
}

fn equals_float64(v1: f64, code2: &Code) -> bool {
    match code2 {
        Code::Int32(v2) if (v1 - *v2 as f64).abs() <= f64::EPSILON => true,
        Code::Int64(ref v2)
            if ((**v2 as f64) as i64 == **v2) && (v1 - **v2 as f64).abs() <= f64::EPSILON =>
        {
            true
        }
        Code::Float64(ref v2) if (v1 - **v2).abs() <= f64::EPSILON => true,
        _ => false,
    }
}
