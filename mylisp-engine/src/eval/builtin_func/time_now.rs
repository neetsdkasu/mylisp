use std::time::{SystemTime, UNIX_EPOCH};

use crate::builtin_func::BuiltinFunc::TimeNow;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Error;
use super::Result;

pub(super) fn func(constants: &Constants, _args: ArgList) -> Result {
    let now = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .map_err(|err| {
            Error::with_cause(&constants.case.apply(TimeNow.symbol()), &format!("{}", err))
        })?;

    let time = now.as_millis() as i64;

    Ok(Code::Int64(Box::new(time)).rc())
}
