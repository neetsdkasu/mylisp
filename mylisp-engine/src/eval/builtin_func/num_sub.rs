use crate::builtin_func::BuiltinFunc::NumSub;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Error;
use super::Result;

macro_rules! illegal_argument {
    ($constants:expr, $i:expr, $arg:expr) => {
        Error::illegal_argument_with_detail(
            &$constants.case.apply(NumSub.symbol()),
            format!(
                "not number: ({}) [ {} ]",
                $i,
                $constants.get_short_display(&*$arg)
            ),
        )
    };
}

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    let mut tmp: i32 = match &*args[0] {
        Code::Int32(v) => *v,
        Code::Int64(ref v) => return sub_int64(constants, **v, 1, args),
        Code::Float64(ref v) => return sub_float64(constants, **v, 1, args),
        _ => return Err(illegal_argument!(constants, 0, args[0])),
    };
    for (i, arg) in args.iter().enumerate().skip(1) {
        match &**arg {
            Code::Int32(v) => {
                if let Some(new_tmp) = tmp.checked_sub(*v) {
                    tmp = new_tmp;
                } else {
                    return sub_int64(constants, tmp as i64, i, args);
                }
            }
            Code::Int64(_) => return sub_int64(constants, tmp as i64, i, args),
            Code::Float64(_) => return sub_float64(constants, tmp as f64, i, args),
            _ => return Err(illegal_argument!(constants, i, *arg)),
        }
    }
    if args.len() == 1 {
        if let Some(new_tmp) = tmp.checked_neg() {
            tmp = new_tmp;
        } else {
            let tmp: i64 = tmp as i64;
            return Ok(Code::Int64(Box::new(-tmp)).rc());
        }
    }
    Ok(Code::Int32(tmp).rc())
}

fn sub_int64(constants: &Constants, mut tmp: i64, start: usize, args: ArgList) -> Result {
    for (i, arg) in args.iter().enumerate().skip(start) {
        match &**arg {
            Code::Int32(v) => {
                if let Some(new_tmp) = tmp.checked_sub(*v as i64) {
                    tmp = new_tmp;
                } else {
                    return sub_float64(constants, tmp as f64, i, args);
                }
            }
            Code::Int64(ref v) => {
                if let Some(new_tmp) = tmp.checked_sub(**v) {
                    tmp = new_tmp;
                } else {
                    return sub_float64(constants, tmp as f64, i, args);
                }
            }
            Code::Float64(_) => return sub_float64(constants, tmp as f64, i, args),
            _ => return Err(illegal_argument!(constants, i, *arg)),
        }
    }
    if args.len() == 1 {
        if let Some(new_tmp) = tmp.checked_neg() {
            tmp = new_tmp;
        } else {
            let tmp: f64 = tmp as f64;
            return Ok(Code::Float64(Box::new(-tmp)).rc());
        }
    }
    Ok(Code::Int64(Box::new(tmp)).rc())
}

fn sub_float64(constants: &Constants, mut tmp: f64, start: usize, args: ArgList) -> Result {
    for (i, arg) in args.iter().enumerate().skip(start) {
        match &**arg {
            Code::Int32(v) => tmp -= *v as f64,
            Code::Int64(ref v) => tmp -= **v as f64,
            Code::Float64(ref v) => tmp -= **v,
            _ => return Err(illegal_argument!(constants, i, *arg)),
        }
    }
    if args.len() == 1 {
        tmp = -tmp;
    }
    Ok(Code::Float64(Box::new(tmp)).rc())
}
