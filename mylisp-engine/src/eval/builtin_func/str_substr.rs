use crate::builtin_func::BuiltinFunc::StrSubstr;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Error;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    let s = match &*args[0] {
        Code::Str(ref s) => s,
        _ => {
            return Err(Error::illegal_argument_with_detail(
                &constants.case.apply(StrSubstr.symbol()),
                format!(
                    "arg TARGET not string: [ {} ]",
                    constants.get_short_display(&*args[0])
                ),
            ))
        }
    };
    let start = get_value(&args[1]).ok_or_else(|| {
        Error::illegal_argument_with_detail(
            &constants.case.apply(StrSubstr.symbol()),
            format!(
                "arg START-POS wrong value: [ {} ]",
                constants.get_short_display(&*args[1])
            ),
        )
    })?;
    let chars = s.chars();
    let chars = chars.skip(start);
    let s = if let Some(code) = args.get(2) {
        let len = get_value(code).ok_or_else(|| {
            Error::illegal_argument_with_detail(
                &constants.case.apply(StrSubstr.symbol()),
                format!(
                    "arg LENGTH wrong value: [ {} ]",
                    constants.get_short_display(&*args[2])
                ),
            )
        })?;
        chars.take(len).collect()
    } else {
        chars.collect()
    };
    Ok(Code::Str(Box::new(s)).rc())
}

fn get_value(code: &Code) -> Option<usize> {
    let v = match code {
        Code::Int32(v) => *v as i64,
        Code::Int64(ref v) => **v,
        Code::Float64(ref v) => {
            let w = **v as i64;
            if (w as f64 - **v).abs() > f64::EPSILON {
                return None;
            } else {
                w
            }
        }
        _ => return None,
    };
    if v < 0 {
        return None;
    }
    let u = v as usize;
    if u as i64 == v {
        Some(u)
    } else {
        None
    }
}
