use crate::builtin_func::BuiltinFunc::NumRem;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Error;
use super::Result;

macro_rules! illegal_argument {
    ($cause:literal, $constants:expr, $i:expr, $arg:expr) => {
        Error::illegal_argument_with_detail(
            &$constants.case.apply(NumRem.symbol()),
            format!(
                concat!($cause, ": ({}) [ {} ]"),
                $i,
                $constants.get_short_display(&*$arg)
            ),
        )
    };
    ($constants:expr, $i:expr, $arg:expr) => {
        illegal_argument!("wrong value", $constants, $i, $arg)
    };
}

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    let mut tmp: i32;
    match &*args[0] {
        Code::Int32(v) => tmp = *v,
        Code::Int64(ref v) => return rem_int64(constants, **v, 1, args),
        Code::Float64(ref v) => {
            if let Some(w) = f64_to_i32(**v) {
                tmp = w;
            } else if let Some(w) = f64_to_i64(**v) {
                return rem_int64(constants, w, 1, args);
            } else {
                return Err(illegal_argument!(constants, 0, args[0]));
            }
        }
        _ => return Err(illegal_argument!("not number", constants, 0, args[0])),
    }
    for (i, arg) in args.iter().enumerate().skip(1) {
        match &**arg {
            Code::Int32(v) => {
                if let Some(new_tmp) = tmp.checked_rem(*v) {
                    tmp = new_tmp;
                } else {
                    return rem_int64(constants, tmp as i64, i, args);
                }
            }
            Code::Int64(_) => return rem_int64(constants, tmp as i64, i, args),
            Code::Float64(ref v) => {
                if let Some(v) = f64_to_i32(**v) {
                    if let Some(new_tmp) = tmp.checked_rem(v) {
                        tmp = new_tmp;
                    } else {
                        return rem_int64(constants, tmp as i64, i, args);
                    }
                } else {
                    return rem_int64(constants, tmp as i64, i, args);
                }
            }
            _ => return Err(illegal_argument!("not number", constants, i, *arg)),
        }
    }
    Ok(Code::Int32(tmp).rc())
}

fn rem_int64(constants: &Constants, mut tmp: i64, start: usize, args: ArgList) -> Result {
    for (i, arg) in args.iter().enumerate().skip(start) {
        match &**arg {
            Code::Int32(v) => {
                if let Some(new_tmp) = tmp.checked_rem(*v as i64) {
                    tmp = new_tmp;
                } else {
                    return Err(illegal_argument!(constants, i, *arg));
                }
            }
            Code::Int64(ref v) => {
                if let Some(new_tmp) = tmp.checked_rem(**v) {
                    tmp = new_tmp;
                } else {
                    return Err(illegal_argument!(constants, i, *arg));
                }
            }
            Code::Float64(ref v) => {
                if let Some(v) = f64_to_i64(**v) {
                    if let Some(new_tmp) = tmp.checked_rem(v) {
                        tmp = new_tmp;
                    } else {
                        return Err(illegal_argument!(constants, i, *arg));
                    }
                } else {
                    return Err(illegal_argument!(constants, i, *arg));
                }
            }
            _ => return Err(illegal_argument!("not number", constants, i, *arg)),
        }
    }
    Ok(Code::Int64(Box::new(tmp)).rc())
}

fn f64_to_i32(v: f64) -> Option<i32> {
    let x = v as i32;
    if ((x as f64) - v).abs() > f64::EPSILON {
        None
    } else {
        Some(x)
    }
}

fn f64_to_i64(v: f64) -> Option<i64> {
    let x = v as i64;
    if ((x as f64) - v).abs() > f64::EPSILON {
        None
    } else {
        Some(x)
    }
}
