use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    if args[0].is_atom() {
        Ok(constants.t())
    } else {
        Ok(constants.nil())
    }
}
