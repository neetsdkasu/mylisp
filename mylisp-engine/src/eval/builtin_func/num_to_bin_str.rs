use crate::builtin_func::BuiltinFunc::NumToBinStr;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Error;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    let s = match &*args[0] {
        Code::Int32(v) => format!("{:b}", *v),
        Code::Int64(ref v) => format!("{:b}", **v),
        _ => {
            return Err(Error::illegal_argument_with_detail(
                &constants.case.apply(NumToBinStr.symbol()),
                format!(
                    "not integer: [ {} ]",
                    constants.get_short_display(&*args[0])
                ),
            ))
        }
    };
    Ok(Code::Str(Box::new(s)).rc())
}
