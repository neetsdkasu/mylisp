use crate::builtin_func::BuiltinFunc::And;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Error;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    let mut res = true;
    for (i, arg) in args.iter().enumerate() {
        match &**arg {
            Code::True => {}
            Code::False => res = false,
            _ => {
                return Err(Error::illegal_argument_with_detail(
                    &constants.case.apply(And.symbol()),
                    format!(
                        "not boolean: ({}) [ {} ]",
                        i,
                        constants.get_short_display(&**arg)
                    ),
                ))
            }
        }
    }
    Ok(constants.val_bool(res))
}
