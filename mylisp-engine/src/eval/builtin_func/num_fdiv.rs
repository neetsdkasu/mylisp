use crate::builtin_func::BuiltinFunc::NumFDiv;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Error;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    macro_rules! illegal_argument {
        ($i:expr, $arg:expr) => {
            Error::illegal_argument_with_detail(
                &constants.case.apply(NumFDiv.symbol()),
                format!(
                    "not number: ({}) [ {} ]",
                    $i,
                    constants.get_short_display(&*$arg)
                ),
            )
        };
    }
    let mut tmp: f64 = match &*args[0] {
        Code::Int32(v) => *v as f64,
        Code::Int64(ref v) => **v as f64,
        Code::Float64(ref v) => **v,
        _ => return Err(illegal_argument!(0, args[0])),
    };
    for (i, arg) in args.iter().enumerate().skip(1) {
        match &**arg {
            Code::Int32(v) => tmp /= *v as f64,
            Code::Int64(ref v) => tmp /= **v as f64,
            Code::Float64(ref v) => tmp /= **v,
            _ => return Err(illegal_argument!(i, *arg)),
        }
    }
    Ok(Code::Float64(Box::new(tmp)).rc())
}
