use crate::builtin_func::BuiltinFunc::Cdr;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Error;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    match &*args[0] {
        Code::Nil => Ok(constants.nil()),
        Code::Cons(ref cell) => Ok(cell.cdr.clone()),
        _ => Err(Error::illegal_argument_with_detail(
            &constants.case.apply(Cdr.symbol()),
            format!(
                "not cons cell: [ {} ]",
                constants.get_short_display(&*args[0])
            ),
        )),
    }
}
