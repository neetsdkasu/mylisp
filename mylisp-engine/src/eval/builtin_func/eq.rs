use crate::builtin_func::BuiltinFunc::Eq;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Error;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    if let Some(ret) = Code::eq(&args[0], &args[1]) {
        if ret {
            Ok(constants.t())
        } else {
            Ok(constants.nil())
        }
    } else {
        let mut detail = String::new();
        for (i, arg) in args.iter().enumerate().take(2) {
            if Code::eq(arg, arg).is_none() {
                detail += &format!(
                    "not symbol arg: ({}) [ {} ], ",
                    i,
                    constants.get_short_display(&**arg)
                );
            }
        }
        Err(Error::illegal_argument_with_detail(
            &constants.case.apply(Eq.symbol()),
            detail,
        ))
    }
}
