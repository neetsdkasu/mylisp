use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Result;

pub(super) fn func(constants: &Constants, mut args: ArgList) -> Result {
    let mut list = constants.nil();
    while let Some(code) = args.pop() {
        list = Code::cons(code, list);
    }
    Ok(list)
}
