use crate::builtin_func::BuiltinFunc::NumCastInt32;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Error;
use super::Result;

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    let v = match &*args[0] {
        Code::Int32(v) => *v,
        Code::Int64(ref v) => **v as i32,
        Code::Float64(ref v) => **v as i32,
        _ => {
            return Err(Error::illegal_argument_with_detail(
                &constants.case.apply(NumCastInt32.symbol()),
                format!("not number: [ {} ]", constants.get_short_display(&*args[0])),
            ))
        }
    };
    Ok(Code::Int32(v).rc())
}
