use crate::builtin_func::BuiltinFunc::NumMul;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Error;
use super::Result;

macro_rules! illegal_argument {
    ($constants:expr, $i:expr, $arg:expr) => {
        Error::illegal_argument_with_detail(
            &$constants.case.apply(NumMul.symbol()),
            format!(
                "not number: ({}) [ {} ]",
                $i,
                $constants.get_short_display(&**$arg)
            ),
        )
    };
}

pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    let mut tmp: i32 = 1;
    for (i, arg) in args.iter().enumerate() {
        match &**arg {
            Code::Int32(v) => {
                if let Some(new_tmp) = tmp.checked_mul(*v) {
                    tmp = new_tmp;
                } else {
                    return mul_int64(constants, tmp as i64, i, args);
                }
            }
            Code::Int64(_) => return mul_int64(constants, tmp as i64, i, args),
            Code::Float64(_) => return mul_float64(constants, tmp as f64, i, args),
            _ => return Err(illegal_argument!(constants, i, arg)),
        }
    }
    Ok(Code::Int32(tmp).rc())
}

fn mul_int64(constants: &Constants, mut tmp: i64, start: usize, args: ArgList) -> Result {
    for (i, arg) in args.iter().enumerate().skip(start) {
        match &**arg {
            Code::Int32(v) => {
                if let Some(new_tmp) = tmp.checked_mul(*v as i64) {
                    tmp = new_tmp;
                } else {
                    return mul_float64(constants, tmp as f64, i, args);
                }
            }
            Code::Int64(ref v) => {
                if let Some(new_tmp) = tmp.checked_mul(**v) {
                    tmp = new_tmp;
                } else {
                    return mul_float64(constants, tmp as f64, i, args);
                }
            }
            Code::Float64(_) => return mul_float64(constants, tmp as f64, i, args),
            _ => return Err(illegal_argument!(constants, i, arg)),
        }
    }
    Ok(Code::Int64(Box::new(tmp)).rc())
}

fn mul_float64(constants: &Constants, mut tmp: f64, start: usize, args: ArgList) -> Result {
    for (i, arg) in args.iter().enumerate().skip(start) {
        match &**arg {
            Code::Int32(v) => tmp *= *v as f64,
            Code::Int64(ref v) => tmp *= **v as f64,
            Code::Float64(ref v) => tmp *= **v,
            _ => return Err(illegal_argument!(constants, i, arg)),
        }
    }
    Ok(Code::Float64(Box::new(tmp)).rc())
}
