use crate::builtin_func::BuiltinFunc::BitwiseOr;
use crate::code::Code;

use super::ArgList;
use super::Constants;
use super::Error;
use super::Result;

macro_rules! illegal_argument {
    ($constants:expr, $i:expr, $arg:expr, $cause:literal) => {
        Error::illegal_argument_with_detail(
            &$constants.case.apply(BitwiseOr.symbol()),
            format!(
                concat!($cause, ": ({}) [ {} ]"),
                $i,
                $constants.get_short_display(&**$arg)
            ),
        )
    };
}
pub(super) fn func(constants: &Constants, args: ArgList) -> Result {
    let mut tmp: i32 = 0;
    for (i, arg) in args.iter().enumerate() {
        match &**arg {
            Code::Int32(v) => tmp |= *v,
            Code::Int64(_) => return or_int64(constants, tmp as i64, i, args),
            Code::Float64(ref v) => {
                let w = **v as i32;
                if (w as f64 - **v).abs() > f64::EPSILON {
                    return or_int64(constants, tmp as i64, i, args);
                }
                tmp |= w;
            }
            _ => return Err(illegal_argument!(constants, i, arg, "not number")),
        }
    }
    Ok(Code::Int32(tmp).rc())
}

fn or_int64(constants: &Constants, mut tmp: i64, start: usize, args: ArgList) -> Result {
    for (i, arg) in args.iter().enumerate().skip(start) {
        match &**arg {
            Code::Int32(v) => tmp |= *v as i64,
            Code::Int64(ref v) => tmp |= **v,
            Code::Float64(ref v) => {
                let w = **v as i64;
                if (w as f64 - **v).abs() > f64::EPSILON {
                    return Err(illegal_argument!(constants, i, arg, "wrong value"));
                }
                tmp |= w;
            }
            _ => return Err(illegal_argument!(constants, i, arg, "not number")),
        }
    }
    Ok(Code::Int64(Box::new(tmp)).rc())
}
