macro_rules! bug {
    ($fmt:literal $(,$arg:expr)*) => {
        format!(concat!("[{}:{}] Engine BUG? ", $fmt), file!(), line!() $(,$arg)*)
    };
    ($fmt:literal $(,$arg:expr)*,) => {
        bug!($fmt $(,$arg)*)
    };
}
