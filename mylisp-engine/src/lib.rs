use std::rc::Rc;
use std::result;

use mylisp_code as code;
use mylisp_common::*;

use builtin_func::BuiltinFunc;
use code::{Bindings, Code};
use io_func::IOFunc;
use special_form::SpecialForm;

pub use error::Error;

use constants::Constants;

#[macro_use]
mod utils;

mod binding;
mod constants;
mod error;
mod eval;

pub type Result = result::Result<Rc<Code>, Error>;

type ArgList = Vec<Rc<Code>>;

pub struct IOSet<R, W, E> {
    pub input: R,
    pub output: W,
    pub error: E,
    pub case: code::Case,
    pub syntax: code::Syntax,
}

impl<R, W, E> IOSet<R, W, E> {
    fn get_display<'a>(&self, code: &'a Code) -> code::CodeDisplay<'a> {
        code.display(self.case, self.syntax)
    }
}

struct Environment {
    bindings: Bindings,
    lexical_parent: Option<Rc<Bindings>>,
}

impl Environment {
    fn new() -> Environment {
        Environment {
            bindings: Bindings::new(),
            lexical_parent: None,
        }
    }

    #[allow(dead_code)]
    fn from_bindings(from_bindings: Rc<Bindings>) -> Environment {
        let mut bindings = Bindings::new();
        bindings.clone_from(&*from_bindings);
        Environment {
            bindings,
            lexical_parent: None,
        }
    }

    fn with_parent(scope: Rc<Bindings>) -> Environment {
        Environment {
            bindings: Bindings::new(),
            lexical_parent: Some(scope),
        }
    }
}

pub struct Engine<R, W, E> {
    lexical: bool,
    bindings: Bindings,
    macros: Bindings,
    constants: Constants,
    call_stack: Vec<eval::CallStackState<R, W, E>>,
    scope: Vec<Environment>,
    top_level: bool,
    pub io_set: IOSet<R, W, E>,
    lambda_id: u32,
}

#[derive(Debug, Clone, Copy)]
pub enum Scope {
    Dynamic,
    Lexical,
}

impl<R, W, E> Engine<R, W, E> {
    #[allow(dead_code)]
    pub fn new(io_set: IOSet<R, W, E>) -> Self {
        Engine::with_scope(Scope::Dynamic, io_set)
    }

    pub fn with_scope(scope: Scope, io_set: IOSet<R, W, E>) -> Self {
        let mut bindings = Bindings::new();
        for e in BuiltinFunc::iter() {
            let key = Rc::new(e.symbol().to_string());
            bindings.insert(key, Code::BuiltinFunc(e).rc());
        }
        for e in SpecialForm::iter() {
            let key = Rc::new(e.symbol().to_string());
            bindings.insert(key, Code::SpecialForm(e).rc());
        }
        for e in IOFunc::iter() {
            let key = Rc::new(e.symbol().to_string());
            bindings.insert(key, Code::IOFunc(e).rc());
        }
        let constants = Constants::new(io_set.case, io_set.syntax);
        let t = constants.t();
        let t_key = t.get_symbol().unwrap_or_else(|| Rc::new("t".to_string()));
        bindings.insert(t_key, t);
        bindings.insert(Rc::new("true".to_string()), constants.val_true());
        bindings.insert(Rc::new("false".to_string()), constants.val_false());
        Engine {
            lexical: matches!(scope, Scope::Lexical),
            bindings,
            macros: Bindings::new(),
            constants,
            call_stack: vec![],
            scope: vec![Environment::new()],
            top_level: true,
            io_set,
            lambda_id: 0,
        }
    }

    fn new_lambda_id(&mut self) -> u32 {
        self.lambda_id += 1;
        self.lambda_id
    }

    pub fn reset(&mut self) {
        self.clean_up();
        self.scope[0].bindings.clear();
        self.macros.clear();
    }

    pub fn set_scope(&mut self, scope: Scope) {
        self.lexical = matches!(scope, Scope::Lexical)
    }
}

impl<R, W, E> Engine<R, W, E>
where
    R: std::io::BufRead,
    W: std::io::Write,
    E: std::io::Write,
{
    pub fn bind_special_code(&mut self, name: &str, code: Rc<Code>) -> result::Result<(), Error> {
        let name = Rc::new(name.to_string());
        let code = self.eval(code)?;
        self.bindings.insert(name, code);
        Ok(())
    }
}
