use std::io;
use std::rc::Rc;
use std::result;

use crate::code::{ClosureInfo, Code, ConsCell, InjectFuncInfo, LambdaInfo};
use crate::special_form::SpecialForm;

use super::ArgList;
use super::Constants;
use super::Engine;
use super::Environment;
use super::Error;
use super::IOSet;
use super::Result;

use self::builtin_func::{get_builtin_func_info, BuiltinFuncInfo};
use self::io_func::{get_io_func_info, IOFuncInfo};
use self::special_form::{get_special_form_info, SpecialFormInfo};

const MAX_ARG_SIZE: usize = 100;

mod builtin_func;
mod builtin_func_state;
mod io_func;
mod io_func_state;
mod special_form;
mod stack_trace;

pub(super) struct BuiltinFuncRequest {
    info: &'static BuiltinFuncInfo,
    evaluated_args: ArgList,
    remaining_args: Rc<Code>,
}

pub(super) struct IOFuncRequest<R, W, E> {
    info: IOFuncInfo<R, W, E>,
    evaluated_args: ArgList,
    remaining_args: Rc<Code>,
}

pub(super) struct SpecialFormRequest<R, W, E> {
    info: SpecialFormInfo<R, W, E>,
    code_is_result: bool,
    code: Option<Rc<Code>>,
    temporary_codes: Option<Vec<Rc<Code>>>,
    remaining_args: Rc<Code>,
}

pub(super) struct LambdaRequest {
    info: Rc<LambdaInfo>,
    evaluated_args: ArgList,
    remaining_args: Rc<Code>,
}

pub(super) struct ClosureRequest {
    info: Rc<ClosureInfo>,
    evaluated_args: ArgList,
    remaining_args: Rc<Code>,
}

pub(super) struct RecurRequest {
    symbol: Rc<String>,
    recur_code: Rc<Code>,
}

pub(super) struct InjectFuncRequest {
    info: Rc<InjectFuncInfo>,
    evaluated_args: ArgList,
    remaining_args: Rc<Code>,
}

pub(super) enum CallStackState<R, W, E> {
    ResolveFuncName(Rc<Code>),
    ResolveArgsForBuiltinFunc(Box<BuiltinFuncRequest>),
    ResolveArgsForIOFunc(Box<IOFuncRequest<R, W, E>>),
    ResolveSpecialForm(Box<SpecialFormRequest<R, W, E>>),
    ResolveArgsForLambda(Box<LambdaRequest>),
    ResolveArgsForClosure(Box<ClosureRequest>),
    ResolveArgsForInjectFunc(Box<InjectFuncRequest>),
    Recurring(Box<RecurRequest>),
    MacroEnd(Rc<String>),
    RemoveScope(Option<Rc<Code>>),
}

impl<R, W, E> Engine<R, W, E> {
    pub(super) fn clean_up(&mut self) {
        self.top_level = true;
        self.call_stack.clear();
        while self.scope.len() > 1 {
            self.scope.pop();
        }
        self.constants.case = self.io_set.case;
        self.constants.syntax = self.io_set.syntax;
    }
}

impl<R, W, E> Engine<R, W, E> {
    pub fn eval(&mut self, code: Rc<Code>) -> Result
    where
        R: io::BufRead,
        W: io::Write,
        E: io::Write,
    {
        self.clean_up();
        self.eval_dirty(code)
    }

    fn eval_dirty(&mut self, mut code: Rc<Code>) -> Result
    where
        R: io::BufRead,
        W: io::Write,
        E: io::Write,
    {
        code = self.extend_macro(code)?;
        loop {
            match *code {
                Code::Cons(ref cell) => {
                    let ConsCell { ref car, ref cdr } = &**cell;
                    if !cdr.is_list() {
                        return Err(Error::wrap(format!(
                            "cannot evaluate: {}",
                            self.io_set.get_display(&*code)
                        )));
                    }
                    self.call_stack
                        .push(CallStackState::ResolveFuncName(cdr.clone()));
                    code = car.clone();
                    continue;
                }
                Code::Symbol(ref name) => code = self.get_binding(name)?,
                _ => {}
            }
            let (new_code, end) = self.sweep_call_stack(code)?;
            code = new_code;
            if end {
                return Ok(code);
            }
        }
    }

    fn extend_macro(&mut self, code: Rc<Code>) -> Result
    where
        R: io::BufRead,
        W: io::Write,
        E: io::Write,
    {
        match &*code {
            Code::Cons(ref cell) => {
                let car = self.extend_macro(cell.car.clone())?;
                let cdr = self.extend_macro(cell.cdr.clone())?;
                let symbol = match car.get_symbol() {
                    Some(symbol) => symbol,
                    None => return Ok(Code::cons(car, cdr)),
                };
                let macro_code = match self.macros.get(&symbol) {
                    Some(macro_code) => macro_code.clone(),
                    None => return Ok(Code::cons(car, cdr)),
                };
                let arg_list = match cdr.to_vec() {
                    Some(arg_list) => arg_list,
                    None => return Ok(Code::cons(car, cdr)),
                };
                match &*macro_code {
                    Code::Macro(ref info) => {
                        if !info.check_args_size(arg_list.len()) {
                            return Ok(Code::cons(car, cdr));
                        }
                        self.push_scope(Environment::new());
                        if self
                            .bind_lambda_args(&info.args, arg_list, &*macro_code)
                            .is_err()
                        {
                            self.pop_scope()?;
                            return Ok(Code::cons(car, cdr));
                        }
                        self.call_stack
                            .push(CallStackState::MacroEnd(symbol.clone()));
                        let new_code = match self.eval_dirty(info.body.clone()) {
                            Ok(new_code) => new_code,
                            Err(err) => {
                                return Err(Error::with_cause(
                                    &format!("MacroCall({})", symbol),
                                    &err.to_string(),
                                ))
                            }
                        };
                        self.pop_scope()?;
                        self.extend_macro(new_code)
                    }
                    Code::MacroWith(ref info) => {
                        if !info.check_args_size(arg_list.len()) {
                            return Ok(Code::cons(car, cdr));
                        }
                        self.push_scope(Environment::with_parent(info.scope.clone()));
                        self.push_scope(Environment::new());
                        let info = &info.lambda_info;
                        if self
                            .bind_lambda_args(&info.args, arg_list, &*macro_code)
                            .is_err()
                        {
                            self.pop_scope()?;
                            self.pop_scope()?;
                            return Ok(Code::cons(car, cdr));
                        }
                        self.call_stack
                            .push(CallStackState::MacroEnd(symbol.clone()));
                        let new_code = match self.eval_dirty(info.body.clone()) {
                            Ok(new_code) => new_code,
                            Err(err) => {
                                return Err(Error::with_cause(
                                    &format!("MacroCall({})", symbol),
                                    &err.to_string(),
                                ))
                            }
                        };
                        self.pop_scope()?;
                        self.pop_scope()?;
                        self.extend_macro(new_code)
                    }
                    _ => Err(Error::wrap(bug!("in extend_macro"))),
                }
            }
            _ => Ok(code),
        }
    }
}

impl<R, W, E> Engine<R, W, E> {
    fn sweep_call_stack(&mut self, mut code: Rc<Code>) -> result::Result<(Rc<Code>, bool), Error>
    where
        R: io::BufRead,
        W: io::Write,
        E: io::Write,
    {
        loop {
            let state = match self.call_stack.pop() {
                None => return Ok((code, true)),
                Some(CallStackState::MacroEnd(_)) => {
                    self.top_level = self.call_stack.is_empty();
                    return Ok((code, true));
                }
                Some(state) => state,
            };
            self.top_level = self.call_stack.is_empty();
            let (new_code, escape) = self.resolve_state(state, code)?;
            code = new_code;
            if escape {
                return Ok((code, false));
            }
        }
    }
}

impl<R, W, E> Engine<R, W, E> {
    fn resolve_state(
        &mut self,
        state: CallStackState<R, W, E>,
        code: Rc<Code>,
    ) -> result::Result<(Rc<Code>, bool), Error>
    where
        R: io::BufRead,
        W: io::Write,
        E: io::Write,
    {
        match state {
            CallStackState::ResolveFuncName(args) => self.resolve_func_name(code, args),
            CallStackState::ResolveArgsForBuiltinFunc(mut request) => {
                request.evaluated_args.push(code);
                self.resolve_builtin_func(request)
            }
            CallStackState::ResolveArgsForIOFunc(mut request) => {
                request.evaluated_args.push(code);
                self.resolve_io_func(request)
            }
            CallStackState::ResolveSpecialForm(mut request) => {
                request.code = Some(code);
                self.resolve_special_form(request)
            }
            CallStackState::ResolveArgsForLambda(mut request) => {
                request.evaluated_args.push(code);
                self.resolve_lambda_args(request)
            }
            CallStackState::ResolveArgsForClosure(mut request) => {
                request.evaluated_args.push(code);
                self.resolve_closure_args(request)
            }
            CallStackState::ResolveArgsForInjectFunc(mut request) => {
                request.evaluated_args.push(code);
                self.resolve_inject_func(request)
            }
            CallStackState::MacroEnd(ref name) => {
                Err(Error::wrap(bug!("in MacroEnd( {:?} )", name)))
            }
            CallStackState::RemoveScope(_) => {
                self.pop_scope()?;
                Ok((code, false))
            }
            CallStackState::Recurring(request) => self.check_recurring(code, request),
        }
    }
}

impl<R, W, E> Engine<R, W, E> {
    fn check_recurring(
        &mut self,
        code: Rc<Code>,
        request: Box<RecurRequest>,
    ) -> result::Result<(Rc<Code>, bool), Error> {
        let info = match &*code {
            Code::Next(ref info) => info,
            _ => return Ok((code, false)),
        };
        if *info.symbol != *request.symbol {
            return Ok((code, false));
        }
        let args = info.args.clone();
        let code = request.recur_code.clone();
        self.call_stack.push(CallStackState::Recurring(request));
        match &*code {
            Code::Lambda(ref info) => {
                let args = args.to_vec().ok_or_else(|| {
                    Error::illegal_argument_with_detail(
                        &self.io_set.case.apply(SpecialForm::Next.symbol()),
                        format!("invalid ARGS: [ {} ]", self.io_set.get_display(&*args)),
                    )
                })?;
                self.resolve_lambda_args(Box::new(LambdaRequest {
                    info: info.clone(),
                    evaluated_args: args.iter().map(Rc::clone).collect(),
                    remaining_args: self.constants.nil(),
                }))
            }
            Code::Closure(ref info) => {
                let args = args.to_vec().ok_or_else(|| {
                    Error::illegal_argument_with_detail(
                        &self.io_set.case.apply(SpecialForm::Next.symbol()),
                        format!("invalid ARGS: [ {} ]", self.io_set.get_display(&*args)),
                    )
                })?;
                self.resolve_closure_args(Box::new(ClosureRequest {
                    info: info.clone(),
                    evaluated_args: args.iter().map(Rc::clone).collect(),
                    remaining_args: self.constants.nil(),
                }))
            }
            _ => Err(Error::wrap(bug!("wakaran..."))),
        }
    }
}

impl<R, W, E> Engine<R, W, E> {
    fn resolve_func_name(
        &mut self,
        code: Rc<Code>,
        args: Rc<Code>,
    ) -> result::Result<(Rc<Code>, bool), Error>
    where
        R: io::BufRead,
        W: io::Write,
        E: io::Write,
    {
        match *code {
            Code::SpecialForm(form) => {
                let info = get_special_form_info(form);
                let request = info.make_request(args);
                self.resolve_special_form(request)
            }
            Code::BuiltinFunc(func) => {
                let info = get_builtin_func_info(func);
                if !info.validate_args_count(&args).unwrap_or(false) {
                    return Err(Error::illegal_argument_with_detail(
                        &self.io_set.case.apply(info.func_type().symbol()),
                        format!("invalid arguments: [ {} ]", self.io_set.get_display(&*args)),
                    ));
                }
                self.resolve_builtin_func(Box::new(BuiltinFuncRequest {
                    info,
                    evaluated_args: vec![],
                    remaining_args: args,
                }))
            }
            Code::Lambda(ref info) => {
                if !info.validate_args_count(&args).unwrap_or(false) {
                    return Err(Error::illegal_argument_with_detail(
                        &self.io_set.get_short_display(&**info),
                        format!("invalid argument: [ {} ]", self.io_set.get_display(&*args)),
                    ));
                }
                self.resolve_lambda_args(Box::new(LambdaRequest {
                    info: info.clone(),
                    evaluated_args: vec![],
                    remaining_args: args,
                }))
            }
            Code::Closure(ref info) => {
                if !info.validate_args_count(&args).unwrap_or(false) {
                    return Err(Error::illegal_argument_with_detail(
                        &self.io_set.get_short_display(&**info),
                        format!("invalid argument: [ {} ]", self.io_set.get_display(&*args)),
                    ));
                }
                self.resolve_closure_args(Box::new(ClosureRequest {
                    info: info.clone(),
                    evaluated_args: vec![],
                    remaining_args: args,
                }))
            }
            Code::IOFunc(func) => {
                let info = get_io_func_info(func);
                if !info.validate_args_count(&args).unwrap_or(false) {
                    return Err(Error::illegal_argument_with_detail(
                        &self.io_set.case.apply(info.func_type().symbol()),
                        format!("invalid arguments: [ {} ]", self.io_set.get_display(&*args)),
                    ));
                }
                self.resolve_io_func(Box::new(IOFuncRequest {
                    info,
                    evaluated_args: vec![],
                    remaining_args: args,
                }))
            }
            Code::InjectFunc(ref info) => {
                if !info.validate_args_count(&args).unwrap_or(false) {
                    return Err(Error::illegal_argument_with_detail(
                        &format!("{:?}", info),
                        format!("invalid arguments: [ {} ]", self.io_set.get_display(&*args)),
                    ));
                }
                self.resolve_inject_func(Box::new(InjectFuncRequest {
                    info: Rc::clone(info),
                    evaluated_args: vec![],
                    remaining_args: args,
                }))
            }
            _ => Err(Error::wrap(format!(
                "not callable: [ {} ]",
                self.io_set.get_short_display(&*code)
            ))),
        }
    }
}

impl<R, W, E> Engine<R, W, E> {
    fn resolve_inject_func(
        &mut self,
        mut request: Box<InjectFuncRequest>,
    ) -> result::Result<(Rc<Code>, bool), Error> {
        match &*request.remaining_args {
            Code::Nil => {
                let info = &request.info;
                let args = request.evaluated_args.as_slice();
                match (info.call_back)(args) {
                    Ok(code) => Ok((code, false)),
                    Err(msg) => Err(Error::wrap(msg)),
                }
            }
            Code::Cons(ref cell) => {
                let code = cell.car.clone();
                request.remaining_args = cell.cdr.clone();
                self.call_stack
                    .push(CallStackState::ResolveArgsForInjectFunc(request));
                Ok((code, true))
            }
            _ => Err(Error::wrap(bug!("in resolve_inject_func"))),
        }
    }
}

impl<R, W, E> Engine<R, W, E> {
    fn resolve_builtin_func(
        &mut self,
        request: Box<BuiltinFuncRequest>,
    ) -> result::Result<(Rc<Code>, bool), Error> {
        let (new_code, new_state) = builtin_func_state::get(&self.constants, request)?;
        if let Some(new_state) = new_state {
            self.call_stack.push(new_state);
            Ok((new_code, true))
        } else {
            Ok((new_code, false))
        }
    }
}

impl<R, W, E> Engine<R, W, E> {
    fn resolve_io_func(
        &mut self,
        request: Box<IOFuncRequest<R, W, E>>,
    ) -> result::Result<(Rc<Code>, bool), Error> {
        let (new_code, new_state) = io_func_state::get(&mut self.io_set, &self.constants, request)?;
        if let Some(new_state) = new_state {
            self.call_stack.push(new_state);
            Ok((new_code, true))
        } else {
            Ok((new_code, false))
        }
    }
}

impl<R, W, E> Engine<R, W, E> {
    fn resolve_special_form(
        &mut self,
        request: Box<SpecialFormRequest<R, W, E>>,
    ) -> result::Result<(Rc<Code>, bool), Error> {
        let call = request.info.get_call_back();
        let request = call(self, request)?;
        if request.code_is_result {
            request
                .code
                .as_ref()
                .map(|code| (code.clone(), false))
                .ok_or_else(|| Error::wrap(bug!("in {:?}", request.info.form_type())))
        } else {
            let code = request
                .code
                .as_ref()
                .map(Rc::clone)
                .ok_or_else(|| Error::wrap(bug!("in {:?}", request.info.form_type())))?;
            self.call_stack
                .push(CallStackState::ResolveSpecialForm(request));
            Ok((code, true))
        }
    }
}

impl<R, W, E> Engine<R, W, E> {
    fn resolve_lambda_args(
        &mut self,
        mut request: Box<LambdaRequest>,
    ) -> result::Result<(Rc<Code>, bool), Error> {
        match &*request.remaining_args {
            Code::Nil => {
                let LambdaRequest {
                    info,
                    evaluated_args,
                    remaining_args: _,
                } = *request;
                self.push_scope(Environment::new());
                self.bind_lambda_args(&info.args, evaluated_args, &*info)?;
                self.call_stack.push(CallStackState::RemoveScope(Some(
                    Code::Lambda(info.clone()).rc(),
                )));
                Ok((info.body.clone(), true))
            }
            Code::Cons(ref cell) => {
                let code = cell.car.clone();
                request.remaining_args = cell.cdr.clone();
                self.call_stack
                    .push(CallStackState::ResolveArgsForLambda(request));
                Ok((code, true))
            }
            _ => Err(Error::wrap(bug!("in resolve_lambda_args"))),
        }
    }
}

impl<R, W, E> Engine<R, W, E> {
    fn resolve_closure_args(
        &mut self,
        mut request: Box<ClosureRequest>,
    ) -> result::Result<(Rc<Code>, bool), Error> {
        match &*request.remaining_args {
            Code::Nil => {
                let ClosureRequest {
                    info,
                    evaluated_args,
                    remaining_args: _,
                } = *request;
                self.push_scope(Environment::with_parent(info.scope.clone()));
                self.push_scope(Environment::new());
                self.bind_lambda_args(&info.lambda_info.args, evaluated_args, &*info)?;
                self.call_stack.push(CallStackState::RemoveScope(Some(
                    Code::Closure(info.clone()).rc(),
                )));
                self.call_stack.push(CallStackState::RemoveScope(None));
                Ok((info.lambda_info.body.clone(), true))
            }
            Code::Cons(ref cell) => {
                let code = cell.car.clone();
                request.remaining_args = cell.cdr.clone();
                self.call_stack
                    .push(CallStackState::ResolveArgsForClosure(request));
                Ok((code, true))
            }
            _ => Err(Error::wrap(bug!("in resolve_closure_args"))),
        }
    }
}

impl<R, W, E> Engine<R, W, E> {
    fn bind_lambda_args<T: stack_trace::ShortDisplay>(
        &mut self,
        arg_names: &Rc<Code>,
        mut arg_values: ArgList,
        call_from: &T,
    ) -> result::Result<(), Error> {
        let mut value_list = self.constants.nil();
        while let Some(code) = arg_values.pop() {
            value_list = Code::Cons(Box::new(ConsCell {
                car: code,
                cdr: value_list,
            }))
            .rc();
        }
        let mut name_stack = vec![arg_names];
        let mut value_stack = vec![value_list];
        while let Some(name) = name_stack.pop() {
            let value = value_stack
                .pop()
                .ok_or_else(|| Error::wrap(bug!("in bind_lambda_args")))?;
            match &**name {
                Code::Nil => {
                    if !value.is_nil() {
                        return Err(Error::illegal_argument_with_detail(
                            &self.io_set.get_short_display(call_from),
                            format!(
                                "unmatch argument pattern: [ {} ] (expect [ {} ])",
                                self.io_set.get_display(&*value),
                                self.io_set.get_display(&Code::Nil)
                            ),
                        ));
                    }
                }
                Code::Cons(ref name_cell) => {
                    name_stack.push(&name_cell.cdr);
                    name_stack.push(&name_cell.car);
                    if let Code::Cons(ref value_cell) = &*value {
                        value_stack.push(value_cell.cdr.clone());
                        value_stack.push(value_cell.car.clone());
                    } else {
                        return Err(Error::illegal_argument_with_detail(
                            &self.io_set.get_short_display(call_from),
                            format!(
                                "unmatch argument pattern: [ {} ] (expect [ {} ])",
                                self.io_set.get_display(&*value),
                                self.io_set.get_display(&*name)
                            ),
                        ));
                    }
                }
                Code::Symbol(ref symbol) => {
                    self.bind(symbol.clone(), value)?;
                }
                _ => return Err(Error::wrap(bug!("in bind_lambda_args"))),
            }
        }
        Ok(())
    }
}
