use std::rc::Rc;
use std::result;

use crate::code::{Bindings, Code};

use super::Engine;
use super::Environment;
use super::Error;
use super::Result;

impl<R, W, E> Engine<R, W, E> {
    pub(super) fn bind(&mut self, name: Rc<String>, code: Rc<Code>) -> result::Result<(), Error> {
        if self.bindings.contains_key(&name) {
            return Err(Error::wrap(format!(
                "invalid binding name: [ {} ] (reserved word)",
                self.io_set.case.apply(name.as_str())
            )));
        }
        if self.macros.contains_key(&name) {
            return Err(Error::wrap(format!(
                "invalid binding name: [ {} ] (used by macro)",
                self.io_set.case.apply(name.as_str())
            )));
        }
        if let Some(env) = self.scope.last_mut() {
            env.bindings.insert(name, code);
            Ok(())
        } else {
            Err(Error::wrap(bug!("not found bindings at bind")))
        }
    }

    pub(super) fn bind_macro(
        &mut self,
        name: Rc<String>,
        code: Rc<Code>,
    ) -> result::Result<(), Error> {
        self.macros.insert(name.clone(), code.clone());
        if let Some(env) = self.scope.last_mut() {
            env.bindings.insert(name, code);
            Ok(())
        } else {
            Err(Error::wrap(bug!("not found bindings at bind")))
        }
    }

    pub(super) fn push_scope(&mut self, env: Environment) {
        self.scope.push(env);
    }

    pub(super) fn pop_scope(&mut self) -> result::Result<(), Error> {
        self.scope
            .pop()
            .ok_or_else(|| Error::wrap(bug!("in pop_scope")))?;
        if self.scope.is_empty() {
            Err(Error::wrap(bug!("in pop_scope")))
        } else {
            Ok(())
        }
    }

    pub(super) fn get_binding(&self, name: &Rc<String>) -> Result {
        self.bindings
            .get(name)
            .or_else(|| {
                self.scope
                    .iter()
                    .rev()
                    .scan(false, |found_scope, env| {
                        if *found_scope {
                            None
                        } else {
                            *found_scope = env.lexical_parent.is_some();
                            Some(env)
                        }
                    })
                    .find_map(|env| {
                        env.lexical_parent
                            .as_ref()
                            .and_then(|parent| parent.get(name))
                            .or_else(|| env.bindings.get(name))
                    })
            })
            .map(Rc::clone)
            .ok_or_else(|| Error::wrap(format!("not found name: {}", name)))
    }

    pub(super) fn get_scope_bindings(&self) -> Bindings {
        self.scope
            .iter()
            .rev()
            .scan(false, |found_scope, env| {
                if *found_scope {
                    None
                } else {
                    *found_scope = env.lexical_parent.is_some();
                    Some(env)
                }
            })
            .fold(Bindings::new(), |mut acc, env| {
                if let Some(ref scope) = env.lexical_parent {
                    scope.iter().for_each(|(k, v)| {
                        acc.entry(k.clone()).or_insert_with(|| v.clone());
                    });
                } else {
                    env.bindings.iter().for_each(|(k, v)| {
                        acc.entry(k.clone()).or_insert_with(|| v.clone());
                    });
                }
                acc
            })
    }
}
