use std::rc::Rc;

use crate::code::{Case, Code, CodeDisplay, Syntax};

pub(super) struct Constants {
    code_symbol_t: Rc<Code>,
    code_nil: Rc<Code>,
    code_true: Rc<Code>,
    code_false: Rc<Code>,
    code_zero: Rc<Code>,
    code_positive_one: Rc<Code>,
    code_negative_one: Rc<Code>,
    pub(super) case: Case,
    pub(super) syntax: Syntax,
}

impl Constants {
    pub(super) fn new(case: Case, syntax: Syntax) -> Constants {
        Constants {
            code_symbol_t: Code::Symbol(Rc::new("t".to_string())).rc(),
            code_nil: Code::Nil.rc(),
            code_true: Code::True.rc(),
            code_false: Code::False.rc(),
            code_zero: Code::Int32(0).rc(),
            code_positive_one: Code::Int32(1).rc(),
            code_negative_one: Code::Int32(-1).rc(),
            case,
            syntax,
        }
    }

    pub(super) fn t(&self) -> Rc<Code> {
        self.code_symbol_t.clone()
    }

    pub(super) fn nil(&self) -> Rc<Code> {
        self.code_nil.clone()
    }

    pub(super) fn val_true(&self) -> Rc<Code> {
        self.code_true.clone()
    }

    pub(super) fn val_false(&self) -> Rc<Code> {
        self.code_false.clone()
    }

    pub(super) fn val_bool(&self, b: bool) -> Rc<Code> {
        if b {
            self.val_true()
        } else {
            self.val_false()
        }
    }

    pub(super) fn zero(&self) -> Rc<Code> {
        self.code_zero.clone()
    }

    pub(super) fn positive_one(&self) -> Rc<Code> {
        self.code_positive_one.clone()
    }

    pub(super) fn negative_one(&self) -> Rc<Code> {
        self.code_negative_one.clone()
    }

    #[allow(dead_code)]
    pub(super) fn get_display<'a>(&self, code: &'a Code) -> CodeDisplay<'a> {
        code.display(self.case, self.syntax)
    }
}
