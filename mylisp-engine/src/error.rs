use std::error;
use std::fmt;
use std::io;

#[derive(Debug)]
pub struct Error {
    msg: String,
    io_err: Option<io::Error>,
}

impl Error {
    #[allow(dead_code)]
    pub(super) fn new(s: &str) -> Error {
        Error::wrap(s.to_string())
    }

    pub(super) fn wrap(msg: String) -> Error {
        Error { msg, io_err: None }
    }

    pub(super) fn with_cause(cause: &str, description: &str) -> Error {
        Error::wrap(format!("[ {} ] {}", cause, description))
    }

    #[allow(dead_code)]
    pub(super) fn illegal_argument(func_name: &str) -> Error {
        Error::illegal_argument_with_detail(func_name, "".to_string())
    }

    pub(super) fn illegal_argument_with_detail(func_name: &str, detail: String) -> Error {
        Error::with_cause(func_name, &format!("Illegal Argument {{ {} }}", detail))
    }

    pub(super) fn io_error(cause: &str, err: io::Error) -> Error {
        Error {
            msg: format!("[ {} ] IO Error", cause),
            io_err: Some(err),
        }
    }

    #[allow(dead_code)]
    pub(super) fn err<R>(self) -> Result<R, Self> {
        Err(self)
    }
}

impl error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.msg)?;
        if let Some(ref io_err) = self.io_err {
            write!(f, " with ( {} )", io_err)?;
        }
        Ok(())
    }
}
